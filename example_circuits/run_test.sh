#!/bin/bash


for f in ./*.cir
do
    echo "Running $f"
    ngspice -b "$f"
done