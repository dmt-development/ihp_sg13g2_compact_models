""" This is the "final" script that compares the measurement data to the data from compact model simulation with ngspice.
The script produces a PDF using PyLatex for visualization.

This script assumes that the IHP-Open-PDK Data is available in the main repo folder. If not, run the script create_dutlib/create_dut_lib.py, before running this script!
"""
from pathlib import Path
import numpy as np
from DMT.config import COMMANDS

COMMANDS["OPENVAF"] = "openvaf"
from DMT.core import DutLib, specifiers, SimCon
from DMT.ngspice import DutNgspice as dut_circuit

# for Tex documents
from pylatex import Section, Subsection, Figure, NoEscape
from pylatex.base_classes import Arguments
from DMT.external import SubFile, CommandInputTikz, CommandLabel

from create_dutlib.ihp_openPDK_SG13g2 import TechSG13G2OpenPDK
from helper_functions import (
    get_dut_mos,
    get_mos_dimensions,
    get_sweep_def_mos,
    get_sweep_def_hbt,
    get_mos_plot,
    get_hbt_plot,
    get_hbt_dimensions,
    get_dut_hbt,
    get_parallel_hbt,
)

# Flags
force = False  # If True: re-run simulations, even if they are already in the cache

# Paths
path_to_folder = Path(__file__).resolve().parent
lib_save_dir = path_to_folder.absolute() / "openPDK_lib"
docu_save_dir = path_to_folder.absolute() / "documentation" / "model_vs_meas"
lib = DutLib.load(lib_save_dir, classes_technology=[TechSG13G2OpenPDK])
fig_dir = path_to_folder.absolute() / "documentation" / "model_vs_meas" / "figures"
fig_dir = fig_dir.absolute()


# define some columns for convenience
col_vd = specifiers.VOLTAGE + "D"
col_vb = specifiers.VOLTAGE + "B"
col_vg = specifiers.VOLTAGE + "G"
col_vs = specifiers.VOLTAGE + "S"
col_id = specifiers.CURRENT + "D"


# simulation controller
sim_con = SimCon(n_core=1, t_max=30)

# define title for the Tex document
title = "IHP SG13G2 Model vs. Measurement Comparisons"

# define each section of the document here, with a lambda that will tell if the device should be put into the section
# done NMOS LV, NPN,
subfiles = {
    "NMOS LV": lambda x: "nmos" in x and not "HV" in x,
    "NMOS HV": lambda x: "nmos" in x and "HV" in x,
    "PMOS HV": lambda x: "pmos" in x and "HV" in x,
    # "PMOS LV": lambda x: "pmos" in x and not "HV" in x,
    "NPN": lambda x: ("npn13g2_" in x) and (not "open" in x) and (not "short" in x),
}

mos_wmin = 0.3e-6

for subfile_name, analyze in subfiles.items():
    fig_sub_dir_abs = fig_dir / subfile_name.replace(" ", "_")
    fig_sub_dir_rel = Path("figures") / subfile_name.replace(" ", "_")
    doc = SubFile()
    with doc.create(
        Section(NoEscape(subfile_name))
    ):  # for Every DUT create a section in the Tex document
        n = 0
        for dut in lib:
            # check if this device should be part of the subsection
            if not analyze(dut.name):
                continue

            # if not "S557_4" in dut.name:
            #     continue

            # get detailed DUT information such as type, width, length etc. from the name of the device
            mos, hbt = False, False
            if "mos" in dut.name:
                mos = True
                nmos = "nmos" in dut.name
                hv = "HV" in dut.name
                width, length = get_mos_dimensions(dut.name)
                if width < mos_wmin:
                    continue

            elif "npn13g2_" in dut.name:
                hbt = True
                width, length = get_hbt_dimensions(dut.name)
            else:
                print("Device Type not implemented: " + dut.name + " Skipping.")
                continue

            n = n + 1
            if n > 20:  # no more than 20 devices!
                continue

            # for Every DUT create a subsection in the Tex document
            doc.append(NoEscape(r"\clearpage "))
            with doc.create(Subsection(NoEscape(dut.name.replace("_", "\_")))):
                # prepare sign conversion for PMOS devices
                sign = 1
                if mos:
                    if not nmos:
                        sign = -1

                # define the DUT for simulation
                if mos:
                    dut_cir = get_dut_mos(dut_circuit, nmos, hv, length, width)
                else:
                    dut_cir = get_dut_hbt(dut_circuit, dut.name, length, width)

                # now go through every measurement
                for key in dut.data.keys():
                    # if not "spar_vce" in key:
                    #     continue
                    # if not "vcb10" in key:
                    #     continue

                    df = dut.data[key]
                    if mos:
                        try:
                            (
                                sweeps,
                                dfs,
                                x_limits,
                                y_limits,
                                fix_var,
                                fix_var2,
                                x_var,
                                y_var,
                                caption,
                            ) = get_sweep_def_mos(dut, key, df, nmos, hv)
                        except NotImplementedError:
                            continue
                    elif hbt:
                        try:
                            (
                                sweeps,
                                dfs,
                                x_limits,
                                y_limits,
                                fix_var,
                                fix_var2,
                                x_var,
                                y_var,
                                caption,
                            ) = get_sweep_def_hbt(dut, key, df, dut.name)
                        except NotImplementedError:
                            continue
                    else:
                        continue

                    # run the circuit simulations
                    for sweep in sweeps:
                        sim_con.append_simulation(dut=dut_cir, sweep=sweep)
                    sim_con.run_and_read(force=force, remove_simulations=False)

                    # define the Plot object
                    if mos:
                        plt = get_mos_plot(
                            dut, key, x_var, y_var, x_limits, y_limits, nmos
                        )
                    elif hbt:
                        plt = get_hbt_plot(dut, key, x_var, y_var, x_limits, y_limits)
                    else:
                        continue

                    # iterate over "lines" in the plot, i.e. where the fixed voltage is constant.
                    # add the data to the plot
                    for i, df_meas_i in enumerate(dfs):
                        sweep = sweeps[i]
                        df_sim_i = dut_cir.get_data(sweep=sweep)

                        # get the data (sign to account for pmos)
                        if specifiers.VOLTAGE in x_var:
                            col_x = x_var
                        else:
                            col_x = x_var

                        ports = ["G", "D"]
                        if hbt:
                            ports = ["B", "C"]

                        # make sure all quantities in dataframe
                        df_sim_i.ensure_specifier_column(col_x, ports=ports)
                        df_sim_i.ensure_specifier_column(y_var, ports=ports)
                        df_meas_i.ensure_specifier_column(col_x, ports=ports)
                        df_meas_i.ensure_specifier_column(y_var, ports=ports)

                        # retrieve measured and simulated data
                        x_sim = sign * np.real(df_sim_i[col_x].to_numpy())
                        y_sim = sign * np.real(df_sim_i[y_var].to_numpy())
                        x_meas = sign * df_meas_i[col_x].to_numpy()
                        y_meas = sign * df_meas_i[y_var].to_numpy()

                        # normalization of currents to width (MOS) or area (HBT)
                        n_parallel = 1  # number of parallel devices in measurement
                        curr_norm = 1 / width
                        if hbt:
                            curr_norm = 1 / (width + 36e-9)
                            curr_norm = curr_norm / (
                                length + 36e-9
                            )  # area, accounting also for difference between drawn and actual emitter window dimensions
                            # here we hardcode the number of parallel devices according to dut_setup.htm from the IHP Git
                            n_parallel = get_parallel_hbt(dut.name)

                        if specifiers.CURRENT in y_var:
                            y_meas = y_meas * curr_norm / n_parallel
                            y_sim = y_sim * curr_norm

                        if specifiers.CURRENT in x_var:
                            x_meas = x_meas * curr_norm / n_parallel
                            x_sim = x_sim * curr_norm

                        # add data to plot
                        v_fixed = np.unique(df_meas_i[fix_var])[0]
                        if fix_var2 is not None:
                            v_fixed2 = np.unique(df_meas_i[fix_var2])[0]
                            label = r"$\SI{{{0:1.2f}}}{{\volt}}$,$\SI{{{1:1.2f}}}{{\volt}}$".format(
                                v_fixed, v_fixed2
                            )
                        else:
                            label = r"$\SI{{{0:1.2f}}}{{\volt}}$".format(v_fixed)

                        plt.add_data_set(x_meas, y_meas, label=label)
                        plt.add_data_set(x_sim, y_sim)

                    # plt.plot_pyqtgraph(show=True)

                    # add the plot to the document
                    doc.append(NoEscape(r"\FloatBarrier "))
                    with doc.create(Figure(position="h!")) as _plot:
                        _plot.append(NoEscape(r"\centering"))
                        plt_save_name = plt.save_tikz(
                            fig_sub_dir_abs,
                            width=None,
                            mark_repeat=2,
                            extension=r"tikz",
                        )
                        # if (
                        #     "dut_SG13_pmosXm1Y3_W10u0_L5u0_S549_4_T300_dot_00Kdc_idvg"
                        #     in plt_save_name
                        # ):
                        #     plt_save_name = plt.save_tikz(
                        #         fig_sub_dir_abs,
                        #         width=None,
                        #         mark_repeat=2,
                        #         extension=r"tikz",
                        #     )
                        if len(plt.data) > 32:
                            _plot.append(
                                CommandInputTikz(
                                    arguments=Arguments(
                                        "width=0.6\\textwidth, height=0.9\\textheight",
                                        str(fig_sub_dir_rel / plt_save_name),
                                    )
                                )
                            )
                        else:
                            _plot.append(
                                CommandInputTikz(
                                    arguments=Arguments(
                                        "width=0.6\\textwidth, height=0.4\\textheight",
                                        str(fig_sub_dir_rel / plt_save_name),
                                    )
                                )
                            )

                        _plot.add_caption(NoEscape(caption))
                        _plot.append(CommandLabel(arguments=Arguments(str(n))))

                    doc.append(NoEscape(r"\FloatBarrier "))

    doc.generate_tex(
        str(docu_save_dir / f"model_vs_meas{subfile_name.replace(' ', '_')}")
    )
