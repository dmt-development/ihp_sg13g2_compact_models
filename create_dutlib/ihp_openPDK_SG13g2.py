""" The DMT.Technology class for the open SG13G2 PDK
"""
# IHP SG13G2 Model verification
# Copyright (C) 2023  SemiMod <https://gitlab.com/dmt-development/ihp_sg13g2_compact_models>

# DMT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# DMT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from DMT.core import Technology


class TechSG13G2OpenPDK(Technology):
    def __init__(self):
        super().__init__(name="IHP SG13G2 Open PDK")

    @staticmethod
    def deserialize():
        return TechSG13G2OpenPDK()

    def serialize(self):
        return {
            "class": str(self.__class__),
            "args": [],
            "kwargs": {},
            "constructor": "deserialize",
        }
