""" Script that reads the IHP SG13G2 measurement data into a DMT.DutLib. The DutLib object can conveniently used in Python and is based on the Pandas library. 

This example demonstrates how to read in the IHP SG13G2 measurement data into a DMT DutLib object. The data processing can then be done in Python. 
Feel free to use this example as a starting point for bulk reading of data.

During run of this script the measurment data from https://github.com/IHP-GmbH/IHP-Open-PDK are downloaded and then read into a DutLib.

"""
# IHP SG13G2 Model verification
# Copyright (C) 2023  SemiMod <https://gitlab.com/dmt-development/ihp_sg13g2_compact_models>

# DMT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# DMT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
import shutil
import subprocess
from pathlib import Path
from DMT.core import DutMeas, DutType, DutLib, specifiers
from ihp_openPDK_SG13g2 import TechSG13G2OpenPDK

# paths
path_to_folder = Path(__file__).resolve().parent
path_to_git = (
    path_to_folder / "IHP-Open-PDK"
)  # adjust this path locally so that the Skywater repo is found
path_to_meas = path_to_git / "libs.doc" / "meas"
lib_save_dir = path_to_folder.parent / "openPDK_lib"

# start by check out of the git project
path_to_git.mkdir(exist_ok=True)
subprocess.run(
    [
        "curl",
        "--output",
        "openPDK.tar.gz",
        "https://codeload.github.com/IHP-GmbH/IHP-Open-PDK/tar.gz/main",
    ],
    cwd=path_to_git,
)
subprocess.run(
    [
        "tar",
        "-xz",
        "--strip=2",
        "-f",
        "openPDK.tar.gz",
        "IHP-Open-PDK-main/ihp-sg13g2/libs.doc/meas",
    ],
    cwd=path_to_git,
)
(path_to_git / "openPDK.tar.gz").unlink()

# remove the non-measurement files

for child in path_to_meas.glob("**/**/*"):
    if child.is_file() and not child.suffix == ".mdm":
        child.unlink()

shutil.rmtree(path_to_meas / "HBT" / "doc", ignore_errors=True)
shutil.rmtree(path_to_meas / "MOS" / "doc", ignore_errors=True)

# resort the mos folder
meas_files = [meas_file for meas_file in (path_to_meas / "MOS").glob("**/**/*")]
for child in meas_files:
    if child.is_file():
        # no sizes -> not usable
        if "Cbdsg" in child.name:
            child.unlink()
            continue
        if "Cgds" in child.name:
            child.unlink()
            continue
        if "CJBD" in child.name:
            child.unlink()
            continue
        if "CJBS" in child.name:
            child.unlink()
            continue
        if "Cox" in child.name:
            child.unlink()
            continue
        if "Diode" in child.name:
            child.unlink()
            continue

        if not "~" in child.name:
            # already processed
            continue

        # mos with name and size, this is what we want
        # SG13_nmosHVXm1Y3/SG13_nmosHV~W0u3_L0u3_S560_1~dc_idvd_vbmin~300K.mdm
        # to
        # SG13_nmosHVXm1Y3_W0u3_L0u3_S560_1/T300K/dc_idvd_vbmin.mdm
        file_name_parts = child.stem.split("~")
        device_name = str(child.parent.name) + "_" + file_name_parts[1]
        temperature = "T" + file_name_parts[3]
        meas_name = file_name_parts[2] + ".mdm"

        target_folder = child.parent / device_name / temperature
        target_folder.mkdir(exist_ok=True, parents=True)
        shutil.move(child, target_folder / meas_name)

# hbt is already good (single temperature..., but we may have to move the deembeding structures to a separate folder..)
meas_files = [meas_file for meas_file in (path_to_meas / "HBT").glob("**/**/*")]
for child in meas_files:
    if child.is_file():
        # either
        # something with "open" or "short" in the name
        # or
        # meas data
        if "T300K" in child.parts:
            # already processed
            continue

        temperature = "T300K"

        if "open" in child.stem:
            device_name = "open"
        elif "short" in child.stem:
            device_name = "short"
        else:
            device_name = child.parent.name

        target_folder = child.parent / device_name / temperature
        target_folder.mkdir(exist_ok=True, parents=True)
        shutil.move(child, target_folder / child.name)

# Create a DutLib object that is used to store the measurement data
lib = DutLib(save_dir=lib_save_dir, force=True)


# for importing all data at once, DutLib can use the import_directory method. This method requires a custom filter function to create the correct DutViews, each holding data of one device and corresponding miscellaneous data such as contact configurations etc.
def filter_dut(dut_name):
    """Create DutView objects from the different folder names. Infer information from folder structure of Skywater.

    Parameters
    ----------
    dut_name : str
        Path to the dut folder.

    Returns
    -------
    DMT.core.DutView
        DutView which should represent the device represented by a folder of measurements.
    """
    dut_path = Path(dut_name)

    if "npn13g2" in dut_path.name:
        dut_type = DutType.npn
    elif "short" in dut_path.name and "npn13g2" in dut_path.parts[-2]:
        dut_type = DutType.deem_short_bjt
    elif "open" in dut_path.name and "npn13g2" in dut_path.parts[-2]:
        dut_type = DutType.deem_open_bjt
    elif "nmos" in dut_path.name:
        dut_type = DutType.n_mos
    elif "pmos" in dut_path.name:
        dut_type = DutType.p_mos
    else:
        raise IOError(
            "This can not happen with the meas data at release of this script."
        )

    if dut_type in [DutType.npn, DutType.deem_short_bjt, DutType.deem_open_bjt]:
        if dut_type == DutType.npn:
            name = dut_path.name
            deemb_name = dut_path.name
            flavor = name.split("_")[0]
        else:
            name = dut_path.parts[-1] + "_" + dut_path.parts[-2]
            deemb_name = dut_path.parts[-2]
            flavor = name.split("_")[0]
        reference_node = "E"
        contact_config = "BEC"
        if "npn13g2v" in name:
            width = 0.12e-6
            if "T00" in name:
                length = 2.5e-6
                multiplication_factor = 2
            elif "T01" in name:
                length = 2.5e-6
                multiplication_factor = 4
            elif "T02" in name:
                length = 5e-6
                multiplication_factor = 1
            elif "T03" in name:
                length = 5e-6
                multiplication_factor = 2
            elif "T04" in name:
                length = 5e-6
                multiplication_factor = 4
        elif "npn13g2l" in name:
            width = 0.07e-6
            if "T00" in name:
                length = 2.5e-6
                multiplication_factor = 2
            elif "T01" in name:
                length = 2.5e-6
                multiplication_factor = 4
            elif "T02" in name:
                length = 5e-6
                multiplication_factor = 1
            elif "T03" in name:
                length = 5e-6
                multiplication_factor = 2
            elif "T04" in name:
                length = 5e-6
                multiplication_factor = 4
        else:
            width = 0.07e-6
            length = 0.9e-6
            if "T00" in name:
                multiplication_factor = 1
            elif "T01" in name:
                multiplication_factor = 2
            elif "T02" in name:
                multiplication_factor = 4
            elif "T03" in name:
                multiplication_factor = 8

    elif dut_type == DutType.n_mos or dut_type == DutType.p_mos:
        name = dut_path.name
        deemb_name = dut_path.name
        name_parts = name.split("_")
        width = float(name_parts[2][1:].replace("u", ".")) * 1e-6
        length = float(name_parts[3][1:].replace("u", ".")) * 1e-6
        multiplication_factor = 1  # always
        flavor = name_parts[1]
        reference_node = "S"
        contact_config = "GSD"
    else:
        raise IOError("This can not happen.")

    dut = DutMeas(
        database_dir=None,
        dut_type=dut_type,
        force=True,
        width=width,
        length=length,
        name=name,
        reference_node=reference_node,
        ndevices=multiplication_factor,
        flavor=flavor,
        contact_config=contact_config,
        deemb_name=deemb_name,
    )

    return dut


# Import the measurements using the dut filter function in parallel. See the method`s documentation in DMT.core.
lib.n_jobs = 1  # number of parallel jobs
lib.import_directory(
    import_dir=path_to_meas,
    dut_filter=filter_dut,
    dut_level=3,
    force=True,
    # temperature_converter=key_generator,
)
tech = TechSG13G2OpenPDK()
# "Clean" the data so that DMT standardized names for electrical quantities are used.
# we use the already deembeded data to save some time...
fallback_dict = {
    # "S(1,1)": None,
    # "S(2,1)": None,
    # "S(1,2)": None,
    # "S(2,2)": None,
    # "S_DEEMB(1,1)": specifiers.SS_PARA_S + ["B", "B"],
    # "S_DEEMB(2,1)": specifiers.SS_PARA_S + ["C", "B"],
    # "S_DEEMB(1,2)": specifiers.SS_PARA_S + ["B", "C"],
    # "S_DEEMB(2,2)": specifiers.SS_PARA_S + ["C", "C"],
    "S_DEEMB(1,1)": None,
    "S_DEEMB(2,1)": None,
    "S_DEEMB(1,2)": None,
    "S_DEEMB(2,2)": None,
}
for dut_a in lib:
    dut_a.technology = tech
    dut_a.clean_data(fallback=fallback_dict)

lib.dut_ref = lib.duts[0]
# does not matter for this script but is needed for data saving and documentation.

lib.deem_types = [DutType.bjt]
lib.AC_filter_names = [("spar", "dummy")]
lib.ignore_duts = [
    "npn13g2v_T00",
    "npn13g2v_T01",
    "npn13g2v_T02",
    "npn13g2v_T03",
    "npn13g2v_T04",
]
lib.deembed_AC(True, True, True)
lib.ignore_duts = []

# The data is now read for further use
# to save just use:
lib.save()
# to load in a different script:
# lib = DutLib.load(lib_save_dir, classes_technology=[TechSG13G2OpenPDK])
