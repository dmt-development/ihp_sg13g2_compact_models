""" In this script the already created DutLib is loaded and an measurement documentation is created.
"""
# IHP SG13G2 Model verification
# Copyright (C) 2023  SemiMod <https://gitlab.com/dmt-development/ihp_sg13g2_compact_models>

# DMT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# DMT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from pathlib import Path
from DMT.core import DutLib, DocuDutLib, specifiers, DutType
from ihp_openPDK_SG13g2 import TechSG13G2OpenPDK

path_to_folder = Path(__file__).resolve().parent
lib_save_dir = path_to_folder.parent / "openPDK_lib"
docu_save_dir = path_to_folder.parent / "documentation" / "meas_data_overview"
lib = DutLib.load(lib_save_dir, classes_technology=[TechSG13G2OpenPDK])

docu = DocuDutLib(
    lib,
    devices=[
        {"dut_type": DutType.npn},
        {"dut_type": DutType.p_mos},
        {"dut_type": DutType.n_mos},
    ],
)
docu.generate_docu(
    docu_save_dir,
    plot_specs=[
        {
            "type": "gummel_vbc",
            "exclude_at": [],
            "key": ["spar_vcb025", "spar_vcb05"],
            "exact_match": False,
            "FREQ": 1e9,  # optional, float: If given, use only data at FREQ
            # "xmin": 10e-2,  # optional, float: Minimum Value on x-axis to be displayed
            # "xmax": 1e2,  # optional, float: Maximum Value on x-axis to be displayed
            # "ymax": 300,  # optional, float: Maximum Value on y-axis to be displayed
            # "ymin": 0,
            # "no_at": True,  # optional, Bool: if True, do not display the "at" quantities in the legend.
            "at": specifiers.VOLTAGE + ["B", "C"],
        },
        {
            "type": "ft_jc_vbc",
            "exclude_at": [],
            "key": ["spar_vcb025", "spar_vcb05"],
            "exact_match": False,
            "FREQ": 1e9,  # optional, float: If given, use only data at FREQ
            # "xmin": 10e-2,  # optional, float: Minimum Value on x-axis to be displayed
            # "xmax": 1e2,  # optional, float: Maximum Value on x-axis to be displayed
            # "ymax": 300,  # optional, float: Maximum Value on y-axis to be displayed
            "ymin": 0,
            # "no_at": True,  # optional, Bool: if True, do not display the "at" quantities in the legend.
            "at": specifiers.VOLTAGE + ["B", "C"],
        },
        {
            "dut_type": DutType.n_mos,
            "type": "id(vg)",
            # "exclude_at": [],
            "key": "dc_idvg",
            "exact_match": True,
        },
        {
            "dut_type": DutType.n_mos,
            "type": "id(vd)",
            "key": "dc_idvd",
            "exact_match": True,
        },
        {
            "dut_type": DutType.p_mos,
            "type": "id(vg)",
            # "exclude_at": [],
            "key": "dc_idvg",
            "exact_match": True,
        },
        {
            "dut_type": DutType.p_mos,
            "type": "id(vd)",
            "key": "dc_idvd",
            "exact_match": True,
        },
    ],
    show=False,
)
