#!/bin/bash
pdflatex --shell-escape --interaction=nonstopmode model_vs_measNMOS_HV.tex 
pdflatex --shell-escape --interaction=nonstopmode model_vs_measNMOS_LV.tex 
pdflatex --shell-escape --interaction=nonstopmode model_vs_measPMOS_HV.tex 
pdflatex --shell-escape --interaction=nonstopmode model_vs_measPMOS_LV.tex 
pdflatex --shell-escape --interaction=nonstopmode model_vs_measNPN.tex 

# make -j 12 -f model_vs_measNMOS_HV.makefile 
# make -j 12 -f model_vs_measNMOS_LV.makefile 
# make -j 12 -f model_vs_measPMOS_HV.makefile 
# make -j 12 -f model_vs_measPMOS_LV.makefile 
# make -j 12 -f model_vs_measNPN.makefile 

# pdflatex --shell-escape --interaction=nonstopmode model_vs_measNMOS_HV.tex 
# pdflatex --shell-escape --interaction=nonstopmode model_vs_measNMOS_LV.tex 
# pdflatex --shell-escape --interaction=nonstopmode model_vs_measPMOS_HV.tex 
# pdflatex --shell-escape --interaction=nonstopmode model_vs_measPMOS_LV.tex 
# pdflatex --shell-escape --interaction=nonstopmode model_vs_measNPN.tex 

# pdflatex --shell-escape --interaction=nonstopmode documentation.tex 
# pdflatex --shell-escape --interaction=nonstopmode documentation.tex 