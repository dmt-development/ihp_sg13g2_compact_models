% DMT
% Copyright (C) 2019  Markus Müller and Mario Krattenmacher and the DMT contributors <https://gitlab.hrz.tu-chemnitz.de/CEDIC_Bipolar/DMT/>

% DMT is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% DMT is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>

%---------------------------READ BEFORE USING
% the symbols here shall be ordered thematically. E.g. dimensions of a 1D BJT should be grouped together.
% Do not order alphabetically...

%GICCR stuff----------------------------------------------------------------------------
\newglossaryentry{alpha_mE}%
{%
  name={\ensuremath{\alpha_{\mathrm{mE}}}},
  description={emitter minority charge partitioning factor (see \equref{equ:TICCR:chargePartFactor_minorityEmitter})},
  sort={alphamE}, type=symbolslist_greek
}
\newglossaryentry{alpha_jE}%
{%
  name={\ensuremath{\alpha_{\mathrm{jE}}}},
  description={emitter depletion charge partitioning factor (see \equref{equ:TICCR:chargePartFactor_depletionEmitter})},
  sort={alphajE}, type=symbolslist_greek
}
\newglossaryentry{alpha_mB}%
{%
  name={\ensuremath{\alpha_{\mathrm{mB}}}},
  description={base minority charge partitioning factor (see \equref{equ:TICCR:chargePartFactor_minorityBase})},
  sort={alphamB}, type=symbolslist_greek
}
\newglossaryentry{alpha_jC}%
{%
  name={\ensuremath{\alpha_{\mathrm{jC}}}},
  description={collector depletion charge partitioning factor (see \equref{equ:TICCR:chargePartFactor_depletionCollector})},
  sort={alphajC}, type=symbolslist_greek
}
\newglossaryentry{alpha_mC}%
{%
  name={\ensuremath{\alpha_{\mathrm{mC}}}},
  description={collector minority charge partitioning factor (see \equref{equ:TICCR:chargePartFactor_minorityCollector})},
  sort={alphamC}, type=symbolslist_greek
}
%organic DOS stuff----------------------------------------------------------------------
\newglossaryentry{sig_npti}%
{%
  name={\ensuremath{\sigma_{\mathrm{n(p)t}i}}},
  description={$i$-th slope of a series of exponential DOS functions},
  sort={signpti}, type=symbolslist_greek
}
\newglossaryentry{sig_nti}%
{%
  name={\ensuremath{\sigma_{\mathrm{nt}i}}},
  description={width for the $i$-th exponential trap DOS for electrons},
  sort={signti}, type=symbolslist_greek
}

\newglossaryentry{sig_pti}%
{%
  name={\ensuremath{\sigma_{\mathrm{pt}i}}},
  description={width for the $i$-th exponential trap DOS for holes},
  sort={sigpti}, type=symbolslist_greek
}

\newglossaryentry{sig_nt}%
{%
  name={\ensuremath{\sigma_{\mathrm{nt}}}},
  description={width for a single exponential trap DOS for electrons},
  sort={signt}, type=symbolslist_greek
}

\newglossaryentry{sig_pt}%
{%
  name={\ensuremath{\sigma_{\mathrm{pt}}}},
  description={width for a single exponential trap DOS for holes},
  sort={sigpt}, type=symbolslist_greek
}
\newglossaryentry{sig_n(p)}%
{%
  name={\ensuremath{\sigma_{\mathrm{n(p)}}}},
  description={standard deviation of the Gaussian DOS that models the LUMO (HOMO)},
  sort={signp}, type=symbolslist_greek
}

\newglossaryentry{sig_n}%
{%
  name={\ensuremath{\sigma_{\mathrm{n}}}},
  description={standard deviation of the Gaussian DOS that models the LUMO},
  sort={sign}, type=symbolslist_greek
}

\newglossaryentry{sig_p}%
{%
  name={\ensuremath{\sigma_{\mathrm{p}}}},
  description={standard deviation of the Gaussian DOS that models the HOMO},
  sort={sigp}, type=symbolslist_greek
}
%1D BJT dimension stuff-----------------------------------------------------------------
%basic equation systems stuff ----------------------------------------------------------
\newglossaryentry{eps}%
{%
  name={\ensuremath{\varepsilon}},
  description={permittivity: $\varepsilon = \ac{epsilon0} \varepsilon_{\mathrm{r}}$},
  sort={epsilon}, type=symbolslist_greek
}

\newglossaryentry{eps_r}%
{%
  name={\ensuremath{\varepsilon_{\mathrm{r}}}},
  description={relative permittivity},
  sort={epsilonr}, type=symbolslist_greek
}
\newglossaryentry{phi}
{
  name={\ensuremath{\varphi}},
  description={Fermi potential at thermal equilibrium},
  sort={phi}, type=symbolslist_greek
}
\newglossaryentry{phi_n}
{
  name={\ensuremath{\varphi_{\mathrm{n}}}},
  description={quasi-Fermi potential of electrons},
  sort={phin}, type=symbolslist_greek
}
\newglossaryentry{phi_p}
{
  name={\ensuremath{\varphi_{\mathrm{p}}}},
  description={quasi-Fermi potential of holes},
  sort={phip}, type=symbolslist_greek
}

\newglossaryentry{phi_n(p)}
{
  name={\ensuremath{\varphi_{\mathrm{n(p)}}}},
  description={quasi-Fermi potential of electrons (holes)},
  sort={phinp}, type=symbolslist_greek
}
\newglossaryentry{psi}
{
  name={\ensuremath{\psi}},
  description={electrostatic potential},
  sort={psi}, type=symbolslist_greek
}
\newglossaryentry{psi_builtin}
{
  name={\ensuremath{\psi_{\mathrm{equ}}}},
  description={built-in potential},
  sort={psiequ}, type=symbolslist_greek
}
% other ---------------------------------------------------------------------------------
\newglossaryentry{gamma_p}%
{%
  name={\ensuremath{\gamma_\mathrm{p}}},
  description={PoA separation factor},
  sort={gammap}, type=symbolslist_greek
}
% boundary conditions -------------------------------------------------------------------
\newglossaryentry{Phi_n}
{
  name={\ensuremath{\Phi_{\mathrm{n}}}},
  description={Electron injection barrier at a Schottky contact},
  sort={Phin}, type=symbolslist_greek
}
\newglossaryentry{Phi_p}
{
  name={\ensuremath{\Phi_{\mathrm{p}}}},
  description={Hole injection barrier at a Schottky contact},
  sort={Phip}, type=symbolslist_greek
}
\newglossaryentry{psi_schottky}
{
  name={\ensuremath{\Psi{\mathrm{Schottky}}}},
  description={Electrostatic potential at a Schottky contact},
  sort={psischottky}, type=symbolslist_greek
}
\newglossaryentry{rho}%
{%
  name={\ensuremath{\rho}},
  description={space-charge density},
  sort={rho}, type=symbolslist_greek
}
\newglossaryentry{voltConduction}%
{%
  name={\ensuremath{\psi_{\mathrm{C}}}},
  description={conduction band potential},
  sort={conductionbandpo}, type=symbolslist_greek
}

\newglossaryentry{voltValence}%
{%
  name={\ensuremath{\psi_{\mathrm{V}}}},
  description={valence band potential},
  sort={valencebandpotential}, type=symbolslist_greek
}
\newglossaryentry{xi}%
{%
  name={\ensuremath{\xi}},
  description={additional spatial coordinate of a one dimensional bipolar transistor, used when one integration boundary is x},
  sort={xi}, type=symbolslist_greek
}
% recombination -------------------------------------------------------------------------
\newglossaryentry{tau_nllis}%
{%
  name={\ensuremath{\tau_{\mathrm{n,srh,lli}}}},
  description={LLI lifetime of of electrons due to SRH recombination} ,
  sort={tauns}, type=symbolslist_greek
}
\newglossaryentry{tau_pllis}%
{%
  name={\ensuremath{\tau_{\mathrm{p,SRH,lli}}}},
  description={LLI lifetime of of electrons due to SRH recombination} ,
  sort={tauns}, type=symbolslist_greek
}
\newglossaryentry{tau_ns}%
{%
  name={\ensuremath{\tau_{\mathrm{ns}}}},
  description={SRH lifetime of electrons} ,
  sort={tauns}, type=symbolslist_greek
}
\newglossaryentry{tau_ps}%
{%
  name={\ensuremath{\tau_{\mathrm{ps}}}},
  description={SRH lifetime of holes} ,
  sort={taups}, type=symbolslist_greek
}
\newglossaryentry{tau_n}%
{%
  name={\ensuremath{\tau_{\mathrm{n}}}},
  description={minority carrier lifetime of electrons} ,
  sort={taun}, type=symbolslist_greek
}

\newglossaryentry{tau_m}%
{%
  name={\ensuremath{\tau_{\mathrm{m}}}},
  description={minority carrier lifetime} ,
  sort={taum}, type=symbolslist_greek
}

\newglossaryentry{tau_p}%
{%
  name={\ensuremath{\tau_{\mathrm{p}}}},
  description={minority carrier lifetime of holes} ,
  sort={taup}, type=symbolslist_greek
}
% mobility ------------------------------------------------------------------------------
\newglossaryentry{mu_n}%
{%
  name={\ensuremath{\mu_{\mathrm{n}}}},
  description={electron mobility},
  sort={mun}, type=symbolslist_greek
}

\newglossaryentry{mu_nr}%
{%
  name={\ensuremath{\mu_{\mathrm{nr}}}},
  description={reference electron mobility},
  sort={munr}, type=symbolslist_greek
}

\newglossaryentry{mu_pr}%
{%
  name={\ensuremath{\mu_{\mathrm{pr}}}},
  description={reference hole mobility},
  sort={mupr}, type=symbolslist_greek
}
\newglossaryentry{mu_0n}%
{%
  name={\ensuremath{\mu_{\mathrm{0n}}}},
  description={zero field electron mobility},
  sort={muon}, type=symbolslist_greek
}
\newglossaryentry{mu_0p}%
{%
  name={\ensuremath{\mu_{\mathrm{0p}}}},
  description={zero field hole mobility},
  sort={muop}, type=symbolslist_greek
}
\newglossaryentry{mu_0n(p)}%
{%
  name={\ensuremath{\mu_{\mathrm{0n(p)}}}},
  description={zero field electron (hole) mobility},
  sort={muonp}, type=symbolslist_greek
}
\newglossaryentry{mu_p}%
{%
  name={\ensuremath{\mu_{\mathrm{p}}}},
  description={hole mobility},
  sort={mup}, type=symbolslist_greek
}
\newglossaryentry{mu_n(p)}%
{%
  name={\ensuremath{\mu_{\mathrm{n(p)}}}},
  description={electron (hole) mobility},
  sort={munp}, type=symbolslist_greek
}
\newglossaryentry{gamma}%
{%
  name={\ensuremath{\gamma}},
  description={field enhancement factor of poole-frenkel mobility model},
  sort={gamma}, type=symbolslist_greek
}
\newglossaryentry{omega}%
{%
  name={\ensuremath{\omega}},
  description={circular frequency},
  sort={omega}, type=symbolslist_greek
}
%---------TRANSIT TIMES
\newglossaryentry{tau_f}%
{%
  name={\ensuremath{\tau_{\mathrm{f}}}},
  description={forward storage time} ,
  sort={tauf}, type=symbolslist_greek
}
\newglossaryentry{tau_f0}%
{%
  name={\ensuremath{\tau_{\mathrm{f0}}}},
  description={low current transit time} ,
  sort={tauf0}, type=symbolslist_greek
}
\newglossaryentry{tau_Bf0}%
{%
  name={\ensuremath{\tau_{\mathrm{Bf0}}}},
  description={low current transit time due to the neutral base} ,
  sort={tauBf0}, type=symbolslist_greek
}
\newglossaryentry{tau_Cf0}%
{%
  name={\ensuremath{\tau_{\mathrm{Cf0}}}},
  description={low current transit time due to the neutral collector} ,
  sort={tauCf0}, type=symbolslist_greek
}
\newglossaryentry{tau_Ef0}%
{%
  name={\ensuremath{\tau_{\mathrm{Ef0}}}},
  description={low current transit time due to the neutral emitter} ,
  sort={tauEf0}, type=symbolslist_greek
}
\newglossaryentry{tau_BCf0}%
{%
  name={\ensuremath{\tau_{\mathrm{BCf0}}}},
  description={low current transit time due to the base collector junction region},
  sort={tauBCf0}, type=symbolslist_greek
}
\newglossaryentry{d_tau_f}%
{%
  name={\ensuremath{\Delta\tau_{\mathrm{f}}}},
  description={variable that models the increase of the transit time through a BJT with increasing current},
  sort={tauf}, type=symbolslist_greek
}
\newglossaryentry{d_tau_e}%
{%
  name={\ensuremath{\Delta\tau_{\mathrm{Ef}}}},
  description={variable that models the increase of the transit time through the emitter},
  sort={taufe}, type=symbolslist_greek
}
\newglossaryentry{d_tau_Bf}%
{%
  name={\ensuremath{\Delta\tau_{\mathrm{Bf}}}},
  description={variable that models the increase of the base transit time through a BJT with increasing current},
  sort={tauBf}, type=symbolslist_greek
}
\newglossaryentry{d_tau_Ef}%
{%
  name={\ensuremath{\Delta\tau_{\mathrm{Ef}}}},
  description={variable that models the increase of the emitter transit time through a BJT with increasing current},
  sort={tauEf}, type=symbolslist_greek
}
\newglossaryentry{d_tau_Cf}%
{%
  name={\ensuremath{\Delta\tau_{\mathrm{Cf}}}},
  description={variable that models the increase of the collector transit time through a BJT with increasing current},
  sort={tauCf}, type=symbolslist_greek
}
\newglossaryentry{tau_e}%
{%
  name={\ensuremath{\tau_{\mathrm{e}}}},
  description={forward storage time of the neutral emitter region} ,
  sort={taue}, type=symbolslist_greek
}
\newglossaryentry{tau_b}%
{%
  name={\ensuremath{\tau_{\mathrm{b}}}},
  description={forward storage time of the neutral base region} ,
  sort={taub}, type=symbolslist_greek
}
\newglossaryentry{alpha_t}%
{%
  name={\ensuremath{\alpha_{\mathrm{t}}}},
  description={base transport factor} ,
  sort={alphat}, type=symbolslist_greek
}
\newglossaryentry{tau_c}%
{%
  name={\ensuremath{\tau_{\mathrm{C}}}},
  description={forward storage time of the neutral collector region} ,
  sort={tauc}, type=symbolslist_greek
}
\newglossaryentry{tau_be}%
{%
  name={\ensuremath{\tau_{\mathrm{be}}}},
  description={forward storage time of the base-emitter junction region} ,
  sort={taube}, type=symbolslist_greek
}
\newglossaryentry{tau_bc}%
{%
  name={\ensuremath{\tau_{\mathrm{bc}}}},
  description={forward storage time of the base-collector junction region} ,
  sort={taubc}, type=symbolslist_greek
}



