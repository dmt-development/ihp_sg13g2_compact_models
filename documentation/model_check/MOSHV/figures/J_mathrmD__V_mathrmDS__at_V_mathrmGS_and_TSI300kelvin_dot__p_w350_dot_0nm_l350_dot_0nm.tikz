\begin{tikzpicture}[font=\normalsize,trim axis left, trim axis right,tight background]
\pgfplotsset{every axis/.append style={very thick},compat=1.5},
\definecolor{color0}{rgb}{0.00000, 0.39216, 0.00000}
\definecolor{color1}{rgb}{0.00000, 0.00000, 0.54510}
\definecolor{color2}{rgb}{0.69020, 0.18824, 0.37647}
\definecolor{color3}{rgb}{1.00000, 0.00000, 0.00000}
\definecolor{color4}{rgb}{0.58039, 0.40392, 0.74118}
\definecolor{color5}{rgb}{0.87059, 0.72157, 0.52941}
\definecolor{color6}{rgb}{0.00000, 1.00000, 0.00000}
\definecolor{color7}{rgb}{0.00000, 1.00000, 1.00000}
\definecolor{color8}{rgb}{1.00000, 0.00000, 1.00000}
\definecolor{color9}{rgb}{0.39216, 0.58431, 0.92941}

\begin{axis}[scale only axis,ytick pos=left,
xlabel={$-V_{\mathrm{DS}} ( \si{\volt} )$},
ylabel={$-J_{\mathrm{D}} ( \si{\milli\ampere\per\micro\meter} )$},
% xmin=0,
% xmax=0,
% restrict x to domain=0:1,
log basis x=10,
% ymin=0,
% ymax=0,
% restrict y to domain=0:1,
log basis y=10,
xlabel shift = -5 pt,
 ylabel shift = -5 pt,
xmajorgrids,
enlargelimits=false,
scaled ticks=true,
ymajorgrids,
x tick style={color=black},
y tick style={color=black},
x grid style={white!69.01960784313725!black},
y grid style={white!69.01960784313725!black},
/tikz/mark repeat=2,
legend style={at={(1.02,1.00)}, anchor=north west,legend cell align=left, align=left},
]
\addplot [color=color0, solid, mark=x, mark options={solid}, mark phase=1, ]
  table[row sep=crcr, x expr=\thisrowno{0}*1.000000e+00, y expr=\thisrowno{1}*1.000000e-03]{
-0 -6.73572e-06\\
0.05 8.82463\\
0.1 16.7623\\
0.15 23.752\\
0.2 29.7368\\
0.25 34.6892\\
0.3 38.6362\\
0.35 41.6696\\
0.4 43.9332\\
0.45 45.5919\\
0.5 46.8013\\
0.55 47.69\\
0.6 48.3547\\
0.65 48.8638\\
0.7 49.2643\\
0.75 49.588\\
0.8 49.8563\\
0.85 50.0838\\
0.9 50.2805\\
0.95 50.4536\\
1 50.6081\\
1.05 50.7479\\
1.1 50.8755\\
1.15 50.9932\\
1.2 51.1026\\
1.25 51.2049\\
1.3 51.3011\\
1.35 51.3921\\
1.4 51.4785\\
1.45 51.5608\\
1.5 51.6396\\
1.55 51.7152\\
1.6 51.788\\
1.65 51.8581\\
1.7 51.9259\\
1.75 51.9915\\
1.8 52.0552\\
1.85 52.1171\\
1.9 52.1774\\
1.95 52.2361\\
2 52.2935\\
};
\addlegendentry{$V_{\mathrm{BE}}=\SI{-1.00}{\volt}$}
\addplot [color=color1, solid, mark=+, mark options={solid}, mark phase=1, ]
  table[row sep=crcr, x expr=\thisrowno{0}*1.000000e+00, y expr=\thisrowno{1}*1.000000e-03]{
-0 -5.97693e-06\\
0.05 8.42593\\
0.1 15.9456\\
0.15 22.4974\\
0.2 28.0274\\
0.25 32.5194\\
0.3 36.0207\\
0.35 38.6477\\
0.4 40.563\\
0.45 41.9396\\
0.5 42.9301\\
0.55 43.6531\\
0.6 44.1938\\
0.65 44.61\\
0.7 44.9403\\
0.75 45.2099\\
0.8 45.4359\\
0.85 45.6296\\
0.9 45.7989\\
0.95 45.9493\\
1 46.0847\\
1.05 46.2081\\
1.1 46.3216\\
1.15 46.4269\\
1.2 46.5252\\
1.25 46.6177\\
1.3 46.705\\
1.35 46.7879\\
1.4 46.8668\\
1.45 46.9423\\
1.5 47.0147\\
1.55 47.0844\\
1.6 47.1515\\
1.65 47.2164\\
1.7 47.2793\\
1.75 47.3402\\
1.8 47.3994\\
1.85 47.4571\\
1.9 47.5133\\
1.95 47.5681\\
2 47.6217\\
};
\addlegendentry{$V_{\mathrm{BE}}=\SI{-0.97}{\volt}$}
\addplot [color=color2, solid, mark=triangle, mark options={solid, rotate=180}, mark phase=1, ]
  table[row sep=crcr, x expr=\thisrowno{0}*1.000000e+00, y expr=\thisrowno{1}*1.000000e-03]{
-0 -5.2853e-06\\
0.05 8.01614\\
0.1 15.107\\
0.15 21.2108\\
0.2 26.2786\\
0.25 30.3087\\
0.3 33.3722\\
0.35 35.6109\\
0.4 37.2043\\
0.45 38.3286\\
0.5 39.1287\\
0.55 39.711\\
0.6 40.148\\
0.65 40.4873\\
0.7 40.7595\\
0.75 40.9846\\
0.8 41.1755\\
0.85 41.341\\
0.9 41.4872\\
0.95 41.6183\\
1 41.7374\\
1.05 41.8467\\
1.1 41.9478\\
1.15 42.0422\\
1.2 42.1308\\
1.25 42.2144\\
1.3 42.2937\\
1.35 42.3693\\
1.4 42.4414\\
1.45 42.5106\\
1.5 42.5772\\
1.55 42.6413\\
1.6 42.7032\\
1.65 42.7632\\
1.7 42.8214\\
1.75 42.8779\\
1.8 42.9329\\
1.85 42.9865\\
1.9 43.0388\\
1.95 43.0898\\
2 43.1398\\
};
\addlegendentry{$V_{\mathrm{BE}}=\SI{-0.94}{\volt}$}
\addplot [color=color3, solid, mark=triangle, mark options={solid, rotate=0}, mark phase=1, ]
  table[row sep=crcr, x expr=\thisrowno{0}*1.000000e+00, y expr=\thisrowno{1}*1.000000e-03]{
-0 -4.65664e-06\\
0.05 7.5953\\
0.1 14.2468\\
0.15 19.8935\\
0.2 24.4934\\
0.25 28.0632\\
0.3 30.7014\\
0.35 32.5753\\
0.4 33.877\\
0.45 34.7802\\
0.5 35.4183\\
0.55 35.8833\\
0.6 36.2351\\
0.65 36.5115\\
0.7 36.7363\\
0.75 36.9247\\
0.8 37.0866\\
0.85 37.2286\\
0.9 37.3554\\
0.95 37.4701\\
1 37.575\\
1.05 37.672\\
1.1 37.7624\\
1.15 37.8471\\
1.2 37.927\\
1.25 38.0027\\
1.3 38.0747\\
1.35 38.1435\\
1.4 38.2094\\
1.45 38.2728\\
1.5 38.3339\\
1.55 38.3928\\
1.6 38.4499\\
1.65 38.5052\\
1.7 38.559\\
1.75 38.6113\\
1.8 38.6622\\
1.85 38.7119\\
1.9 38.7605\\
1.95 38.808\\
2 38.8545\\
};
\addlegendentry{$V_{\mathrm{BE}}=\SI{-0.91}{\volt}$}
\addplot [color=color4, solid, mark=asterisk, mark options={solid}, mark phase=1, ]
  table[row sep=crcr, x expr=\thisrowno{0}*1.000000e+00, y expr=\thisrowno{1}*1.000000e-03]{
-0 -4.08688e-06\\
0.05 7.16357\\
0.1 13.3657\\
0.15 18.5471\\
0.2 22.6758\\
0.25 25.7913\\
0.3 28.0223\\
0.35 29.5598\\
0.4 30.6028\\
0.45 31.3169\\
0.5 31.8202\\
0.55 32.1893\\
0.6 32.472\\
0.65 32.6975\\
0.7 32.8837\\
0.75 33.0421\\
0.8 33.18\\
0.85 33.3023\\
0.9 33.4126\\
0.95 33.5133\\
1 33.606\\
1.05 33.6923\\
1.1 33.773\\
1.15 33.8491\\
1.2 33.9211\\
1.25 33.9896\\
1.3 34.055\\
1.35 34.1176\\
1.4 34.1778\\
1.45 34.2358\\
1.5 34.2917\\
1.55 34.3458\\
1.6 34.3983\\
1.65 34.4492\\
1.7 34.4988\\
1.75 34.5471\\
1.8 34.5941\\
1.85 34.6401\\
1.9 34.6851\\
1.95 34.7291\\
2 34.7722\\
};
\addlegendentry{$V_{\mathrm{BE}}=\SI{-0.88}{\volt}$}
\addplot [color=color5, solid, mark=triangle, mark options={solid, rotate=270}, mark phase=1, ]
  table[row sep=crcr, x expr=\thisrowno{0}*1.000000e+00, y expr=\thisrowno{1}*1.000000e-03]{
-0 -3.5721e-06\\
0.05 6.72128\\
0.1 12.4648\\
0.15 17.1743\\
0.2 20.8316\\
0.25 23.5037\\
0.3 25.3515\\
0.35 26.5856\\
0.4 27.4052\\
0.45 27.9615\\
0.5 28.355\\
0.55 28.6471\\
0.6 28.8746\\
0.65 29.0592\\
0.7 29.2141\\
0.75 29.3479\\
0.8 29.4658\\
0.85 29.5716\\
0.9 29.6678\\
0.95 29.7563\\
1 29.8384\\
1.05 29.9152\\
1.1 29.9874\\
1.15 30.0557\\
1.2 30.1206\\
1.25 30.1825\\
1.3 30.2417\\
1.35 30.2986\\
1.4 30.3534\\
1.45 30.4062\\
1.5 30.4574\\
1.55 30.5069\\
1.6 30.555\\
1.65 30.6018\\
1.7 30.6473\\
1.75 30.6917\\
1.8 30.7351\\
1.85 30.7774\\
1.9 30.8189\\
1.95 30.8595\\
2 30.8994\\
};
\addlegendentry{$V_{\mathrm{BE}}=\SI{-0.85}{\volt}$}
\addplot [color=color6, solid, mark=triangle, mark options={solid, rotate=90}, mark phase=1, ]
  table[row sep=crcr, x expr=\thisrowno{0}*1.000000e+00, y expr=\thisrowno{1}*1.000000e-03]{
-0 -3.10853e-06\\
0.05 6.26895\\
0.1 11.5455\\
0.15 15.7788\\
0.2 18.9684\\
0.25 21.214\\
0.3 22.7087\\
0.35 23.6765\\
0.4 24.3082\\
0.45 24.7364\\
0.5 25.0424\\
0.55 25.2736\\
0.6 25.4572\\
0.65 25.6091\\
0.7 25.7387\\
0.75 25.8522\\
0.8 25.9534\\
0.85 26.0451\\
0.9 26.1292\\
0.95 26.2071\\
1 26.2798\\
1.05 26.348\\
1.1 26.4125\\
1.15 26.4737\\
1.2 26.5321\\
1.25 26.5879\\
1.3 26.6415\\
1.35 26.693\\
1.4 26.7427\\
1.45 26.7908\\
1.5 26.8374\\
1.55 26.8825\\
1.6 26.9264\\
1.65 26.9692\\
1.7 27.0109\\
1.75 27.0516\\
1.8 27.0913\\
1.85 27.1302\\
1.9 27.1683\\
1.95 27.2057\\
2 27.2424\\
};
\addlegendentry{$V_{\mathrm{BE}}=\SI{-0.82}{\volt}$}
\addplot [color=color7, solid, mark=*, mark options={solid, fill}, mark phase=1, ]
  table[row sep=crcr, x expr=\thisrowno{0}*1.000000e+00, y expr=\thisrowno{1}*1.000000e-03]{
-0 -2.69254e-06\\
0.05 5.80736\\
0.1 10.6103\\
0.15 14.3656\\
0.2 17.0962\\
0.25 18.9389\\
0.3 20.1165\\
0.35 20.8575\\
0.4 21.3361\\
0.45 21.6627\\
0.5 21.9004\\
0.55 22.084\\
0.6 22.2329\\
0.65 22.3585\\
0.7 22.4674\\
0.75 22.5641\\
0.8 22.6512\\
0.85 22.7308\\
0.9 22.8044\\
0.95 22.8728\\
1 22.9371\\
1.05 22.9977\\
1.1 23.0552\\
1.15 23.1099\\
1.2 23.1622\\
1.25 23.2124\\
1.3 23.2606\\
1.35 23.3071\\
1.4 23.3521\\
1.45 23.3956\\
1.5 23.4378\\
1.55 23.4788\\
1.6 23.5187\\
1.65 23.5576\\
1.7 23.5956\\
1.75 23.6327\\
1.8 23.6689\\
1.85 23.7045\\
1.9 23.7393\\
1.95 23.7735\\
2 23.807\\
};
\addlegendentry{$V_{\mathrm{BE}}=\SI{-0.79}{\volt}$}
\addplot [color=color8, solid, mark=square, mark options={solid}, mark phase=1, ]
  table[row sep=crcr, x expr=\thisrowno{0}*1.000000e+00, y expr=\thisrowno{1}*1.000000e-03]{
-0 -2.32066e-06\\
0.05 5.33763\\
0.1 9.66217\\
0.15 12.9414\\
0.2 15.2278\\
0.25 16.6986\\
0.3 17.6\\
0.35 18.1541\\
0.4 18.5118\\
0.45 18.7599\\
0.5 18.9449\\
0.55 19.0915\\
0.6 19.2131\\
0.65 19.3174\\
0.7 19.4093\\
0.75 19.4917\\
0.8 19.5668\\
0.85 19.6359\\
0.9 19.7001\\
0.95 19.7602\\
1 19.8169\\
1.05 19.8705\\
1.1 19.9215\\
1.15 19.9703\\
1.2 20.0169\\
1.25 20.0618\\
1.3 20.105\\
1.35 20.1468\\
1.4 20.1872\\
1.45 20.2264\\
1.5 20.2644\\
1.55 20.3014\\
1.6 20.3375\\
1.65 20.3727\\
1.7 20.4071\\
1.75 20.4407\\
1.8 20.4736\\
1.85 20.5058\\
1.9 20.5374\\
1.95 20.5685\\
2 20.599\\
};
\addlegendentry{$V_{\mathrm{BE}}=\SI{-0.76}{\volt}$}
\addplot [color=color9, solid, mark=o, mark options={solid, fill}, mark phase=1, ]
  table[row sep=crcr, x expr=\thisrowno{0}*1.000000e+00, y expr=\thisrowno{1}*1.000000e-03]{
-0 -1.98957e-06\\
0.05 4.86123\\
0.1 8.70513\\
0.15 11.5147\\
0.2 13.3791\\
0.25 14.5162\\
0.3 15.1857\\
0.35 15.5915\\
0.4 15.8565\\
0.45 16.0451\\
0.5 16.1899\\
0.55 16.3076\\
0.6 16.4073\\
0.65 16.4943\\
0.7 16.5719\\
0.75 16.6422\\
0.8 16.7068\\
0.85 16.7666\\
0.9 16.8225\\
0.95 16.8751\\
1 16.9248\\
1.05 16.972\\
1.1 17.0171\\
1.15 17.0602\\
1.2 17.1016\\
1.25 17.1415\\
1.3 17.18\\
1.35 17.2172\\
1.4 17.2533\\
1.45 17.2883\\
1.5 17.3224\\
1.55 17.3556\\
1.6 17.3879\\
1.65 17.4195\\
1.7 17.4504\\
1.75 17.4807\\
1.8 17.5103\\
1.85 17.5393\\
1.9 17.5678\\
1.95 17.5959\\
2 17.6234\\
};
\addlegendentry{$V_{\mathrm{BE}}=\SI{-0.73}{\volt}$}
\addplot [color=color0, solid, mark=x, mark options={solid}, mark phase=1, ]
  table[row sep=crcr, x expr=\thisrowno{0}*1.000000e+00, y expr=\thisrowno{1}*1.000000e-03]{
-0 -1.69612e-06\\
0.05 4.38016\\
0.1 7.7445\\
0.15 10.0969\\
0.2 11.5695\\
0.25 12.4177\\
0.3 12.9003\\
0.35 13.1927\\
0.4 13.3884\\
0.45 13.5325\\
0.5 13.6465\\
0.55 13.7415\\
0.6 13.8235\\
0.65 13.8961\\
0.7 13.9616\\
0.75 14.0214\\
0.8 14.0768\\
0.85 14.1283\\
0.9 14.1767\\
0.95 14.2224\\
1 14.2657\\
1.05 14.307\\
1.1 14.3465\\
1.15 14.3844\\
1.2 14.4209\\
1.25 14.456\\
1.3 14.49\\
1.35 14.523\\
1.4 14.5549\\
1.45 14.586\\
1.5 14.6162\\
1.55 14.6457\\
1.6 14.6745\\
1.65 14.7026\\
1.7 14.7301\\
1.75 14.7571\\
1.8 14.7835\\
1.85 14.8095\\
1.9 14.8349\\
1.95 14.86\\
2 14.8847\\
};
\addlegendentry{$V_{\mathrm{BE}}=\SI{-0.70}{\volt}$}
\end{axis}

\end{tikzpicture}
