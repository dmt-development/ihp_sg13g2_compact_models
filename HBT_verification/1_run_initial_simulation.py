# this file runs an initial ngspice simulation using the IHP HBT models to see if they work in ngspice
from pathlib import Path
from DMT.config import COMMANDS

COMMANDS["OPENVAF"] = "openvaf"
from DMT.core import (
    DutType,
    specifiers,
    sub_specifiers,
    SimCon,
    Plot,
    MCard,
    save_or_show,
)

from DMT.ngspice import DutNgspice
from DMT.xyce import DutXyce
from DMT.extraction import get_sweep_bjt
from get_circuit_vbic import get_circuit_vbic_common_emitter
from mc_vbic import McVbic

repo_dir = Path(__file__).parent.parent

duts_circuit = [
    DutNgspice,
    DutXyce,
]
show = True  # If True: open plots and visualize them
force = (
    False  # If True: re-run simulations even if the simulation results already exist
)


# define a "sweep" object that defines the voltage and temperature sweeps in the simulator
sweep = get_sweep_bjt(
    vbe=[0.1, 1.1, 111], vbc=[0], ac=True, temp=300, freq=10e9, mode="lin"
)

# define a modelcard for the HBT
mc = MCard(
    ["C", "B", "E", "S"],
    "npn13G2",
    "common_emitter",
    1.0,
)

# define a circuit to be simulated
duts = []
for dut_circuit in duts_circuit:
    circuit_CE = get_circuit_vbic_common_emitter(
        1, mc=mc, flavor="HS", dut_class=dut_circuit
    )

    # define a device that combines all the previously defined objects
    dut = dut_circuit(
        None,
        DutType.npn,
        circuit_CE,
        nodes="C,B,E,S",
        reference_node="E",
    )

    dut.del_db()
    dut._modelcard = "npn13G2"
    duts.append(dut)

mc_va = McVbic()
# mc_va.set_values({"rth": 1e-3})
dut_va = DutNgspice(
    None,
    DutType.npn,
    mc_va,
    reference_node="E",
)
duts.append(dut_va)

# this object is used in DMT to control running simulations:
sim_con = SimCon(n_core=len(duts), t_max=30)
sim_con.append_simulation(dut=duts, sweep=sweep)
sim_con.run_and_read(force=force, remove_simulations=False)

# make sure that the simulation data contains derived quantities like F_T and C_BE
col_ic = specifiers.CURRENT + "C"
col_ib = specifiers.CURRENT + "B"
col_vc_f = specifiers.VOLTAGE + "C" + sub_specifiers.FORCED
col_vb_f = specifiers.VOLTAGE + "B" + sub_specifiers.FORCED
col_ft = specifiers.TRANSIT_FREQUENCY
col_vt = specifiers.VOLTAGE + "T"
col_fmax = specifiers.MAXIMUM_OSCILLATION_FREQUENCY
col_y11 = specifiers.SS_PARA_Y + ["B", "B"] + sub_specifiers.REAL
col_y21 = specifiers.SS_PARA_Y + ["C", "B"] + sub_specifiers.REAL
col_y12 = specifiers.SS_PARA_Y + ["B", "C"] + sub_specifiers.REAL
col_y22 = specifiers.SS_PARA_Y + ["C", "C"] + sub_specifiers.REAL
cols_y = [col_y11, col_y21, col_y12, col_y22]
col_cbe = specifiers.CAPACITANCE + ["B", "E"]
col_cbc = specifiers.CAPACITANCE + ["B", "C"]
dfs = []
area = 0.176e-6 * 1e-6
for dut in duts:
    for sweep_i in [sweep]:
        df = dut.get_data(sweep=sweep_i)
        for df_i in [df]:
            df_i.ensure_specifier_column(col_y11, ports=["B", "C"])
            df_i.ensure_specifier_column(col_y21, ports=["B", "C"])
            df_i.ensure_specifier_column(col_y12, ports=["B", "C"])
            df_i.ensure_specifier_column(col_y22, ports=["B", "C"])
            df_i.ensure_specifier_column(col_ft, ports=["B", "C"])
            df_i.ensure_specifier_column(col_fmax, ports=["B", "C"])
            df_i.ensure_specifier_column(col_cbe, ports=["B", "C"])
            df_i.ensure_specifier_column(col_cbc, ports=["B", "C"])

    df = dut.get_data(sweep=sweep)
    dfs.append(df)


# define plots
plt_gummel = Plot(
    r"gum",
    x_label=r"$V_{\mathrm{BE}} (\si{\volt})$",
    y_label=r"$J_{\mathrm{C}} ( \si{\milli\ampere\per\square\micro\meter} )$",
    y_log=True,
    style="mix",
    legend_location="upper left",
    y_scale=1e3 / 1e6 / 1e6,
)
plt_gummel.x_limits = (0.4, 1.1)
plt_gummel.y_limits = (1e-5, 1e2)
plt_t = Plot(
    r"dtj",
    x_label=r"$V_{\mathrm{BE}} (\si{\volt})$",
    y_label=r"$\Delta T_{\mathrm{j}} ( \si{\kelvin} )$",
    style="mix",
    legend_location="upper left",
    y_scale=1,
)
plts_y = []
for col_y in cols_y:
    plts_y.append(
        Plot(
            str(col_y),
            x_label=r"$V_{\mathrm{BE}} (\si{\volt})$",
            y_specifier=col_y,
            style="mix",
            legend_location="upper left",
        )
    )
    plts_y[-1].x_limits = (0.7, 1.1)

plt_ib = Plot(
    r"ib",
    x_label=r"$V_{\mathrm{BE}} (\si{\volt})$",
    y_label=r"$J_{\mathrm{B}} ( \si{\milli\ampere\per\square\micro\meter} )$",
    y_log=True,
    style="mix",
    legend_location="upper left",
    y_scale=1e3 / 1e6 / 1e6,
)
plt_ib.x_limits = (0.7, 1.1)
plt_ft = Plot(
    r"ft",
    x_label=r"$J_{\mathrm{C}} ( \si{\milli\ampere\per\square\micro\meter} )$",
    y_label=r"$f_{\mathrm{T}} ( \si{\giga\hertz} ) $",
    x_log=True,
    style="mix",
    legend_location="upper left",
    y_scale=1e-9,
    x_scale=1e3 / 1e6 / 1e6,
)
plt_ft.x_limits = (1e-5, 1e2)
plt_ft.y_limits = (0, 500)
plt_cbe = Plot(
    r"cbe",
    x_label=r"$V_{\mathrm{BE}} (\si{\volt})$",
    y_label=r"$C_{\mathrm{BE}} (\si{\femto\farad\per\square\micro\meter})$",
    style="mix",
    y_scale=1e15 / 1e6 / 1e6,
)
plt_cbe.x_limits = (0.7, 1.1)
plt_cbc = Plot(
    r"cbc",
    x_label=r"$V_{\mathrm{BE}} (\si{\volt})$",
    y_label=r"$C_{\mathrm{BC}} (\si{\femto\farad\per\square\micro\meter})$",
    style="mix",
    y_scale=1e15 / 1e6 / 1e6,
)
plt_cbc.x_limits = (0.7, 1.1)

plts = []
plts.append(plt_gummel)
plts.append(plt_t)
plts += plts_y
plts.append(plt_ib)
plts.append(plt_ft)
plts.append(plt_cbe)
plts.append(plt_cbc)

labels = {
    DutNgspice: "ngspice",
    DutXyce: "xyce",
    "OSDI": "OSDI",
}
for df, dut_circuit in zip(dfs, duts_circuit + ["OSDI"]):
    # normalize quantities to the ACTUAL emitter window area
    ic = df[col_ic].to_numpy() / area
    ib = df[col_ib].to_numpy() / area
    vb = df[col_vb_f].to_numpy()
    vt = df[col_vt].to_numpy()
    vc = df[col_vc_f].to_numpy()
    ft = df[col_ft].to_numpy()
    cbe = df[col_cbe].to_numpy() / area
    cbc = df[col_cbc].to_numpy() / area

    # add data to plots
    plt_gummel.add_data_set(vb, ic, label=labels[dut_circuit])
    plt_t.add_data_set(vb, vt, label=labels[dut_circuit])
    for plt, col_y in zip(plts_y, cols_y):
        plt.add_data_set(vb, df[col_y].to_numpy(), label=labels[dut_circuit])

    plt_ib.add_data_set(vb, ib, label=labels[dut_circuit])
    plt_cbe.add_data_set(vb, cbe, label=labels[dut_circuit])
    plt_cbc.add_data_set(vb, cbc, label=labels[dut_circuit])
    plt_ft.add_data_set(ic, ft, label=labels[dut_circuit])

# open the plots or save them as Tikz (if show=False)
save_or_show(
    plts=plts,
    show=show,
    location=str(repo_dir / "pictures" / "ihp_model_simulation_results"),
    fontsize="normalsize",
    line_width="thin",
    mark_repeat=1,
    width="3in",
    height="2in",
    restrict=False,
)
