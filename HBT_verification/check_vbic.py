import numpy as np
import os
import verilogae
from DMT.core import MCard
import copy

path_vbic = os.path.join("HBT_verification/vbic_4T_et_cf.va")
path_modelcard = os.path.join("model_files/HBT/modelcard_sg13g2.txt")


vbic = verilogae.load(path_vbic)


mc_sg13g2 = MCard(
        nodes_list = ["C","B","E"],
        default_subckt_name = "common_emitter",
        default_module_name = "vbic",
        va_file=path_vbic,
)
mc_sg13g2.load_model_parameters(path_modelcard)
mc_kwargs = mc_sg13g2.to_kwargs()
mc_kwargs = {k.upper():v for k,v in mc_kwargs.items()}

Vrth = 100
Vbei = 1.1
Vbe = 1.1
Vbc = 0.2
Vbci = 0.2
Vbex = 0.9
Vbcx = 0.2
Vrbp = 0.05
Vrs = 0.05
Vrcx = 0.05
Vrci = 0.1
Vrbi = 0.1
Vre = 0.05
Vcei = 0.7
Vbcp = -0.9
Vbep = 0.1
Vrbx = 0.01
temp = 300
        
kwargs_vae = {
    "temperature":temp,
    "voltages":{
        "br_dt":Vrth,
        "br_biei":Vbei,
        "br_bici":Vbci,
        "br_bxei":Vbex,
        "br_bicx":Vbcx,
        "br_bxbp":Vbep,
        "br_sibp":Vbcp,
        "br_ccx":Vrcx,
        "br_cxci":Vrci,
        "br_bbx":Vrbx,
        "br_bxbi":Vrbi,
        "br_bpcx":Vrbp,
        "br_eei":Vre,
        "br_ssi":Vrs,
        "br_ciei":Vbei-Vbci,
        "br_bxsi":0,
    },
}

# works: Itzf, Qbe, Ire
# deviations: 
# - Irci:Irci_Vrth

for var in ["Itzf"]:
    voltages = vbic.functions[var].voltages
    val_0 = vbic.functions[var].eval(
        **kwargs_vae,
        **mc_kwargs,
    )
    print("value of " + var + " {0:2.5e}".format(val_0))
    dv = 1
    for voltage_i in voltages:
        kwargs_vae_der = copy.deepcopy(kwargs_vae)
        kwargs_vae_der["voltages"][voltage_i] = kwargs_vae_der["voltages"][voltage_i] + dv
        val_der = (
            vbic.functions[var].eval(
                **kwargs_vae_der,
                **mc_kwargs,
            ) - val_0
        )/dv
        print("value of " + var + " der " + voltage_i + " {0:2.5e}".format(val_der))

    print("\n\n")

qm = vbic


