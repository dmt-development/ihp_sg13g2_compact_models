""" Author: Markus Müller
Purpose: Verify model against measurements and generate PyLatex document.
"""
from pathlib import Path
import numpy as np
import copy
from DMT.core import (
    MCard,
    specifiers,
    sub_specifiers,
    Plot,
    DutType,
    SimCon,
)
from DMT.extraction import get_sweep_bjt
import copy

from pylatex import Section, Figure, NoEscape
from pylatex.base_classes import Arguments
from pylatexenc.latex2text import LatexNodes2Text

from DMT.external import SubFile, CommandInputTikz, CommandLabel

from DMT.ngspice import DutNgspice
from DMT.xyce import DutXyce

dut_circuit = DutNgspice
dut_circuit = DutXyce

from get_circuit_vbic import get_circuit_vbic_common_emitter

force = False  # if True: force re-simulation

simulator = {DutXyce: "xyce", DutNgspice: "ngspice"}[dut_circuit]

fig_dir = (
    Path(__file__).parent.parent
    / "documentation"
    / "model_check"
    / f"HBT_{simulator}"
    / "figures"
).resolve()

width = None  # width of figures
dmt = SimCon(t_max=50, n_core=4)  # simulation controller
spot_freq = 20e9  # spot frequency for HF plots

# a section for each of these transistor flavors is generated
flavors = ["HS", "HSL", "HV"]

# define a modelcard for the HBT
mc = MCard(
    ["C", "B", "E", "S"],
    "npn13G2",
    "common_emitter",
    1.0,
)

# area from model file (=mask size?) (not actual device), used for normalization of currents
areas = {
    "HS": 120e-9 * 960e-9,
    "HSL": 120e-9 * 2500e-9,
    "HV": 120e-9 * 2500e-9,
}

# define the sweeps that will be simulated
sweeps_dc = {
    "300_gum_vce": get_sweep_bjt(
        name="300_gum_vce",
        vbe=np.linspace(0.1, 0.93, 84),
        vce=[0.5, 1, 1.5],
        ac=False,
        temp=300,
        mode="list",
    ),
    "300_beta_vce": get_sweep_bjt(
        name="300_beta_vce",
        vbe=np.linspace(0.1, 0.93, 84),
        vce=[0.5, 1, 1.5],
        ac=False,
        temp=300,
        mode="list",
    ),
    "300_gum_vbc": get_sweep_bjt(
        name="300_gum_vbc",
        vbe=np.linspace(0.1, 0.93, 84),
        vbc=[-0.5, 0, 0.5],
        ac=False,
        temp=300,
        mode="list",
    ),
    "300_out_vbe": get_sweep_bjt(
        name="300_out_vbe",
        vce=np.linspace(0, 1.5, 51),
        vbe=np.linspace(0.82, 0.92, 6),
        ac=False,
        temp=300,
        mode="list",
    ),
}

sweeps_ac = {
    "300_ft_ic_at_vce": get_sweep_bjt(
        name="300_ft_ic_at_vce",
        vbe=np.linspace(0.1, 1.1, 101),
        vce=[0.5, 1, 1.1],
        ac=True,
        freq=10e9,
        temp=300,
        mode="list",
    ),
    "300_fmax_ic_at_vce": get_sweep_bjt(
        name="300_fmax_ic_at_vce",
        vbe=np.linspace(0.1, 1.1, 101),
        vce=[0.5, 1, 1.1],
        ac=True,
        freq=10e9,
        temp=300,
        mode="list",
    ),
    "300_MSG_ic_at_vce": get_sweep_bjt(
        name="300_MSG_ic_at_vce",
        vbe=np.linspace(0.1, 1.1, 101),
        vce=[0.5, 1, 1.1],
        ac=True,
        freq=10e9,
        temp=300,
        mode="list",
    ),
    "300_ReY21_ic_at_vce": get_sweep_bjt(
        name="300_ReY21_ic_at_vce",
        vbe=np.linspace(0.1, 1.1, 101),
        vce=[0.5, 1, 1.1],
        ac=True,
        freq=10e9,
        temp=300,
        mode="list",
    ),
}


sweeps_T = []
for T in np.linspace(250, 400, 5):
    pass
    sweeps_T.append(
        get_sweep_bjt(
            name="300_ft_ic_at_T" + str(T),
            vbe=np.linspace(0.1, 1.1, 101),
            vce=[1],
            ac=True,
            freq=10e9,
            temp=T,
            mode="list",
        )
    )


# define the Plot objects for the sweeps
plts = {
    "300_gum_vce": Plot(
        r"$J_{\mathrm{C}}(V_{\mathrm{BE}})$ at $V_{\mathrm{CE}}$ and $T=\SI{300}{\kelvin}$.",
        x_label=r"$V_{\mathrm{BE}} ( \si{\volt} )$",
        y_label=r"$J_{\mathrm{C}} ( \si{\milli\ampere\per\square\micro\meter} )$",
        y_scale=1e3 / 1e12,
        y_log=True,
        legend_location="upper left",
    ),
    "300_beta_vce": Plot(
        r"$\beta ( J_{\mathrm{C}} )$ at $V_{\mathrm{CE}}$ and $T=\SI{300}{\kelvin}$.",
        y_label=r"$\beta$",
        x_label=r"$J_{\mathrm{C}} ( \si{\milli\ampere\per\square\micro\meter} )$",
        x_scale=1e3 / 1e12,
        x_log=True,
        legend_location="upper left",
    ),
    "300_gum_vbc": Plot(
        r"$J_{\mathrm{C}} (V_{\mathrm{BE}})$ at $V_{\mathrm{BC}}$ and $T=\SI{300}{\kelvin}$.",
        x_label=r"$V_{\mathrm{BE}} ( \si{\volt} )$",
        y_label=r"$J_{\mathrm{C}} ( \si{\milli\ampere\per\square\micro\meter} )$",
        y_scale=1e3 / 1e12,
        y_log=True,
        legend_location="upper left",
    ),
    "300_out_vbe": Plot(
        r"$J_{\mathrm{C}} ( V_{\mathrm{CE}} )$ at $V_{\mathrm{BE}}$ and $T=\SI{300}{\kelvin}$.",
        x_label=r"$V_{\mathrm{CE}} ( \si{\volt} )$",
        y_label=r"$J_{\mathrm{C}} ( \si{\milli\ampere\per\square\micro\meter} )$",
        y_scale=1e3 / 1e12,
        legend_location="upper left",
    ),
    "300_ft_ic_at_vce": Plot(
        r"$f_{\mathrm{T}} ( J_{\mathrm{C}} )$ at $V_{\mathrm{CE}}$ and $T=\SI{300}{\kelvin}$.",
        x_label=r"$J_{\mathrm{C}} ( \si{\milli\ampere\per\square\micro\meter} )$",
        x_scale=1e3 / 1e12,
        y_label=r"$f_{\mathrm{T}} ( \si{\giga\hertz} )$",
        y_scale=1e-9,
        legend_location="upper left",
        x_log=True,
    ),
    "300_fmax_ic_at_vce": Plot(
        r"$f_{\mathrm{max}} ( J_{\mathrm{C}} )$ at $V_{\mathrm{CE}}$ and $T=\SI{300}{\kelvin}$.",
        x_label=r"$J_{\mathrm{C}} ( \si{\milli\ampere\per\square\micro\meter} )$",
        x_scale=1e3 / 1e12,
        y_label=r"$f_{\mathrm{max}} ( \si{\giga\hertz} )$",
        y_scale=1e-9,
        legend_location="upper left",
        x_log=True,
    ),
    "300_MSG_ic_at_vce": Plot(
        r"$\mathrm{MSG} ( J_{\mathrm{C}} )$ at $V_{\mathrm{CE}}$ and $T=\SI{300}{\kelvin}$.",
        x_label=r"$J_{\mathrm{C}} ( \si{\milli\ampere\per\square\micro\meter} )$",
        x_scale=1e3 / 1e12,
        y_label=r"$\mathrm{MSG} ( \si{\dB} )$",
        y_scale=1,
        legend_location="upper left",
        x_log=True,
    ),
    "300_ReY21_ic_at_vce": Plot(
        r"$\Re \left\{ \underline{Y}_{21} \right\} ( J_{\mathrm{C}} )$ at $V_{\mathrm{CE}}$ and $T=\SI{300}{\kelvin}$.",
        x_label=r"$J_{\mathrm{C}} ( \si{\milli\ampere\per\square\micro\meter} )$",
        x_scale=1e3 / 1e12,
        y_label=r"$\Re \left\{ \underline{Y}_{21} \right\} ( \si{\milli\siemens\per\square\micro\meter} )$",
        y_scale=1e3 / 1e12,
        legend_location="upper left",
        x_log=True,
        y_log=True,
    ),
    "ic_vbe_T": Plot(
        r"$J_{\mathrm{C}} (V_{\mathrm{BE}})$ at $V_{\mathrm{CE}}=\SI{1}{\volt}$ over $T$.",
        x_label=r"$V_{\mathrm{BE}} ( \si{\volt} )$",
        y_label=r"$J_{\mathrm{C}} ( \si{\milli\ampere} )$",
        y_scale=1e3 / 1e12,
        y_log=True,
        legend_location="upper left",
    ),
    "ft_ic_T": Plot(
        r"$f_{\mathrm{T}} ( J_{\mathrm{C}} )$ at $V_{\mathrm{CE}}=\SI{1}{\volt}$ over $T$.",
        x_label=r"$J_{\mathrm{C}} ( \si{\milli\ampere} )$",
        x_scale=1e3 / 1e12,
        y_label=r"$f_{\mathrm{T}} ( \si{\giga\hertz} )$",
        y_scale=1e-9,
        x_log=True,
        legend_location="upper left",
    ),
}

plts_T = ["ic_vbe_T", "ft_ic_T"]

# define some column names that represent electrical quantities
col_vbc = specifiers.VOLTAGE + "B" + "C" + sub_specifiers.FORCED
col_vbe = specifiers.VOLTAGE + "B" + "E" + sub_specifiers.FORCED
col_vce = specifiers.VOLTAGE + "C" + "E" + sub_specifiers.FORCED
col_ic = specifiers.CURRENT + "C"
col_ib = specifiers.CURRENT + "B"
col_ft = specifiers.TRANSIT_FREQUENCY
col_fmax = specifiers.MAXIMUM_OSCILLATION_FREQUENCY
col_msg = specifiers.MAXIMUM_STABLE_GAIN
col_rey21 = specifiers.SS_PARA_Y + "C" + "B" + sub_specifiers.REAL

# this object is used in DMT to control running simulations:
sim_con = SimCon(n_core=1, t_max=100)

# we generate a section in the document for every HBT flavor
for flavor in flavors:
    area = areas[flavor]

    # define a circuit to be simulated
    circuit_CE = get_circuit_vbic_common_emitter(
        1, mc=mc, flavor=flavor, dut_class=dut_circuit
    )

    save_names = {}

    # define a device that combines all the previously defined objects
    dut = dut_circuit(
        None,
        DutType.npn,
        circuit_CE,
        nodes="C,B,E,S",
        reference_node="E",
    )

    dut.del_db()
    dut._modelcard = "npn13G2"

    # run the simulations
    for sweep_name, sweep in sweeps_dc.items():
        sim_con.append_simulation(dut=dut, sweep=sweep)
    for sweep_name, sweep in sweeps_ac.items():
        sim_con.append_simulation(dut=dut, sweep=sweep)
    for sweep in sweeps_T:
        sim_con.append_simulation(dut=dut, sweep=sweep)
    sim_con.run_and_read(force=force, remove_simulations=False)

    # set the flavor string
    if flavor == "HS":
        model_tex_str = "VBIC NPN HS"
    elif flavor == "HSL":
        model_tex_str = "VBIC NPN HSL"
    elif flavor == "HV":
        model_tex_str = "VBIC NPN HV"

    # generate the DC plots
    for sweep_name, sweep in sweeps_dc.items():
        plt = copy.deepcopy(plts[sweep_name])
        df = dut.get_data(sweep=sweep)
        df.ensure_specifier_column(col_vbe, ports=["B", "C"])
        df.ensure_specifier_column(col_vbc, ports=["B", "C"])
        df.ensure_specifier_column(col_vce, ports=["B", "C"])

        # here, for every plot type, we define how the data gets plotted
        if sweep_name == "300_gum_vce":
            for _i_c, v_c, df_filtered in df.iter_unique_col(col_vce, decimals=3):
                ic = df_filtered[col_ic].to_numpy() / area
                vbe = df_filtered[col_vbe].to_numpy()
                label = r"$V_{{\mathrm{{CE}}}}=\SI{{{0:1.1f}}}{{\volt}}$".format(
                    np.real(v_c)
                )
                plt.add_data_set(np.real(vbe), np.real(ic), label=label)
            plt.x_limits = (0.6, 0.95)
            plt.y_limits = (1e-3, 50)
            if flavor == "HV":
                plt.y_limits = (1e-3, 20)

        elif sweep_name == "300_beta_vce":
            for _i_c, v_c, df_filtered in df.iter_unique_col(col_vce, decimals=3):
                ic = df_filtered[col_ic].to_numpy() / area
                ib = df_filtered[col_ib].to_numpy() / area
                beta = ic / ib
                vbe = df_filtered[col_vbe].to_numpy()
                label = r"$V_{{\mathrm{{CE}}}}=\SI{{{0:1.1f}}}{{\volt}}$".format(
                    np.real(v_c)
                )
                plt.add_data_set(np.real(ic), np.real(beta), label=label)
            plt.x_limits = (1e-3, 50)
            plt.y_limits = (0, 1500)
            if flavor == "HV":
                plt.x_limits = (1e-3, 20)

        elif sweep_name == "300_gum_vbc":
            for _i_c, v_bc, df_filtered in df.iter_unique_col(col_vbc, decimals=3):
                ic = df_filtered[col_ic].to_numpy() / area
                vbe = df_filtered[col_vbe].to_numpy()
                label = r"$V_{{\mathrm{{BC}}}}=\SI{{{0:1.1f}}}{{\volt}}$".format(
                    np.real(v_bc)
                )
                plt.add_data_set(np.real(vbe), np.real(ic), label=label)
            plt.x_limits = (0.6, 0.95)
            plt.y_limits = (1e-3, 50)
            if flavor == "HV":
                plt.y_limits = (1e-3, 20)

        elif sweep_name == "300_out_vbe":
            for _i_c, v_be, df_filtered in df.iter_unique_col(col_vbe, decimals=3):
                ic = df_filtered[col_ic].to_numpy() / area
                vce = df_filtered[col_vce].to_numpy()
                label = r"$V_{{\mathrm{{BE}}}}=\SI{{{0:1.2f}}}{{\volt}}$".format(
                    np.real(v_be)
                )
                plt.add_data_set(np.real(vce), np.real(ic), label=label)

            plt.x_limits = (0, 1.5)
            plt.y_limits = (-2, 20)
            if flavor == "HV":
                plt.y_limits = (-2, 12)

        else:
            raise IOError

        plt.num = plt.num + "_" + flavor
        plt_save_name = plt.save_tikz(
            fig_dir, width=width, mark_repeat=2, extension=r"tikz"
        )
        save_names[plt.name] = plt_save_name

    # generate the AC plots
    for sweep_name, sweep in sweeps_ac.items():
        plt = copy.deepcopy(plts[sweep_name])
        df = dut.get_data(sweep=sweep)
        df.ensure_specifier_column(col_vbe, ports=["B", "C"])
        df.ensure_specifier_column(col_vbc, ports=["B", "C"])
        df.ensure_specifier_column(col_vce, ports=["B", "C"])
        df.ensure_specifier_column(col_ft, ports=["B", "C"])
        df.ensure_specifier_column(col_fmax, ports=["B", "C"])
        df.ensure_specifier_column(col_msg, ports=["B", "C"])
        df.ensure_specifier_column(col_rey21, ports=["B", "C"])

        # here, for every plot type, we define how the data gets plotted
        if sweep_name == "300_ft_ic_at_vce":
            for _i_c, v_c, df_filtered in df.iter_unique_col(col_vce, decimals=3):
                ic = df_filtered[col_ic].to_numpy() / area
                ft = df_filtered[col_ft].to_numpy()
                label = r"$V_{{\mathrm{{CE}}}}=\SI{{{0:1.1f}}}{{\volt}}$".format(
                    np.real(v_c)
                )
                plt.add_data_set(np.real(ic), np.real(ft), label=label)
            plt.x_limits = (1e-3, 100)
            plt.y_limits = (0, 600)
            if flavor == "HV":
                plt.y_limits = (0, 150)
                plt.x_limits = (1e-3, 20)

        elif sweep_name == "300_fmax_ic_at_vce":
            for _i_c, v_c, df_filtered in df.iter_unique_col(col_vce, decimals=3):
                ic = df_filtered[col_ic].to_numpy() / area
                fmax = df_filtered[col_fmax].to_numpy()
                label = r"$V_{{\mathrm{{CE}}}}=\SI{{{0:1.1f}}}{{\volt}}$".format(
                    np.real(v_c)
                )
                plt.add_data_set(np.real(ic), np.real(fmax), label=label)
            plt.x_limits = (1e-3, 100)
            plt.y_limits = (0, 600)
            if flavor == "HV":
                plt.x_limits = (1e-3, 20)
                plt.y_limits = (0, 300)

        elif sweep_name == "300_MSG_ic_at_vce":
            for _i_c, v_c, df_filtered in df.iter_unique_col(col_vce, decimals=3):
                ic = df_filtered[col_ic].to_numpy() / area
                msg = df_filtered[col_msg].to_numpy()
                label = r"$V_{{\mathrm{{CE}}}}=\SI{{{0:1.1f}}}{{\volt}}$".format(
                    np.real(v_c)
                )
                plt.add_data_set(np.real(ic), 20 * np.log10(np.real(msg)), label=label)

            plt.x_limits = (1e-3, 100)
            plt.y_limits = (0, 50)

            if flavor == "HV":
                plt.x_limits = (1e-3, 20)

        elif sweep_name == "300_ReY21_ic_at_vce":
            for _i_c, v_c, df_filtered in df.iter_unique_col(col_vce, decimals=3):
                ic = df_filtered[col_ic].to_numpy() / area
                rey21 = df_filtered[col_rey21].to_numpy() / area
                label = r"$V_{{\mathrm{{CE}}}}=\SI{{{0:1.1f}}}{{\volt}}$".format(
                    np.real(v_c)
                )
                plt.add_data_set(np.real(ic), rey21, label=label)

            plt.x_limits = (1e-3, 100)
            plt.y_limits = (1e-2, 5e2)
            if flavor == "HV":
                plt.x_limits = (1e-3, 20)

        plt.num = plt.num + "_" + flavor
        plt_save_name = plt.save_tikz(
            fig_dir, width=width, mark_repeat=2, extension=r"tikz"
        )
        save_names[plt.name] = plt_save_name

    # generate the T plots
    for plt_name in plts_T:
        plt = copy.deepcopy(plts[plt_name])

        for sweep in sweeps_T:
            df = dut.get_data(sweep=sweep)
            df.ensure_specifier_column(col_vbe, ports=["B", "C"])
            df.ensure_specifier_column(col_vbc, ports=["B", "C"])
            df.ensure_specifier_column(col_vce, ports=["B", "C"])
            df.ensure_specifier_column(col_ft, ports=["B", "C"])
            df.ensure_specifier_column(col_fmax, ports=["B", "C"])
            df.ensure_specifier_column(col_msg, ports=["B", "C"])
            df.ensure_specifier_column(col_rey21, ports=["B", "C"])

            key = dut.get_sweep_key(sweep)
            T = dut.get_key_temperature(key)
            label = r"$T=\SI{{{0:1.1f}}}{{\kelvin}}$".format(T)

            # here, for every plot type, we define how the data gets plotted
            if plt_name == "ic_vbe_T":
                for _i_c, v_c, df_filtered in df.iter_unique_col(col_vce, decimals=3):
                    vbe = df_filtered[col_vbe].to_numpy()
                    ic = df_filtered[col_ic].to_numpy() / area
                    plt.add_data_set(np.real(vbe), np.real(ic), label=label)

                plt.x_limits = (0.4, 1)
                plt.y_limits = (1e-6, 100)
                if flavor == "HV":
                    plt.y_limits = (1e-6, 20)

            elif plt_name == "ft_ic_T":
                for _i_c, v_c, df_filtered in df.iter_unique_col(col_vce, decimals=3):
                    ft = df_filtered[col_ft].to_numpy()
                    ic = df_filtered[col_ic].to_numpy() / area
                    plt.add_data_set(np.real(ic), np.real(ft), label=label)

                plt.y_limits = (0, 700)
                plt.x_limits = (1e-3, 100)
                if flavor == "HV":
                    plt.y_limits = (0, 150)
                    plt.x_limits = (1e-3, 20)

        plt.num = plt.num + "_" + flavor
        plt_save_name = plt.save_tikz(
            fig_dir, width=width, mark_repeat=2, extension=r"tikz"
        )
        save_names[plt.name] = plt_save_name

    # create the Tex document
    doc = SubFile()
    title = "IHP SG13G2 " + model_tex_str + " Sanity Checks"
    with doc.create(Section(title)):
        doc.append(
            NoEscape(
                r"This section gives an overview of the DC Characteristics of the HBT unit cell. "
            )
        )
        doc.append(
            NoEscape(
                r"Next, some DC characteristics @$T=\SI{300}{\kelvin}$ are visualized."
            )
        )
        for sweep_name, _sweep in sweeps_dc.items():
            plt = plts[sweep_name]
            doc.append(NoEscape(r"\FloatBarrier "))
            with doc.create(Figure(position="h!")) as _plot:
                _plot.append(NoEscape(r"\centering"))
                _plot.append(
                    CommandInputTikz(
                        arguments=Arguments(
                            "width=0.6\\textwidth, height=0.4\\textheight",
                            str(Path("figures") / save_names[plt.name]),
                        )
                    )
                )
                _plot.add_caption(NoEscape(plt.name))
                _plot.append(
                    CommandLabel(
                        arguments=Arguments(LatexNodes2Text().latex_to_text(plt.name))
                    )
                )

        doc.append(NoEscape(r"\FloatBarrier "))

        doc.append(
            NoEscape(
                r"Next, some AC characteristics @$T=\SI{300}{\kelvin}$ are visualized."
            )
        )
        for sweep_name, _sweep in sweeps_ac.items():
            plt = plts[sweep_name]
            doc.append(NoEscape(r"\FloatBarrier "))
            with doc.create(Figure(position="h!")) as _plot:
                _plot.append(NoEscape(r"\centering"))
                _plot.append(
                    CommandInputTikz(
                        arguments=Arguments(
                            "width=0.6\\textwidth, height=0.4\\textheight",
                            str(Path("figures") / save_names[plt.name]),
                        )
                    )
                )
                _plot.add_caption(NoEscape(plt.name))
                _plot.append(
                    CommandLabel(
                        arguments=Arguments(LatexNodes2Text().latex_to_text(plt.name))
                    )
                )

        doc.append(NoEscape(r"\FloatBarrier "))

        doc.append(
            NoEscape(
                r"Next, some characteristics at different $T$ and $V_{\mathrm{CE}}=\SI{1}{\volt}$ are visualized."
            )
        )
        for plt_name in plts_T:
            plt = plts[plt_name]
            doc.append(NoEscape(r"\FloatBarrier "))
            with doc.create(Figure(position="h!")) as _plot:
                _plot.append(NoEscape(r"\centering"))
                _plot.append(
                    CommandInputTikz(
                        arguments=Arguments(
                            "width=0.6\\textwidth, height=0.4\\textheight",
                            str(Path("figures") / save_names[plt.name]),
                        )
                    )
                )
                _plot.add_caption(NoEscape(plt.name))
                _plot.append(
                    CommandLabel(
                        arguments=Arguments(LatexNodes2Text().latex_to_text(plt.name))
                    )
                )

        doc.append(NoEscape(r"\FloatBarrier "))

    doc.append(NoEscape(r"\FloatBarrier "))
    doc.append(NoEscape(r"\newpage "))

    doc.generate_tex(str(fig_dir.parent / ("HBT_model_check_flavor" + flavor)))
