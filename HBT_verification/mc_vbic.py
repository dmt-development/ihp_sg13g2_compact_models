from pathlib import Path
from DMT.core import MCard
from DMT.core.circuit import Circuit, RESISTANCE, CAPACITANCE, VOLTAGE, CircuitElement


class McVbic(MCard):
    def __init__(
        self,
        version=1.3,
        nodes_list=("C", "B", "E", "S", "T"),
        default_subckt_name="Q_VBIC",
        default_module_name="vbic_va",
        possible_groups=None,
        vae_module=None,
        **kwargs,
    ):
        if possible_groups is None:
            possible_groups = {}

        super().__init__(
            nodes_list,
            default_subckt_name,
            default_module_name,
            version=version,
            possible_groups=possible_groups,
            vae_module=vae_module,
            va_file=Path(__file__).parent / "vbic_4T_et_cf.va",
            **kwargs,
        )

        super().load_model_parameters(
            Path(__file__).parent / "modelcard_sg13g2.txt",
            force=False,
        )

    def get_circuit(self, **kwargs) -> Circuit:
        """Here the modelcard defines it's default simulation circuit.

        Parameters
        ----------
        use_build_in : {False, True}, optional
            Creates a circuit for the modelcard using the build-in model
        topology : optional
            If a model has multiple standard circuits, use the topology to differentiate between them..
        """
        # parasitic base, emitter and collector series resistors from metal and SMU connection
        try:
            rem = self["_rem"].value
            rbm = self["_rbm"].value
            rcm = self["_rcm"].value
        except KeyError:
            rem = 1e-3
            rbm = 1e-3
            rcm = 1e-3

        circuit_elements = []
        circuit_elements.append(
            CircuitElement(
                self.default_module_name,
                self.default_subckt_name,
                [f"n_{node.upper()}" for node in ["C", "B", "E", "S", "T"]],
                parameters=self,
            )
        )

        # Res from T node to ground
        circuit_elements.append(
            CircuitElement("R", "Rt", ["n_T", "0"], parameters=[("R", str(1e6))])
        )
        # BASE NODE CONNECTION #############
        # metal resistance between contact base point and real collector
        circuit_elements.append(
            CircuitElement(
                "R", "Rbm", ["n_B_FORCED", "n_B"], parameters=[("R", str(rbm))]
            )
        )
        # shorts for current measurement
        circuit_elements.append(
            CircuitElement(
                "Short",
                "I_B",
                ["n_BX", "n_B_FORCED"],
            )
        )
        # capacitance since AC already deembeded Rbm
        circuit_elements.append(
            CircuitElement(
                "C", "Cbm", ["n_B_FORCED", "n_B"], parameters=[("C", str(1e-6))]
            )
        )

        # COLLECTOR NODE CONNECTION #############
        circuit_elements.append(
            CircuitElement(
                "Short",
                "I_C",
                ["n_CX", "n_C_FORCED"],
            )
        )
        # metal resistance between contact collector point and real collector
        circuit_elements.append(
            CircuitElement(
                "R", "Rcm", ["n_C_FORCED", "n_C"], parameters=[("R", str(rcm))]
            )
        )
        # capacitance since AC already deembeded Rcm
        circuit_elements.append(
            CircuitElement(
                "C", "Ccm", ["n_C_FORCED", "n_C"], parameters=[("C", str(1e-6))]
            )
        )
        circuit_elements.append(
            CircuitElement(
                "Short",
                "I_S",
                ["n_SX", "n_S"],
            )
        )
        # EMITTER NODE CONNECTION #############
        circuit_elements.append(
            CircuitElement(
                "Short",
                "I_E",
                ["n_EX", "n_E_FORCED"],
            )
        )
        # metal resistance between contact emiter point and real emiter
        circuit_elements.append(
            CircuitElement(
                "R", "Rem", ["n_E_FORCED", "n_E"], parameters=[("R", str(rem))]
            )
        )
        # capacitance since AC already deembeded Rcm
        circuit_elements.append(
            CircuitElement(
                "C", "Cem", ["n_E_FORCED", "n_E"], parameters=[("C", str(1e-6))]
            )
        )
        # add sources and thermal resistance
        circuit_elements.append(
            CircuitElement(
                "V_Source",
                "V_B",
                ["n_BX", "0"],
                parameters=[("Vdc", "V_B"), ("Vac", "V_B_ac")],
            )
        )
        circuit_elements.append(
            CircuitElement(
                "V_Source",
                "V_C",
                ["n_CX", "0"],
                parameters=[("Vdc", "V_C"), ("Vac", "V_C_ac")],
            )
        )
        circuit_elements.append(
            CircuitElement(
                "V_Source",
                "V_E",
                ["n_EX", "0"],
                parameters=[("Vdc", "V_E"), ("Vac", "V_E_ac")],
            )
        )
        circuit_elements.append(
            CircuitElement(
                "V_Source",
                "V_S",
                ["n_SX", "0"],
                parameters=[("Vdc", "V_S"), ("Vac", "V_S_ac")],
            )
        )
        circuit_elements += [
            "V_B=0",
            "IB=0",
            "V_C=0",
            "V_S=0",
            "V_E=0",
            "ac_switch=0",
            "V_B_ac=1-ac_switch",
            "V_C_ac=ac_switch",
            "V_S_ac=0",
            "V_E_ac=0",
        ]
        circuit = Circuit(circuit_elements)
        circuit.standard_circuit = "common_emitter"
        return circuit
