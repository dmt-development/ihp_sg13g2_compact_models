from pathlib import Path
from DMT.core import Circuit, CircuitElement
from DMT.core.circuit import SUBCIRCUIT
from DMT.ngspice import DutNgspice
from DMT.xyce import DutXyce


def get_circuit_vbic_common_emitter(nx: int, mc=None, flavor="HS", dut_class=None):
    """This method generates the equivalent circuit for simulating the SG12G2 HBT devices in DMT, which then
    translates the circuit to ngspice.

    Arguments
    ---------
    nx : integer
        The number of parallel devices.
    mc : DMT.core.McParameterComposition
        The DMT modelcard that can hold some additional model parameters.
    flavor : "HS", "HSL" or "HV"
        Specify the HBT flavor that shall be simulated.
    dut_class : DMT.DutCircuit
        A circuit simulator class that will be used for simulation.

    Returns
    -------
    circuit : DMT.core.Circuit
        A DMT.Circuit object that can be used for simulation using the DMT circuit simulator interface.
    """
    repo_dir = Path(__file__).parent.parent

    # parasitic base, emitter and collector series resistors from metal and SMU connection
    if mc is not None:
        try:
            rem = mc["_rem"].value
            rbm = mc["_rbm"].value
            rcm = mc["_rcm"].value
        except KeyError:
            rem = 1e-3
            rbm = 1e-3
            rcm = 1e-3

    # check if flavor is valid
    if not flavor in ["HS", "HSL", "HV"]:
        raise IOError(
            "The HBT flavor "
            + str(flavor)
            + " is not valid. Only HS, HSL or HV supported."
        )

    # model instance
    circuit_elements = []
    if dut_class == DutXyce:
        element_type = SUBCIRCUIT
        circuit_elements.append(".include ./SG13G2_hbt_woStatistics.hsp.lib\n")
    elif dut_class == DutNgspice:
        element_type = SUBCIRCUIT
        circuit_elements.append(".include ./SG13G2_hbt_woStatistics.hsp.lib\n")
    else:
        raise IOError("Keyword argument dut_class not properly set.")

    hbt_subcircuit_names = {
        "HS": "npn13G2",
        "HSL": "npn13G2l",
        "HV": "npn13G2v",
    }

    circuit_elements.append(
        CircuitElement(
            element_type,
            "X1",
            [
                "n_C",
                "n_B",
                "n_E",
                "n_S",
                "n_T",
            ],  # this HBT has no T node
            parameters=[
                hbt_subcircuit_names[flavor],
                ("Nx", "{0:1.2e}".format(nx)),
                ("selft", "{0:1.2e}".format(1)),
            ],
        )
    )
    # Res from T node to ground
    circuit_elements.append(
        CircuitElement("R", "Rt", ["n_T", "0"], parameters=[("R", str(1e6))])
    )
    # BASE NODE CONNECTION #############
    # metal resistance between contact base point and real collector
    circuit_elements.append(
        CircuitElement("R", "Rbm", ["n_B_FORCED", "n_B"], parameters=[("R", str(rbm))])
    )
    # shorts for current measurement
    circuit_elements.append(
        CircuitElement(
            "Short",
            "I_B",
            ["n_BX", "n_B_FORCED"],
        )
    )
    # capacitance since AC already deembeded Rbm
    circuit_elements.append(
        CircuitElement("C", "Cbm", ["n_B_FORCED", "n_B"], parameters=[("C", str(1e-6))])
    )

    # COLLECTOR NODE CONNECTION #############
    circuit_elements.append(
        CircuitElement(
            "Short",
            "I_C",
            ["n_CX", "n_C_FORCED"],
        )
    )
    # metal resistance between contact collector point and real collector
    circuit_elements.append(
        CircuitElement("R", "Rcm", ["n_C_FORCED", "n_C"], parameters=[("R", str(rcm))])
    )
    # capacitance since AC already deembeded Rcm
    circuit_elements.append(
        CircuitElement("C", "Ccm", ["n_C_FORCED", "n_C"], parameters=[("C", str(1e-6))])
    )
    circuit_elements.append(
        CircuitElement(
            "Short",
            "I_S",
            ["n_SX", "n_S"],
        )
    )
    # EMITTER NODE CONNECTION #############
    circuit_elements.append(
        CircuitElement(
            "Short",
            "I_E",
            ["n_EX", "n_E_FORCED"],
        )
    )
    # metal resistance between contact emiter point and real emiter
    circuit_elements.append(
        CircuitElement("R", "Rem", ["n_E_FORCED", "n_E"], parameters=[("R", str(rem))])
    )
    # capacitance since AC already deembeded Rcm
    circuit_elements.append(
        CircuitElement("C", "Cem", ["n_E_FORCED", "n_E"], parameters=[("C", str(1e-6))])
    )
    # add sources and thermal resistance
    circuit_elements.append(
        CircuitElement(
            "V_Source",
            "V_B",
            ["n_BX", "0"],
            parameters=[("Vdc", "V_B"), ("Vac", "V_B_ac")],
        )
    )
    circuit_elements.append(
        CircuitElement(
            "V_Source",
            "V_C",
            ["n_CX", "0"],
            parameters=[("Vdc", "V_C"), ("Vac", "V_C_ac")],
        )
    )
    circuit_elements.append(
        CircuitElement(
            "V_Source",
            "V_E",
            ["n_EX", "0"],
            parameters=[("Vdc", "V_E"), ("Vac", "V_E_ac")],
        )
    )
    circuit_elements.append(
        CircuitElement(
            "V_Source",
            "V_S",
            ["n_SX", "0"],
            parameters=[("Vdc", "V_S"), ("Vac", "V_S_ac")],
        )
    )
    circuit_elements += [
        "V_B=0",
        "IB=0",
        "V_C=0",
        "V_S=0",
        "V_E=0",
        "ac_switch=0",
        "V_B_ac=1-ac_switch",
        "V_C_ac=ac_switch",
        "V_S_ac=0",
        "V_E_ac=0",
    ]

    pdk_parameter_file = {
        DutNgspice: repo_dir
        / "model_files"
        / "ngspice"
        / "SG13G2_hbt_woStatistics.hsp.lib",
        DutXyce: repo_dir / "model_files" / "xyce" / "SG13G2_hbt_woStatistics.hsp.lib",
    }[dut_class]

    circuit = Circuit(circuit_elements, lib_files=[pdk_parameter_file])
    circuit.standard_circuit = "common_emitter"
    return circuit
