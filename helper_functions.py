# some helper functions are put here so that the code in "create_model_vs_meas_document.py" becomes cleaner
# and better to read
import re
import copy
import numpy as np
from pathlib import Path
from DMT.core import MCard, DutType, specifiers, Plot
from DMT.ngspice import DutNgspice
from DMT.xyce import DutXyce
from DMT.extraction import get_sweep_mos, get_sweep_bjt
from MOS_verification.get_circuit_psp import get_circuit_psp
from HBT_verification.get_circuit_vbic import get_circuit_vbic_common_emitter


# define some columns for convenience
col_vd = specifiers.VOLTAGE + "D"
col_vc = specifiers.VOLTAGE + "C"
col_vb = specifiers.VOLTAGE + "B"
col_vg = specifiers.VOLTAGE + "G"
col_vs = specifiers.VOLTAGE + "S"
col_ve = specifiers.VOLTAGE + "E"
col_id = specifiers.CURRENT + "D"
col_ic = specifiers.CURRENT + "C"

col_vcb = specifiers.VOLTAGE + "C" + "B"
col_vbe = specifiers.VOLTAGE + "B" + "E"
col_vce = specifiers.VOLTAGE + "C" + "E"

col_ft = specifiers.TRANSIT_FREQUENCY


def get_dut_hbt(dut_circuit, dut_name, length, width, nx=1):
    """Function that generates a DMT.DutCircuit for HBT simulation according to the input.

    dut_circuit : DMT.DutCircuit
        A circuit simulator class that shall be instantiated.
    dut_name : str
        The name of the HBT. The HBT flavor is inferred automatically.
    length : float
        Emitter window length in [m].
    width : float
        Emitter window width in [m].

    Returns
    -------

    dut_cir : DMT.core.DutCircuit
        A DMT.DutCircuit object that can be used for MOSFET simulation

    """
    repo_dir = Path(__file__).parent
    flavor = "HS"

    # define a modelcard for the MOS
    mc = MCard(
        ["C", "B", "E", "S"],
        "npn13G2",
        "common_emitter",
        1.0,
    )
    # define circuit
    circuit_CE = get_circuit_vbic_common_emitter(
        nx, mc=mc, flavor=flavor, dut_class=dut_circuit
    )

    dut_cir = dut_circuit(
        None,
        DutType.npn,
        circuit_CE,
        nodes="C,B,E,S",
        list_copy=[
            {
                DutNgspice: repo_dir
                / "model_files"
                / "ngspice"
                / "SG13G2_hbt_woStatistics.hsp.lib",
                DutXyce: repo_dir
                / "model_files"
                / "xyce"
                / "SG13G2_hbt_woStatistics.hsp.lib",
            }[dut_circuit]
        ],
        reference_node="E",
        simulator_options={},
    )

    dut_cir.del_db()
    dut_cir._modelcard = "npn13G2"
    return dut_cir


def get_dut_mos(dut_circuit, nmos, hv, length, width, corner="mos_tt"):
    """Function that generates a DMT.DutCircuit for MOSFET simulation according to the input.

    dut_circuit : DMT.DutCircuit
        A circuit simulator class that shall be instantiated.
    nmos : Boolean
        If True, this is an nmos, else pmos.
    hv : Boolean
        If True, this is an HV device, else LV.
    length : float
        length in [m].
    width : float
        width in [m].

    Returns
    -------

    dut_cir : DMT.core.DutCircuit
        A DMT.DutCircuit object that can be used for MOSFET simulation

    """
    flavor = "LV"
    if hv:
        flavor = "HV"

    type_mos = "n"
    if not nmos:
        type_mos = "p"
    # define a modelcard for the MOS
    mc = MCard(
        ["G", "S", "D", "B"],
        "npn13G2",
        "common_source",
        1.0,
    )
    # define circuit
    circuit_CS = get_circuit_psp(
        circuit_type="common_source",
        modelcard=mc,
        corner=corner,
        flavor=flavor,
        type_mos=type_mos,
        length=length,
        width=width,
        dut_circuit=dut_circuit,
    )

    dut_cir = dut_circuit(
        None,
        DutType.n_mos,
        circuit_CS,
        nodes="D,G,S,B",
        reference_node="S",
        simulator_options={},
    )
    dut_cir.del_db()
    dut_cir._modelcard = "nmos13G2"
    return dut_cir


def get_sweep_def_hbt(dut, key, df, dut_name):
    """Function thhbtat returns a bunch of plotting and simulation information for the dataframe df, for HBT devices.

    Parameters
    ----------
    dut : DMT.core.DutView
        A DutView object that holds the measurement data for one DUT.
    key : str
        The name of the measurement.
    df : pd.DataFrame
        A pandas DataFrame that holds the measurement data.
    dut_name : str
        Name of the device.

    Returns
    -------
    sweeps : [DMT.core.Sweep]
        A list of sweeps to be simulated for the measurement.
    dfs : [pd.DataFrame]
        The incoming dataframe broken down into smaller dataframes where only one variable is swept.
    x_limits : (float,float)
        The x-axis limits for the plot.
    y_limits : (float, float)
        The y-axis limits for the plot.
    fix_var  : DMT.core.SpecifierStr
        The fixed variable for each line.
    fix_var2  : DMT.core.SpecifierStr
        The potential second fixed variable for each line.
    x_var : DMT.core.SpecifierStr
        The variable to be plotted on the x-axis for each line.
    y_var : DMT.core.SpecifierStr
        The variable to be plotted on the y-axis for each line.
    caption : DMT.core.SpecifierStr
        A human readable caption for each measurement.
    """
    temp = dut.get_key_temperature(key)
    y_limits = (None, None)
    x_limits = (None, None)
    sweeps, dfs = [], []

    if "fg_vcb0_RF" in key:
        sweep_var = col_vbe
        fix_var = col_vcb
        fix_var2 = None
        y_var = col_ic
        x_var = col_vbe
        vcb = 0
        x_limits = (0.4, 1)
        y_limits = (1e-6, 1e2)
        df.ensure_specifier_column(col_vcb)
        df.ensure_specifier_column(col_vbe)
        for _i, v_fixed, data_i in df.iter_unique_col(fix_var, 5):
            sweeps.append(
                get_sweep_bjt(
                    name="icvb_vcb",
                    vbc=v_fixed,
                    vbe=data_i[col_vbe].to_numpy(),
                    temp=temp,
                    ac=False,
                    mode="list",
                )
            )
            dfs.append(copy.deepcopy(data_i))

        caption = r"$J_{{\mathrm{{C}}}}$ vs. $V_{{\mathrm{{BE}}}}$ sweep at $V_{{\mathrm{{BC}}}}=\SI{{{0:1.2f}}}{{\volt}}$ at temperature $T=\SI{{{1:2.2f}}}{{\volt}}$".format(
            0, temp
        )
    elif "fg_vce_RF" in key:
        sweep_var = col_vbe
        fix_var = col_vce
        fix_var2 = None
        y_var = col_ic
        x_var = col_vbe
        vcb = 0
        x_limits = (0.4, 1)
        y_limits = (1e-6, 1e2)
        df.ensure_specifier_column(col_vce)
        df.ensure_specifier_column(col_vbe)
        for _i, v_fixed, data_i in df.iter_unique_col(fix_var, 5):
            sweeps.append(
                get_sweep_bjt(
                    name="icvb_vce",
                    vce=v_fixed,
                    vbe=data_i[col_vbe].to_numpy(),
                    temp=temp,
                    ac=False,
                    mode="list",
                )
            )
            dfs.append(copy.deepcopy(data_i))

        caption = r"$J_{{\mathrm{{C}}}}$ vs. $V_{{\mathrm{{BE}}}}$ sweep at $V_{{\mathrm{{CE}}}}$ at temperature $T=\SI{{{0:2.2f}}}{{\volt}}$".format(
            temp
        )
    elif "fo_vb_RF" in key:
        sweep_var = col_vce
        fix_var = col_vbe
        fix_var2 = None
        y_var = col_ic
        x_var = col_vce
        df.ensure_specifier_column(col_vce)
        df.ensure_specifier_column(col_vbe)
        df = df[df[fix_var] < 0.98]
        for _i, v_fixed, data_i in df.iter_unique_col(fix_var, 5):
            sweeps.append(
                get_sweep_bjt(
                    name="icvc_vbe",
                    vbe=v_fixed,
                    vce=data_i[col_vce].to_numpy(),
                    temp=temp,
                    ac=False,
                    mode="list",
                )
            )
            dfs.append(copy.deepcopy(data_i))

        caption = r"$J_{{\mathrm{{C}}}}$ vs. $V_{{\mathrm{{CE}}}}$ sweep at $V_{{\mathrm{{BE}}}}$ at temperature $T=\SI{{{0:2.2f}}}{{\volt}}$".format(
            temp
        )
    elif "spar_vcb" in key:
        sweep_var = col_vbe
        fix_var = col_vcb
        fix_var2 = None
        y_var = col_ft
        x_var = col_ic
        df.ensure_specifier_column(col_vcb)
        df.ensure_specifier_column(col_vbe)
        df = df[df[specifiers.FREQUENCY] == 30e9]
        for _i, v_fixed, data_i in df.iter_unique_col(fix_var, 5):
            sweeps.append(
                get_sweep_bjt(
                    name="icvb_vcb_ac",
                    vbc=-v_fixed,
                    # vce=1,
                    vbe=data_i[col_vbe].to_numpy(),
                    temp=temp,
                    ac=True,
                    freq=30e9,
                    mode="list",
                )
            )
            dfs.append(copy.deepcopy(data_i))

        caption = r"$f_{{\mathrm{{T}}}}$ vs. $J_{{\mathrm{{C}}}}$ sweep at $V_{{\mathrm{{CB}}}}$ at temperature $T=\SI{{{0:2.2f}}}{{\volt}}$".format(
            temp
        )

    elif "spar_vce" in key:
        sweep_var = col_vbe
        fix_var = col_vce
        fix_var2 = None
        y_var = col_ft
        x_var = col_ic
        df.ensure_specifier_column(col_vce)
        df.ensure_specifier_column(col_vbe)
        df = df[df[specifiers.FREQUENCY] == 30e9]
        df = df[df[col_vbe] < 0.98]
        vces = [
            0.4,
            0.8,
        ]
        for _i, v_fixed, data_i in df.iter_unique_col(fix_var, 5):
            sweeps.append(
                get_sweep_bjt(
                    name="icvb_vcb_ac",
                    vce=v_fixed,
                    vbe=data_i[col_vbe].to_numpy(),
                    temp=temp,
                    ac=True,
                    freq=30e9,
                    mode="list",
                )
            )
            dfs.append(copy.deepcopy(data_i))

        caption = r"$f_{{\mathrm{{T}}}}$ vs. $J_{{\mathrm{{C}}}}$ sweep at $V_{{\mathrm{{CE}}}}$ at temperature $T=\SI{{{0:2.2f}}}{{\volt}}$".format(
            temp
        )

    else:
        print("skipping key " + key)
        raise NotImplementedError

    return sweeps, dfs, x_limits, y_limits, fix_var, fix_var2, x_var, y_var, caption


def get_sweep_def_mos(dut, key, df, nmos, hv):
    """Function that returns a bunch of plotting and simulation information for the dataframe df, for MOSFET devices.

    Parameters
    ----------
    dut : DMT.core.DutView
        A DutView object that holds the measurement data for one DUT.
    key : str
        The name of the measurement.
    df : pd.DataFrame
        A pandas DataFrame that holds the measurement data.
    nmos : Boolean
        If True, this is an NMOS device, else PMOS.
    hv : Boolean
        If True, this is a HV device, else LV.

    Returns
    -------
    sweeps : [DMT.core.Sweep]
        A list of sweeps to be simulated for the measurement.
    dfs : [pd.DataFrame]
        The incoming dataframe broken down into smaller dataframes where only one variable is swept.
    x_limits : (float,float)
        The x-axis limits for the plot.
    y_limits : (float, float)
        The y-axis limits for the plot.
    fix_var  : DMT.core.SpecifierStr
        The fixed variable for each line.
    fix_var2  : DMT.core.SpecifierStr
        The potential second fixed variable for each line.
    y_var : DMT.core.SpecifierStr
        The variable to be plotted on the y-axis for each line.
    x_var : DMT.core.SpecifierStr
        The variable to be plotted on the x-axis for each line.
    caption : DMT.core.SpecifierStr
        A human readable caption for each measurement.
    """
    temp = dut.get_key_temperature(key)
    y_limits = (None, None)
    x_limits = (None, None)
    sweeps, dfs = [], []

    if "dc_idvd" in key:
        x_var = col_vd
        fix_var = col_vg
        fix_var2 = None
        y_var = col_id
        if hv:
            y_limits = (0, None)
            x_limits = (0, 3.5)
        vbs = np.unique(df[col_vb].to_numpy())
        for _i, v_fixed, data_i in df.iter_unique_col(fix_var, 5):
            sweeps.append(
                get_sweep_mos(
                    name="idvd",
                    vgs=v_fixed,
                    vds=data_i[col_vd].to_numpy(),
                    vbs=vbs,
                    temp=temp,
                    ac=False,
                    mode="list",
                )
            )
            dfs.append(copy.deepcopy(data_i))

        caption = r"$J_{{\mathrm{{D}}}}$ vs. $V_{{\mathrm{{DS}}}}$ sweep at $V_{{\mathrm{{BS}}}}=\SI{{{0:1.2f}}}{{\volt}}$ at temperature $T=\SI{{{1:2.2f}}}{{\volt}}$".format(
            vbs[0], temp
        )

    elif "dc_idvg" in key:
        x_var = col_vg
        fix_var = col_vd
        fix_var2 = col_vb
        y_var = col_id
        if hv:
            y_limits = (1e-6, 1)
            x_limits = (0.5, 3.5)
        else:
            y_limits = (1e-8, 0.1)
            x_limits = (0, 1.5)
        for _i, v_fixed, data_i in df.iter_unique_col(fix_var, 5):
            for _j, v_fixed2, data_j in data_i.iter_unique_col(fix_var2, 5):
                sweeps.append(
                    get_sweep_mos(
                        name="idvg",
                        vds=v_fixed,
                        vgs=data_j[col_vg].to_numpy(),
                        vbs=v_fixed2,
                        temp=temp,
                        ac=False,
                        mode="list",
                    )
                )
                dfs.append(copy.deepcopy(data_j))

        caption = r"$J_{{\mathrm{{D}}}}$ vs. $V_{{\mathrm{{GS}}}}$ sweep at fixed $(V_{{\mathrm{{DS}}}},\, V_{{\mathrm{{BS}}}})$ and temperature $T=\SI{{{0:2.2f}}}{{\volt}}$".format(
            temp
        )

    else:
        print("skipping key " + key)
        raise NotImplementedError()

    return sweeps, dfs, x_limits, y_limits, fix_var, fix_var2, x_var, y_var, caption


def get_mos_dimensions(dut_name):
    """Find the MOSFET dimensions from a given DUT name.

    Arguments:
    dut_name : str
        A string that is the name of the DUT to be analyzed for dimensions.

    Returns
    -------
    width : float
        The width of the transistor [m].
    length : float
        The length of the transistor in [m].
    """
    width_str = re.findall("_W([0-9,u]+)_", dut_name)[0].replace("u", ".")
    width = float(width_str) * 1e-6
    length_str = re.findall("_L([0-9,u]+)_", dut_name)[0].replace("u", ".")
    length = float(length_str) * 1e-6
    return width, length


def get_hbt_dimensions(dut_name):
    """Find the HBT dimensions from a given DUT name.

    Arguments:
    dut_name : str
        A string that is the name of the DUT to be analyzed for dimensions.

    Returns
    -------
    width : float
        The width of the transistor [m].
    length : float
        The length of the transistor in [m].
    """
    if "npn13g2_" in dut_name:
        return 0.96e-6, 0.12e-6  # hardcoded, this HBT has only one available geometry
    else:
        raise IOError


def get_hbt_plot(dut, key, x_var, y_var, x_limits, y_limits):
    """Function that creates a plot object for a given HBT device.

    Arguments
    ---------
    dut : DMT.core.DutVie
        A DUT for which the plot shall be created.
    key : str
        The measurement name for which the plot is created.
    y_var : DMT.core.SpecifierStr
        The y-variable of the plot.
    sweep_var : DMT.core.SpecifierStr
        The swept variable of the lines in the plot.

    Returns
    -------
    plt : DMT.core.Plot
        A plot object for displaying a certain characteristics.
    """
    y_scale, x_scale = 1, 1
    y_label, x_label = None, None
    y_log, x_log = False, False

    if specifiers.CURRENT in y_var:
        y_label = r"$J_{\mathrm{C}} ( \si{\milli\ampere\per\square\micro\meter} ) $"
        y_scale = 1e3 / 1e6 / 1e6
    elif y_var == specifiers.TRANSIT_FREQUENCY:
        x_log = True
        x_label = r"$J_{\mathrm{C}} ( \si{\milli\ampere\per\square\micro\meter} ) $"
        x_scale = 1e3 / 1e6 / 1e6
        y_scale = 1e-9  # GHz

    if specifiers.CURRENT in y_var:
        if col_vbe == x_var:
            y_log = True

    # define the Plot
    plt = Plot(
        "dut_" + dut.name + "_" + key,
        x_specifier=x_var,
        x_scale=x_scale,
        x_label=x_label,
        x_log=x_log,
        y_specifier=y_var,
        y_scale=y_scale,
        y_label=y_label,
        y_log=y_log,
        style="xtraction_color",
        legend_location="upper right outer",
    )
    plt.x_limits = x_limits
    plt.y_limits = y_limits
    return plt


def get_mos_plot(dut, key, x_var, y_var, x_limits, y_limits, nmos):
    """Function that creates a plot object for a given MOSFET device.

    Arguments
    ---------
    dut : DMT.core.DutVie
        A DUT for which the plot shall be created.
    key : str
        The measurement name for which the plot is created.
    x_var : DMT.core.SpecifierStr
        The x-variable of the plot.
    y_var : DMT.core.SpecifierStr
        The y-variable of the plot.
    sweep_var : DMT.core.SpecifierStr
        The swept variable of the lines in the plot.
    nmos : Bool
        If True, this plot is for an nmos transistor, else pmos.

    Returns
    -------
    plt : DMT.core.Plot
        A plot object for displaying a certain characteristics.
    """
    y_scale, x_scale = 1, 1
    y_label, x_label = None, None
    y_log, x_log = False, False
    if specifiers.CURRENT in y_var:
        y_label = r"$J_{\mathrm{D}} ( \si{\milli\ampere\per\micro\meter} ) $"
        y_scale = 1e3 / 1e6  # MOS mA/um

    if specifiers.CURRENT in y_var:
        if col_vg == x_var:
            y_log = True

    # define the Plot
    plt = Plot(
        "dut_" + dut.name + "_" + key,
        x_specifier=x_var,
        x_scale=1,
        x_label=x_label,
        x_log=x_log,
        y_specifier=y_var,
        y_scale=y_scale,
        y_label=y_label,
        y_log=y_log,
        style="xtraction_color",
        legend_location="upper right outer",
    )
    plt.x_limits = x_limits
    plt.y_limits = y_limits

    if not nmos:  # for pmos we invert signs
        for var_name in ["J", "V"]:
            plt.x_label = plt.x_label.replace(var_name, "-" + var_name)
            plt.y_label = plt.y_label.replace(var_name, "-" + var_name)

    return plt


def get_parallel_hbt(dut_name):
    """Hardcoded function that returns number of parallel HBTs for given dut_name."""
    if "T00" in dut_name:
        n_parallel = 8
    elif "T01" in dut_name:
        n_parallel = 8
    elif "T02" in dut_name:
        n_parallel = 8
    elif "T03" in dut_name:
        n_parallel = 8
    else:
        raise IOError
    return n_parallel
