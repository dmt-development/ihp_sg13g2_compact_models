""" 
Purpose: Generate some nice plots for SSC-M paper of IHP

6 Plots:
* 1 lv nmos w 2um x l 0.13 um JD(VDS)
* lv nmos w 2um JD(VGS)
* 1 lv pmos w 2um x l 0.33 um -JD(-VDS)
* lv pmos l 0.33 um -JD(-VGS)
* npn fT(JC)
* npn JC(VCE)
"""

from pathlib import Path
import numpy as np
from DMT.config import COMMANDS

COMMANDS["OPENVAF"] = "openvaf"
from DMT.core import DutLib, specifiers, Plot, SimCon, DutType
from DMT.ngspice import DutNgspice as dut_circuit
from DMT.xyce import DutXyce

from helper_functions import (
    get_dut_mos,
    get_sweep_def_mos,
    get_sweep_def_hbt,
    get_hbt_dimensions,
    get_dut_hbt,
)
from create_dutlib.ihp_openPDK_SG13g2 import TechSG13G2OpenPDK

force = False  # if True: force re-simulation

path_to_folder = Path(__file__).parent.resolve().absolute()
fig_dir = path_to_folder / "documentation" / "paper_sscm"
lib_save_dir = path_to_folder / "openPDK_lib"
lib = DutLib.load(lib_save_dir, classes_technology=[TechSG13G2OpenPDK])

width = None  # width of figures
sim_con = SimCon(t_max=50, n_core=1)  # simulation controller


# define some columns for convenience
col_vd = specifiers.VOLTAGE + "D"
col_vb = specifiers.VOLTAGE + "B"
col_vc = specifiers.VOLTAGE + "C"
col_vg = specifiers.VOLTAGE + "G"
col_vs = specifiers.VOLTAGE + "S"

col_vgs = specifiers.VOLTAGE + ["G", "S"]
col_vds = specifiers.VOLTAGE + ["D", "S"]
col_vbe = specifiers.VOLTAGE + ["B", "E"]
col_vce = specifiers.VOLTAGE + ["C", "E"]

col_id = specifiers.CURRENT + "D"
col_ic = specifiers.CURRENT + "C"

col_ft = specifiers.TRANSIT_FREQUENCY

# first plot
# 1 lv nmos w 2um x l 0.13 um JD(VDS)
plot_1 = Plot(
    "single_lv_nmos_id_vs_vds",
    x_specifier=col_vds,
    y_specifier=col_id,
    # style="xtraction_color",
    style="comparison_4",
    legend_location="upper left",
)
plot_1.x_limits = (0.0, 1.4)
plot_1.y_limits = (0.0, None)
plot_1.legend_frame = False
# lv nmos w 2um JD(VGS)
plot_2 = Plot(
    "many_lv_nmos_id_vs_vgs",
    x_specifier=col_vgs,
    y_specifier=col_id,
    y_log=True,
    style="xtraction_color",
    legend_location="lower right",
)
plot_2.x_limits = (0.0, 1.4)
plot_2.legend_frame = False
# 1 lv pmos w 2um x l 0.33 um -JD(-VDS)
plot_3 = Plot(
    "single_lv_pmos_id_vs_vds",
    x_specifier=col_vds,
    y_specifier=col_id,
    style="comparison_4",
    legend_location="upper right outer",
)
plot_3.x_label = plot_3.x_label.replace("V", "-V")
plot_3.y_label = plot_3.y_label.replace("I", "-I")
plot_3.x_limits = (0.0, 1.4)
plot_3.y_limits = (0.0, None)
plot_3.legend_frame = False
# lv pmos l 0.33 um -JD(-VGS)
plot_4 = Plot(
    "many_lv_pmos_id_vs_vgs",
    x_specifier=col_vgs,
    y_specifier=col_id,
    y_log=True,
    style="xtraction_color",
    legend_location="lower right",
)
plot_4.x_label = plot_4.x_label.replace("V", "-V")
plot_4.y_label = plot_4.y_label.replace("I", "-I")
plot_4.x_limits = (0.0, 1.4)
plot_4.legend_frame = False
# npn fT(JC)
plot_5 = Plot(
    "hbt_npn_ft_vs_ic",
    x_specifier=col_ic,
    x_log=True,
    y_specifier=col_ft,
    style="xtraction_color",
    legend_location="upper left",
)
plot_5.x_limits = (1e-2, 50)
plot_5.y_limits = (0, 400)
plot_5.legend_frame = False
# npn JC(VCE)
plot_6 = Plot(
    "hbt_npn_ic_vs_vce",
    x_specifier=col_vce,
    y_specifier=col_ic,
    style="xtraction_color",
    legend_location="upper left",
)
plot_6.x_limits = (0, 2)
plot_6.y_limits = (0, 5)
plot_6.legend_frame = False

count = 0
for dut in lib:
    if dut.dut_type == DutType.n_mos and np.isclose(dut.width, 2e-6):
        if "HV" in dut.name:
            continue

        dut_cir_tt = get_dut_mos(
            dut_circuit, True, False, dut.length, dut.width, corner="mos_tt"
        )
        dut_cir_ff = get_dut_mos(
            dut_circuit, True, False, dut.length, dut.width, corner="mos_ff"
        )
        dut_cir_ss = get_dut_mos(
            dut_circuit, True, False, dut.length, dut.width, corner="mos_ss"
        )
        if np.isclose(dut.length, 0.13e-6):
            # plot 1
            key = dut.join_key("T300.00K", "dc_idvd")
            df = dut.data[key]
            (
                sweeps,
                dfs,
                x_limits,
                y_limits,
                fix_var,
                fix_var2,
                col_x,
                col_y,
                caption,
            ) = get_sweep_def_mos(dut, key, df, True, False)
            # run the circuit simulations
            for sweep in sweeps:
                sim_con.append_simulation(dut=dut_cir_tt, sweep=sweep)
                sim_con.append_simulation(dut=dut_cir_ss, sweep=sweep)
                sim_con.append_simulation(dut=dut_cir_ff, sweep=sweep)
            sim_con.run_and_read(force=force, remove_simulations=False)

            # iterate over "lines" in the plot, i.e. where the fixed voltage is constant.
            # add the data to the plot
            for sweep, df_meas in zip(sweeps, dfs):
                for dut_cir, label in zip(
                    [dut_cir_tt, dut_cir_ss, dut_cir_ff], ["tt", "ss", "ff"]
                ):
                    df_sim = dut_cir.get_data(sweep=sweep)

                    # get the data (sign to account for pmos)
                    ports = ["G", "D"]

                    # make sure all quantities in dataframe
                    df_sim.ensure_specifier_column(col_x, ports=ports)
                    df_sim.ensure_specifier_column(col_y, ports=ports)
                    df_meas.ensure_specifier_column(col_x, ports=ports)
                    df_meas.ensure_specifier_column(col_y, ports=ports)

                    # retrieve measured and simulated data
                    x_sim = df_sim[col_x].to_numpy().real
                    y_sim = df_sim[col_y].to_numpy().real
                    x_meas = df_meas[col_x].to_numpy()
                    y_meas = df_meas[col_y].to_numpy()

                    # add data to plot
                    v_fixed = np.unique(df_meas[fix_var])[0]
                    # if v_fixed < 1.3:
                    #     continue

                    if label == "tt":
                        plot_1.add_data_set(
                            x_meas,
                            y_meas,
                            label=r"$\SI{{{0:1.2f}}}{{\volt}}$".format(v_fixed),
                        )
                    plot_1.add_data_set(x_sim, y_sim)  # , label=label)

        # plot 2
        key = dut.join_key("T300.00K", "dc_idvg")
        df = dut.data[key]
        (
            sweeps,
            dfs,
            x_limits,
            y_limits,
            fix_var,
            fix_var2,
            col_x,
            col_y,
            caption,
        ) = get_sweep_def_mos(dut, key, df, True, False)
        # run the circuit simulations
        for sweep in sweeps:
            sim_con.append_simulation(dut=dut_cir_tt, sweep=sweep)
        sim_con.run_and_read(force=force, remove_simulations=False)
        # iterate over "lines" in the plot, i.e. where the fixed voltage is constant.
        # add the data to the plot
        for sweep, df_meas in zip(sweeps, dfs):
            df_sim = dut_cir_tt.get_data(sweep=sweep)

            # get the data (sign to account for pmos)
            ports = ["G", "D"]

            # make sure all quantities in dataframe
            df_sim.ensure_specifier_column(col_x, ports=ports)
            df_sim.ensure_specifier_column(col_y, ports=ports)
            df_meas.ensure_specifier_column(col_x, ports=ports)
            df_meas.ensure_specifier_column(col_y, ports=ports)

            # retrieve measured and simulated data
            x_sim = df_sim[col_x].to_numpy().real
            y_sim = df_sim[col_y].to_numpy().real
            x_meas = df_meas[col_x].to_numpy()
            y_meas = df_meas[col_y].to_numpy()

            # add data to plot
            v_fixed = np.unique(df_meas[fix_var])[0]

            if np.isclose(v_fixed, 0.6):
                continue

            v_fixed2 = np.unique(df_meas[fix_var2])[0]
            if not np.isclose(v_fixed2, 0.0):
                continue

            label = r"$l=\SI{{{0:1.2f}}}{{\micro\metre}}$, $V_{{DS}}=\SI{{{1:1.2f}}}{{\volt}}$".format(
                dut.length * 1e6, v_fixed
            )

            plot_2.add_data_set(x_meas, y_meas, label=label)
            plot_2.add_data_set(x_sim, y_sim)

    if dut.dut_type == DutType.p_mos and np.isclose(dut.length, 1.2e-6):
        if "HV" in dut.name:
            continue

        dut_cir_tt = get_dut_mos(
            dut_circuit, False, False, dut.length, dut.width, corner="mos_tt"
        )
        dut_cir_ff = get_dut_mos(
            dut_circuit, False, False, dut.length, dut.width, corner="mos_ff"
        )
        dut_cir_ss = get_dut_mos(
            dut_circuit, False, False, dut.length, dut.width, corner="mos_ss"
        )
        if np.isclose(dut.width, 0.6e-6):
            # plot 3
            key = dut.join_key("T300.00K", "dc_idvd")
            df = dut.data[key]
            (
                sweeps,
                dfs,
                x_limits,
                y_limits,
                fix_var,
                fix_var2,
                col_x,
                col_y,
                caption,
            ) = get_sweep_def_mos(dut, key, df, False, False)
            # run the circuit simulations
            for sweep in sweeps:
                sim_con.append_simulation(dut=dut_cir_tt, sweep=sweep)
                sim_con.append_simulation(dut=dut_cir_ss, sweep=sweep)
                sim_con.append_simulation(dut=dut_cir_ff, sweep=sweep)
            sim_con.run_and_read(force=force, remove_simulations=False)
            # iterate over "lines" in the plot, i.e. where the fixed voltage is constant.
            # add the data to the plot
            for sweep, df_meas in zip(sweeps, dfs):
                for dut_cir, label in zip(
                    [dut_cir_tt, dut_cir_ss, dut_cir_ff], ["tt", "ss", "ff"]
                ):
                    df_sim = dut_cir.get_data(sweep=sweep)

                    # get the data (sign to account for pmos)
                    ports = ["G", "D"]

                    # make sure all quantities in dataframe
                    df_sim.ensure_specifier_column(col_x, ports=ports)
                    df_sim.ensure_specifier_column(col_y, ports=ports)
                    df_meas.ensure_specifier_column(col_x, ports=ports)
                    df_meas.ensure_specifier_column(col_y, ports=ports)

                    # retrieve measured and simulated data
                    x_sim = -1 * df_sim[col_x].to_numpy().real
                    y_sim = -1 * df_sim[col_y].to_numpy().real
                    x_meas = -1 * df_meas[col_x].to_numpy()
                    y_meas = -1 * df_meas[col_y].to_numpy()

                    # add data to plot
                    v_fixed = np.unique(df_meas[fix_var])[0]

                    if label == "tt":
                        plot_3.add_data_set(
                            x_meas,
                            y_meas,
                            label=r"$\SI{{{0:1.2f}}}{{\volt}}$".format(v_fixed),
                        )
                    plot_3.add_data_set(x_sim, y_sim)  # , label=label)

        # plot 4
        key = dut.join_key("T300.00K", "dc_idvg")
        df = dut.data[key]
        (
            sweeps,
            dfs,
            x_limits,
            y_limits,
            fix_var,
            fix_var2,
            col_x,
            col_y,
            caption,
        ) = get_sweep_def_mos(dut, key, df, False, False)
        # run the circuit simulations
        for sweep in sweeps:
            sim_con.append_simulation(dut=dut_cir_tt, sweep=sweep)
        sim_con.run_and_read(force=force, remove_simulations=False)
        # iterate over "lines" in the plot, i.e. where the fixed voltage is constant.
        # add the data to the plot
        for sweep, df_meas in zip(sweeps, dfs):
            df_sim = dut_cir_tt.get_data(sweep=sweep)

            # get the data (sign to account for pmos)
            ports = ["G", "D"]

            # make sure all quantities in dataframe
            df_sim.ensure_specifier_column(col_x, ports=ports)
            df_sim.ensure_specifier_column(col_y, ports=ports)
            df_meas.ensure_specifier_column(col_x, ports=ports)
            df_meas.ensure_specifier_column(col_y, ports=ports)

            # retrieve measured and simulated data
            x_sim = -1 * df_sim[col_x].to_numpy().real
            y_sim = -1 * df_sim[col_y].to_numpy().real
            x_meas = -1 * df_meas[col_x].to_numpy()
            y_meas = -1 * df_meas[col_y].to_numpy()

            # add data to plot
            v_fixed = np.unique(df_meas[fix_var])[0]

            if np.isclose(v_fixed, -0.6):
                continue

            v_fixed2 = np.unique(df_meas[fix_var2])[0]
            if not np.isclose(v_fixed2, 0.0):
                continue

            label = r"$w=\SI{{{0:1.2f}}}{{\micro\metre}}$, $V_{{DS}}=\SI{{{1:1.2f}}}{{\volt}}$".format(
                dut.width * 1e6, v_fixed
            )

            plot_4.add_data_set(x_meas, y_meas, label=label)
            plot_4.add_data_set(x_sim, y_sim)

    if (
        dut.dut_type == DutType.npn
        and np.isclose(dut.length, 0.9e-6)
        and np.isclose(dut.width, 0.07e-6)
    ):
        # plot 5
        key = dut.join_key("T300.00K", "spar_vce")
        df = dut.data[key]
        (
            sweeps,
            dfs,
            x_limits,
            y_limits,
            fix_var,
            fix_var2,
            col_x,
            col_y,
            caption,
        ) = get_sweep_def_hbt(dut, key, df, dut.name)

        # run the circuit simulations
        length, width = get_hbt_dimensions(dut.name)
        # dut_cir = get_dut_hbt(DutNgspice, dut.name, length, width, nx=dut.ndevices)
        dut_cir = get_dut_hbt(DutXyce, dut.name, length, width, nx=dut.ndevices)
        ports = ["B", "C"]
        for sweep in sweeps:
            sim_con.append_simulation(dut=dut_cir, sweep=sweep)
        sim_con.run_and_read(force=force, remove_simulations=False)

        # iterate over "lines" in the plot, i.e. where the fixed voltage is constant.
        # add the data to the plot
        for sweep, df_meas in zip(sweeps, dfs):
            df_sim = dut_cir.get_data(sweep=sweep)

            # make sure all quantities in dataframe
            df_sim.ensure_specifier_column(col_x, ports=ports)
            df_sim.ensure_specifier_column(col_y, ports=ports)
            df_meas.ensure_specifier_column(col_x, ports=ports)
            df_meas.ensure_specifier_column(col_y, ports=ports)

            # retrieve measured and simulated data
            x_sim = df_sim[col_x].to_numpy().real
            y_sim = df_sim[col_y].to_numpy().real
            x_meas = df_meas[col_x].to_numpy() / 8 * dut.ndevices
            y_meas = df_meas[col_y].to_numpy()

            # add data to plot
            v_fixed = np.unique(df_meas[fix_var])[0]
            label = r"NX={:d}, $\SI{{{:1.2f}}}{{\volt}}$".format(dut.ndevices, v_fixed)

            plot_5.add_data_set(x_meas, y_meas, label=label)
            plot_5.add_data_set(x_sim, y_sim)

        if dut.ndevices == 1:
            # plot 6
            key = dut.join_key("T300.00K", "fo_vb_RF")
            df = dut.data[key]
            (
                sweeps,
                dfs,
                x_limits,
                y_limits,
                fix_var,
                fix_var2,
                col_x,
                col_y,
                caption,
            ) = get_sweep_def_hbt(dut, key, df, dut.name)

            # run the circuit simulations
            for sweep in sweeps:
                sim_con.append_simulation(dut=dut_cir, sweep=sweep)
            sim_con.run_and_read(force=force, remove_simulations=False)

            # iterate over "lines" in the plot, i.e. where the fixed voltage is constant.
            # add the data to the plot
            for sweep, df_meas in zip(sweeps, dfs):
                df_sim = dut_cir.get_data(sweep=sweep)

                # make sure all quantities in dataframe
                df_sim.ensure_specifier_column(col_x, ports=ports)
                df_sim.ensure_specifier_column(col_y, ports=ports)
                df_meas.ensure_specifier_column(col_x, ports=ports)
                df_meas.ensure_specifier_column(col_y, ports=ports)

                # retrieve measured and simulated data
                x_sim = df_sim[col_x].to_numpy().real
                y_sim = df_sim[col_y].to_numpy().real
                x_meas = df_meas[col_x].to_numpy()
                y_meas = df_meas[col_y].to_numpy() / 8 * dut.ndevices

                # add data to plot
                v_fixed = np.unique(df_meas[fix_var])[0]
                label = r"$\SI{{{:1.2f}}}{{\volt}}$".format(v_fixed)

                plot_6.add_data_set(x_meas, y_meas, label=label)
                plot_6.add_data_set(x_sim, y_sim)

plot_1.plot_pyqtgraph(show=False)
plot_2.plot_pyqtgraph(show=False)
plot_3.plot_pyqtgraph(show=False)
plot_4.plot_pyqtgraph(show=False)
plot_5.plot_pyqtgraph(show=False)
plot_6.plot_pyqtgraph(show=True)

for plt in [plot_1, plot_2, plot_3, plot_4, plot_5, plot_6]:
    plt.save_tikz(fig_dir, width=r".98\linewidth", mark_repeat=2, restrict=False)
