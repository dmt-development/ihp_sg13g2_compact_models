""" Author: Markus Müller
Purpose: Verify model against measurements and generate PyLatex document.
"""
import os
import numpy as np
from DMT.core import (
    MCard,
    specifiers,
    sub_specifiers,
    Plot,
    DutType,
    SimCon,
    VAFileMap,
)
from DMT.extraction import get_sweep_mos
import copy

# Tex stuff
from pylatex import Section, Figure, NoEscape
from pylatex.base_classes import Arguments
from pylatexenc.latex2text import LatexNodes2Text

from DMT.external import SubFile, CommandInputTikz, CommandLabel
from DMT.ngspice import DutNgspice as dut_circuit

# from DMT.xyce import DutXyce as dut_circuit
from get_circuit_psp import get_circuit_psp

force = False  # if True: force re-simulation

fig_dir = os.path.join("documentation/model_check/MOSHV/figures")
fig_dir = os.path.abspath(fig_dir)  # path to figures

width = None  # width of figures
dmt = SimCon(t_max=50, n_core=4)  # simulation controller
spot_freq = 0.1e9  # spot frequency for HF plots

# define a modelcard for the MOSFET
mc = MCard(
    ["G", "S", "D", "B"],
    "npn13G2",
    "common_source",
    1.0,
)

# for every of these devices, the sanity plots are generated
devices = [
    {
        "flavor": "n",
        "width": 0.35e-6,
        "length": 0.45e-6,
        "corner": "mos_tt",
    },
    {
        "flavor": "n",
        "width": 0.9e-6,
        "length": 0.45e-6,
        "corner": "mos_tt",
    },
    {
        "flavor": "n",
        "width": 0.35e-6,
        "length": 0.8e-6,
        "corner": "mos_tt",
    },
    {
        "flavor": "p",
        "width": 0.35e-6,
        "length": 0.45e-6,
        "corner": "mos_tt",
    },
    {
        "flavor": "p",
        "width": 0.35e-6,
        "length": 0.9e-6,
        "corner": "mos_tt",
    },
]

# define the sweeps that will be simulated
sweeps_dc = {
    "300_idvg_vds": get_sweep_mos(
        name="300_idvg_vds",
        vgs=np.linspace(0.1, 1, 91),
        vds=[0.5, 1, 1.5],
        ac=False,
        temp=300,
        mode="list",
    ),
    "300_gum_vgd": get_sweep_mos(
        name="300_gum_vgd",
        vgs=np.linspace(0.1, 1, 91),
        vgd=[-0.5, 0, 0.5],
        ac=False,
        temp=300,
        mode="list",
    ),
    "300_out_vgs": get_sweep_mos(
        name="300_out_vgs",
        vds=np.linspace(0, 2, 41),
        vgs=np.linspace(0.7, 1, 11),
        ac=False,
        temp=300,
        mode="list",
    ),
}

sweeps_ac = {
    "300_ft_id_at_vds": get_sweep_mos(
        name="300_ft_id_at_vds",
        vgs=np.linspace(0.1, 1.5, 141),
        vds=[0.5, 1, 1.5],
        ac=True,
        freq=0.1e9,
        temp=300,
        mode="list",
    ),
    "300_fmax_ic_at_vce": get_sweep_mos(
        name="300_fmax_ic_at_vce",
        vgs=np.linspace(0.1, 1.5, 141),
        vds=[0.5, 1, 1.5],
        ac=True,
        freq=0.1e9,
        temp=300,
        mode="list",
    ),
    "300_MSG_ic_at_vce": get_sweep_mos(
        name="300_MSG_ic_at_vce",
        vgs=np.linspace(0.1, 1.5, 141),
        vds=[0.5, 1, 1.5],
        ac=True,
        freq=0.1e9,
        temp=300,
        mode="list",
    ),
    "300_ReY21_ic_at_vce": get_sweep_mos(
        name="300_ReY21_ic_at_vce",
        vgs=np.linspace(0.1, 1.5, 141),
        vds=[0.5, 1, 1.5],
        ac=True,
        freq=0.1e9,
        temp=300,
        mode="list",
    ),
}


sweeps_T = []
for T in np.linspace(250, 400, 5):
    pass
    sweeps_T.append(
        get_sweep_mos(
            name="300_ft_id_at_T" + str(T),
            vgs=np.linspace(0.1, 1.5, 141),
            vds=[1.5],
            ac=True,
            freq=0.1e9,
            temp=T,
            mode="list",
        )
    )


# define the Plot objects for the sweeps
plts = {
    "300_idvg_vds": Plot(
        r"$J_{\mathrm{D}}(V_{\mathrm{GS}})$ at $V_{\mathrm{DS}}$ and $T=\SI{300}{\kelvin}$.",
        x_label=r"$V_{\mathrm{GS}} ( \si{\volt} )$",
        y_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere\per\micro\meter} )$",
        y_scale=1e3 / 1e6,
        y_log=True,
        legend_location="upper left",
    ),
    "300_gum_vgd": Plot(
        r"$J_{\mathrm{D}} (V_{\mathrm{GS}})$ at $V_{\mathrm{BC}}$ and $T=\SI{300}{\kelvin}$.",
        x_label=r"$V_{\mathrm{GS}} ( \si{\volt} )$",
        y_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere\per\micro\meter} )$",
        y_scale=1e3 / 1e6,
        y_log=True,
        legend_location="upper left",
    ),
    "300_out_vgs": Plot(
        r"$J_{\mathrm{D}} ( V_{\mathrm{DS}} )$ at $V_{\mathrm{GS}}$ and $T=\SI{300}{\kelvin}$.",
        x_label=r"$V_{\mathrm{DS}} ( \si{\volt} )$",
        y_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere\per\micro\meter} )$",
        y_scale=1e3 / 1e6,
        legend_location="upper right outer",
    ),
    "300_ft_id_at_vds": Plot(
        r"$f_{\mathrm{T}} ( J_{\mathrm{D}} )$ at $V_{\mathrm{DS}}$ and $T=\SI{300}{\kelvin}$.",
        x_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere\per\micro\meter} )$",
        x_scale=1e3 / 1e6,
        y_label=r"$f_{\mathrm{T}} ( \si{\giga\hertz} )$",
        y_scale=1e-9,
        legend_location="upper left",
        x_log=True,
    ),
    "300_fmax_ic_at_vce": Plot(
        r"$f_{\mathrm{max}} ( J_{\mathrm{D}} )$ at $V_{\mathrm{DS}}$ and $T=\SI{300}{\kelvin}$.",
        x_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere\per\micro\meter} )$",
        x_scale=1e3 / 1e6,
        y_label=r"$f_{\mathrm{max}} ( \si{\giga\hertz} )$",
        y_scale=1e-9,
        legend_location="upper left",
        x_log=True,
    ),
    "300_MSG_ic_at_vce": Plot(
        r"$\mathrm{MSG} ( J_{\mathrm{D}} )$ at $V_{\mathrm{DS}}$ and $T=\SI{300}{\kelvin}$.",
        x_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere\per\micro\meter} )$",
        x_scale=1e3 / 1e6,
        y_label=r"$\mathrm{MSG} ( \si{\dB} )$",
        y_scale=1,
        legend_location="upper left",
        x_log=True,
    ),
    "300_ReY21_ic_at_vce": Plot(
        r"$\Re \left\{ \underline{Y}_{21} \right\} ( J_{\mathrm{D}} )$ at $V_{\mathrm{DS}}$ and $T=\SI{300}{\kelvin}$.",
        x_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere\per\micro\meter} )$",
        x_scale=1e3 / 1e6,
        y_label=r"$\Re \left\{ \underline{Y}_{21} \right\} ( \si{\milli\siemens\per\micro\meter} )$",
        y_scale=1e3 / 1e6,
        legend_location="upper left",
        x_log=True,
        y_log=True,
    ),
    "id_vgs_T": Plot(
        r"$J_{\mathrm{D}} (V_{\mathrm{GS}})$ at $V_{\mathrm{DS}}=\SI{1.5}{\volt}$ over $T$.",
        x_label=r"$V_{\mathrm{GS}} ( \si{\volt} )$",
        y_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere\per\micro\meter} )$",
        y_scale=1e3 / 1e6,
        y_log=True,
        legend_location="lower right",
    ),
    "ft_id_T": Plot(
        r"$f_{\mathrm{T}} ( J_{\mathrm{D}} )$ at $V_{\mathrm{DS}}=\SI{1.5}{\volt}$ over $T$.",
        x_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere\per\micro\meter} )$",
        x_scale=1e3 / 1e6,
        y_label=r"$f_{\mathrm{T}} ( \si{\giga\hertz} )$",
        y_scale=1e-9,
        x_log=True,
        legend_location="upper left",
    ),
}

plts_T = ["id_vgs_T", "ft_id_T"]

# small helper method that sets a unique plot name for every MOS device with flavor, width and length instance parameters
def set_plt_num(plt, flavor, width, length):
    plt.num = (
        plt.num
        + "_"
        + flavor
        + "_w"
        + str(width * 1e9)
        + "nm"
        + "_l"
        + str(length * 1e9)
        + "nm"
    )
    return plt


# small helper metho that adjusts plot labels for p type devices
def set_labels_p_type(plt):
    for char in ["V", "I", "J", "Y"]:
        plt.x_label = plt.x_label.replace(char, "-" + char)
        plt.y_label = plt.y_label.replace(char, "-" + char)


# define some column names that represent electrical quantities
col_vgd = specifiers.VOLTAGE + "G" + "D" + sub_specifiers.FORCED
col_vgs = specifiers.VOLTAGE + "G" + "S" + sub_specifiers.FORCED
col_vds = specifiers.VOLTAGE + "D" + "S" + sub_specifiers.FORCED
col_id = specifiers.CURRENT + "D"
col_ig = specifiers.CURRENT + "G"
col_ft = specifiers.TRANSIT_FREQUENCY
col_fmax = specifiers.MAXIMUM_OSCILLATION_FREQUENCY
col_msg = specifiers.MAXIMUM_STABLE_GAIN
col_rey21 = specifiers.SS_PARA_Y + "D" + "G" + sub_specifiers.REAL

# path to PSP 103.6 VA code files
file_map_psp = VAFileMap("va_code_psp103p6/psp103_nqs.va")

# this object is used in DMT to control running simulations:
sim_con = SimCon(n_core=1, t_max=100)

# we generate a section in the document for every HBT flavor
for device in devices:
    width_i = device["width"]
    length_i = device["length"]
    flavor_i = device["flavor"]
    corner_i = device["corner"]

    # here we store the plots for each device
    save_names = {}

    # define a circuit to be simulated
    circuit_CS = get_circuit_psp(
        circuit_type="common_source",
        modelcard=mc,
        type_mos=flavor_i,
        flavor="HV",
        corner=corner_i,
        width=width_i,
        length=length_i,
        dut_circuit=dut_circuit,
    )

    # define a device that combines all the previously defined objects
    dut = dut_circuit(
        None,
        DutType.n_mos,
        circuit_CS,
        nodes="D,G,S,B",
        command_openvaf="openvaf",
        reference_node="S",
    )

    dut.del_db()
    dut._modelcard = "nmos13G2"

    # if p mos, we must change sign of all voltages here
    sign = 1
    if flavor_i == "p":
        sign = -1

    sweeps_dc_local = copy.deepcopy(sweeps_dc)
    sweeps_ac_local = copy.deepcopy(sweeps_ac)
    sweeps_T_local = copy.deepcopy(sweeps_T)
    if sign == -1:
        for sweep in sweeps_dc_local.values():
            for sweep_def in sweep.sweepdef:
                if "V" in sweep_def.var_name:
                    sweep_def.value_def = -sweep_def.value_def

        for sweep in sweeps_ac_local.values():
            for sweep_def in sweep.sweepdef:
                if "V" in sweep_def.var_name:
                    sweep_def.value_def = -sweep_def.value_def

        for sweep in sweeps_T_local:
            for sweep_def in sweep.sweepdef:
                if "V" in sweep_def.var_name:
                    sweep_def.value_def = -sweep_def.value_def

    # run the simulations
    for sweep_name, sweep in sweeps_dc_local.items():
        sim_con.append_simulation(dut=dut, sweep=sweep)
    for sweep_name, sweep in sweeps_ac_local.items():
        sim_con.append_simulation(dut=dut, sweep=sweep)
    for sweep in sweeps_T_local:
        sim_con.append_simulation(dut=dut, sweep=sweep)
    sim_con.run_and_read(force=force, remove_simulations=False)

    # set the flavor string
    model_tex_str = "PSP"
    if flavor_i == "n":
        model_tex_str += " NMOS"
    elif flavor_i == "p":
        model_tex_str += " PMOS"

    model_tex_str += r" $w=\SI{{{0:3.2f}}}{{\micro\meter}}$".format(width_i * 1e6)
    model_tex_str += r" $l=\SI{{{0:3.2f}}}{{\micro\meter}}$".format(length_i * 1e6)

    # generate the DC plots
    for sweep_name, sweep in sweeps_dc_local.items():
        plt = copy.deepcopy(plts[sweep_name])
        df = dut.get_data(sweep=sweep)
        df.ensure_specifier_column(col_vgs, ports=["G", "D"])
        df.ensure_specifier_column(col_vgd, ports=["G", "D"])
        df.ensure_specifier_column(col_vds, ports=["G", "D"])

        # here, for every plot type, we define how the data gets plotted
        if sweep_name == "300_idvg_vds":
            for _i_c, v_d, df_filtered in df.iter_unique_col(col_vds, decimals=3):
                idd = sign * df_filtered[col_id].to_numpy() / width_i
                vgs = sign * df_filtered[col_vgs].to_numpy()
                label = r"$V_{{\mathrm{{DS}}}}=\SI{{{0:1.1f}}}{{\volt}}$".format(
                    np.real(v_d)
                )
                plt.add_data_set(np.real(vgs), np.real(idd), label=label)
            plt.x_limits = (0.1, 1)
            plt.y_limits = (1e-6, 0.5)

        elif sweep_name == "300_gum_vgd":
            for _i_c, v_bc, df_filtered in df.iter_unique_col(col_vgd, decimals=3):
                idd = sign * df_filtered[col_id].to_numpy() / width_i
                vgs = sign * df_filtered[col_vgs].to_numpy()
                label = r"$V_{{\mathrm{{GD}}}}=\SI{{{0:1.1f}}}{{\volt}}$".format(
                    np.real(v_bc)
                )
                plt.add_data_set(np.real(vgs), np.real(idd), label=label)
            plt.x_limits = (0.1, 1)
            plt.y_limits = (1e-6, 0.5)

        elif sweep_name == "300_out_vgs":
            for _i_c, v_be, df_filtered in df.iter_unique_col(col_vgs, decimals=3):
                idd = sign * df_filtered[col_id].to_numpy() / width_i
                vce = sign * df_filtered[col_vds].to_numpy()
                label = r"$V_{{\mathrm{{BE}}}}=\SI{{{0:1.2f}}}{{\volt}}$".format(
                    np.real(v_be)
                )
                plt.add_data_set(np.real(vce), np.real(idd), label=label)

        else:
            raise IOError

        if sign == -1:
            set_labels_p_type(plt)

        plt = set_plt_num(plt, flavor_i, width_i, length_i)
        plt_save_name = plt.save_tikz(
            fig_dir, width=width, mark_repeat=2, extension=r"tikz"
        )
        save_names[plt.name] = plt_save_name

    # generate the AC plots
    for sweep_name, sweep in sweeps_ac_local.items():
        plt = copy.deepcopy(plts[sweep_name])
        df = dut.get_data(sweep=sweep)
        df.ensure_specifier_column(col_vgs, ports=["G", "D"])
        df.ensure_specifier_column(col_vgd, ports=["G", "D"])
        df.ensure_specifier_column(col_vds, ports=["G", "D"])
        df.ensure_specifier_column(col_ft, ports=["G", "D"])
        df.ensure_specifier_column(col_fmax, ports=["G", "D"])
        df.ensure_specifier_column(col_msg, ports=["G", "D"])
        df.ensure_specifier_column(col_rey21, ports=["G", "D"])

        # here, for every plot type, we define how the data gets plotted
        if sweep_name == "300_ft_id_at_vds":
            for _i_c, v_d, df_filtered in df.iter_unique_col(col_vds, decimals=3):
                idd = sign * df_filtered[col_id].to_numpy() / width_i
                ft = df_filtered[col_ft].to_numpy()
                label = r"$V_{{\mathrm{{DS}}}}=\SI{{{0:1.1f}}}{{\volt}}$".format(
                    np.real(v_d)
                )
                plt.add_data_set(np.real(idd), np.real(ft), label=label)
            plt.x_limits = (1e-5, 1)
            # plt.y_limits = (0, 30)

            # plt.plot_py(show=True)

        elif sweep_name == "300_fmax_ic_at_vce":
            for _i_c, v_d, df_filtered in df.iter_unique_col(col_vds, decimals=3):
                idd = sign * df_filtered[col_id].to_numpy() / width_i
                fmax = df_filtered[col_fmax].to_numpy()
                label = r"$V_{{\mathrm{{DS}}}}=\SI{{{0:1.1f}}}{{\volt}}$".format(
                    np.real(v_d)
                )
                plt.add_data_set(np.real(idd), np.real(fmax), label=label)
            plt.x_limits = (1e-5, 1)
            # plt.y_limits = (0, 160)

        elif sweep_name == "300_MSG_ic_at_vce":
            for _i_c, v_d, df_filtered in df.iter_unique_col(col_vds, decimals=3):
                idd = sign * df_filtered[col_id].to_numpy() / width_i
                msg = df_filtered[col_msg].to_numpy()
                label = r"$V_{{\mathrm{{DS}}}}=\SI{{{0:1.1f}}}{{\volt}}$".format(
                    np.real(v_d)
                )
                plt.add_data_set(np.real(idd), 20 * np.log10(np.real(msg)), label=label)

            plt.x_limits = (1e-5, 1)
            # plt.y_limits = (0, 70)

        elif sweep_name == "300_ReY21_ic_at_vce":
            for _i_c, v_d, df_filtered in df.iter_unique_col(col_vds, decimals=3):
                idd = sign * df_filtered[col_id].to_numpy() / width_i
                rey21 = df_filtered[col_rey21].to_numpy() / width_i
                label = r"$V_{{\mathrm{{DS}}}}=\SI{{{0:1.1f}}}{{\volt}}$".format(
                    np.real(v_d)
                )
                plt.add_data_set(np.real(idd), rey21, label=label)

            plt.x_limits = (1e-5, 1)
            plt.y_limits = (1e-4, 2)

        if sign == -1:
            set_labels_p_type(plt)

        plt = set_plt_num(plt, flavor_i, width_i, length_i)
        plt_save_name = plt.save_tikz(
            fig_dir, width=width, mark_repeat=2, extension=r"tikz"
        )
        save_names[plt.name] = plt_save_name

    # generate the T plots
    for plt_name in plts_T:
        plt = copy.deepcopy(plts[plt_name])

        for sweep in sweeps_T_local:
            df = dut.get_data(sweep=sweep)
            df.ensure_specifier_column(col_vgs, ports=["G", "D"])
            df.ensure_specifier_column(col_vgd, ports=["G", "D"])
            df.ensure_specifier_column(col_vds, ports=["G", "D"])
            df.ensure_specifier_column(col_ft, ports=["G", "D"])
            df.ensure_specifier_column(col_fmax, ports=["G", "D"])
            df.ensure_specifier_column(col_msg, ports=["G", "D"])
            df.ensure_specifier_column(col_rey21, ports=["G", "D"])

            key = dut.get_sweep_key(sweep)
            T = dut.get_key_temperature(key)
            label = r"$T=\SI{{{0:1.1f}}}{{\kelvin}}$".format(T)

            # here, for every plot type, we define how the data gets plotted
            if plt_name == "id_vgs_T":
                for _i_c, v_d, df_filtered in df.iter_unique_col(col_vds, decimals=3):
                    vgs = sign * df_filtered[col_vgs].to_numpy()
                    idd = sign * df_filtered[col_id].to_numpy() / width_i
                    plt.add_data_set(np.real(vgs), np.real(idd), label=label)

                plt.x_limits = (0.1, 1.5)
                plt.y_limits = (1e-6, 1)

            elif plt_name == "ft_id_T":
                for _i_c, v_d, df_filtered in df.iter_unique_col(col_vds, decimals=3):
                    ft = df_filtered[col_ft].to_numpy()
                    idd = sign * df_filtered[col_id].to_numpy() / width_i
                    plt.add_data_set(np.real(idd), np.real(ft), label=label)

                # plt.y_limits = (0, 30)
                plt.x_limits = (1e-4, 1)

        if sign == -1:
            set_labels_p_type(plt)

        plt = set_plt_num(plt, flavor_i, width_i, length_i)
        plt_save_name = plt.save_tikz(
            fig_dir, width=width, mark_repeat=2, extension=r"tikz"
        )
        save_names[plt.name] = plt_save_name

    # create the Tex document
    doc = SubFile()
    title = "IHP SG13G2 " + model_tex_str + " Sanity Checks"
    with doc.create(Section(NoEscape(title))):
        doc.append(
            NoEscape(
                r"This section gives an overview of the DC Characteristics of the transistor. "
            )
        )
        doc.append(
            NoEscape(
                r"Next, some DC characteristics @$T=\SI{300}{\kelvin}$ are visualized."
            )
        )
        for sweep_name, _sweep in sweeps_dc_local.items():
            plt = plts[sweep_name]
            doc.append(NoEscape(r"\FloatBarrier "))
            with doc.create(Figure(position="h!")) as _plot:
                _plot.append(NoEscape(r"\centering"))
                _plot.append(
                    CommandInputTikz(
                        arguments=Arguments(
                            "width=0.6\\textwidth, height=0.4\\textheight",
                            "" + os.path.join("figures", save_names[plt.name]) + "",
                        )
                    )
                )
                _plot.add_caption(NoEscape(plt.name))
                _plot.append(
                    CommandLabel(
                        arguments=Arguments(LatexNodes2Text().latex_to_text(plt.name))
                    )
                )

        doc.append(NoEscape(r"\FloatBarrier "))

        doc.append(
            NoEscape(
                r"Next, some AC characteristics @$T=\SI{300}{\kelvin}$ are visualized."
            )
        )
        for sweep_name, _sweep in sweeps_ac_local.items():
            plt = plts[sweep_name]
            doc.append(NoEscape(r"\FloatBarrier "))
            with doc.create(Figure(position="h!")) as _plot:
                _plot.append(NoEscape(r"\centering"))
                _plot.append(
                    CommandInputTikz(
                        arguments=Arguments(
                            "width=0.6\\textwidth, height=0.4\\textheight",
                            "" + os.path.join("figures", save_names[plt.name]) + "",
                        )
                    )
                )
                _plot.add_caption(NoEscape(plt.name))
                _plot.append(
                    CommandLabel(
                        arguments=Arguments(LatexNodes2Text().latex_to_text(plt.name))
                    )
                )

        doc.append(NoEscape(r"\FloatBarrier "))

        doc.append(
            NoEscape(
                r"Next, some characteristics at different $T$ and $V_{\mathrm{DS}}=\SI{1.5}{\volt}$ are visualized."
            )
        )
        for plt_name in plts_T:
            plt = plts[plt_name]
            doc.append(NoEscape(r"\FloatBarrier "))
            with doc.create(Figure(position="h!")) as _plot:
                _plot.append(NoEscape(r"\centering"))
                _plot.append(
                    CommandInputTikz(
                        arguments=Arguments(
                            "width=0.6\\textwidth, height=0.4\\textheight",
                            "" + os.path.join("figures", save_names[plt.name]) + "",
                        )
                    )
                )
                _plot.add_caption(NoEscape(plt.name))
                _plot.append(
                    CommandLabel(
                        arguments=Arguments(LatexNodes2Text().latex_to_text(plt.name))
                    )
                )

        doc.append(NoEscape(r"\FloatBarrier "))

    doc.append(NoEscape(r"\FloatBarrier "))
    doc.append(NoEscape(r"\newpage "))

    doc.generate_tex(
        os.path.join(
            "documentation/model_check/MOSHV",
            r"MOS_model_check_flavor"
            + flavor_i
            + "_w"
            + str(width_i * 1e9)
            + "nm"
            + "_l"
            + str(length_i * 1e9)
            + "nm",
        )
    )

# now we create a Tex section where the length is varied
lengths = [0.45e-6, 0.9e-6, 1.2e-6, 2e-6]
width = 0.35e-6
dfs_out, dfs_idvg = [], []
for flavor_i in ["n", "p"]:
    # for flavor_i in ["p"]:
    for length_i in lengths:
        # define a circuit to be simulated
        circuit_CS = get_circuit_psp(
            circuit_type="common_source",
            modelcard=mc,
            type_mos=flavor_i,
            flavor="HV",
            corner="mos_tt",
            width=width,
            length=length_i,
            dut_circuit=dut_circuit,
        )

        # define a device that combines all the previously defined objects
        dut = dut_circuit(
            None,
            DutType.n_mos,
            circuit_CS,
            nodes="D,G,S,B",
            command_openvaf="openvaf",
            reference_node="S",
        )

        dut.del_db()
        dut._modelcard = flavor_i + "mos13G2"

        sign = 1
        if flavor_i == "p":
            sign = -1

        # define the sweeps
        sweep_ft = get_sweep_mos(
            name="300_idvg_vds_l_scale",
            vgs=sign * np.linspace(0.1, 1.5, 141),
            vds=[sign * 1],
            ac=True,
            freq=0.1e9,
            temp=300,
            mode="list",
        )
        sweep_out = get_sweep_mos(
            name="300_idvd_vgs_l_scale",
            vds=sign * np.linspace(0, 2, 201),
            vgs=[sign * 0.9],
            ac=True,
            freq=0.1e9,
            temp=300,
            mode="list",
        )
        sweeps = [sweep_ft, sweep_out]

        # run the simulations
        sim_con.append_simulation(dut=dut, sweep=sweep_ft)
        sim_con.append_simulation(dut=dut, sweep=sweep_out)
        sim_con.run_and_read(force=force, remove_simulations=False)

        for sweep in sweeps:
            df = dut.get_data(sweep=sweep)
            df.ensure_specifier_column(col_vgs, ports=["G", "D"])
            df.ensure_specifier_column(col_vgd, ports=["G", "D"])
            df.ensure_specifier_column(col_vds, ports=["G", "D"])
            df.ensure_specifier_column(col_ft, ports=["G", "D"])
            df.ensure_specifier_column(col_fmax, ports=["G", "D"])
            df.ensure_specifier_column(col_msg, ports=["G", "D"])
            df.ensure_specifier_column(col_rey21, ports=["G", "D"])

        dfs_out.append(dut.get_data(sweep=sweep_out))
        dfs_idvg.append(dut.get_data(sweep=sweep_ft))

    # define plots
    plts = []
    plt_idvgs_l_scale = Plot(
        r"$J_{\mathrm{D}} ( V_{\mathrm{GS}} ) $ at $V_{\mathrm{DS}}=\SI{1}{\volt}$ over device length.",
        y_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere \per\micro\meter} )$",
        y_scale=1e3 / 1e6,
        x_label=r"$V_{\mathrm{GS}} ( \si{\volt} )$",
        x_scale=1,
        y_log=True,
        legend_location="upper left",
    )
    plt_idvds_l_scale = Plot(
        r"$J_{\mathrm{D}} ( V_{\mathrm{DS}} ) $ at $V_{\mathrm{GS}}=\SI{0.9}{\volt}$ over device length.",
        y_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere \per\micro\meter} )$",
        y_scale=1e3 / 1e6,
        x_label=r"$V_{\mathrm{DS}} ( \si{\volt} )$",
        x_scale=1,
        legend_location="upper left",
    )
    plt_ft_l_scale = Plot(
        r"$f_{\mathrm{T}} ( J_{\mathrm{D}} )$ at $V_{\mathrm{DS}}=\SI{1}{\volt}$ over device length.",
        x_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere \per\micro\meter} )$",
        x_scale=1e3 / 1e6,
        y_label=r"$f_{\mathrm{T}} ( \si{\giga\hertz} )$",
        y_scale=1e-9,
        x_log=True,
        legend_location="upper left",
    )
    plts.append(plt_idvgs_l_scale)
    plts.append(plt_idvds_l_scale)
    plts.append(plt_ft_l_scale)
    for plt in plts:
        plt.num += flavor_i

    if sign == -1:
        for plt in plts:
            set_labels_p_type(plt)

    for length_i, df_out, df_idvg in zip(lengths, dfs_out, dfs_idvg):
        label = r"$\SI{{{0:2.1f}}}{{\nano\meter}}$".format(length_i * 1e9)
        plt_idvgs_l_scale.add_data_set(
            np.abs(df_idvg["V_G"]), sign * df_idvg["I_D"] / width, label=label
        )
        plt_ft_l_scale.add_data_set(
            sign * df_idvg["I_D"] / width, df_idvg["F_T"], label=label
        )
        plt_idvds_l_scale.add_data_set(
            np.abs(df_out["V_D"] - df_out["V_S"]),
            sign * df_out["I_D"] / width,
            label=label,
        )

    # plt_idvds_l_scale.plot_py(show=False)
    # plt_ft_l_scale.plot_py(show=False)
    # plt_idvgs_l_scale.plot_py(show=True)

    # create the Tex document
    doc = SubFile()
    title = "IHP SG13G2 " + flavor_i.upper() + "MOS Length Scaling Sanity Checks"
    with doc.create(Section(NoEscape(title))):
        for plt in plts:
            plt_save_name = plt.save_tikz(
                fig_dir, width=None, mark_repeat=2, extension=r"tikz"
            )
            doc.append(NoEscape(r"\FloatBarrier "))
            with doc.create(Figure(position="h!")) as _plot:
                _plot.append(NoEscape(r"\centering"))
                _plot.append(
                    CommandInputTikz(
                        arguments=Arguments(
                            "width=0.6\\textwidth, height=0.4\\textheight",
                            "" + os.path.join("figures", plt_save_name) + "",
                        )
                    )
                )
                _plot.add_caption(NoEscape(plt.name))
                _plot.append(
                    CommandLabel(
                        arguments=Arguments(LatexNodes2Text().latex_to_text(plt.name))
                    )
                )

    doc.generate_tex(
        os.path.join(
            "documentation/model_check/MOSHV",
            r"MOS_model_check_flavor_" + flavor_i + "_lscaling",
        )
    )

# now we create a Tex section where the width is varied
widths = [0.35e-6, 0.6e-6, 0.9e-6, 1.2e-6]
length = 0.45e-6
dfs_out, dfs_idvg = [], []
for flavor_i in ["n", "p"]:
    # for flavor_i in ["p"]:
    for width_i in widths:
        # define a circuit to be simulated
        circuit_CS = get_circuit_psp(
            circuit_type="common_source",
            modelcard=mc,
            type_mos=flavor_i,
            flavor="HV",
            corner="mos_tt",
            width=width_i,
            length=length,
            dut_circuit=dut_circuit,
        )

        # define a device that combines all the previously defined objects
        dut = dut_circuit(
            None,
            DutType.n_mos,
            circuit_CS,
            nodes="D,G,S,B",
            command_openvaf="openvaf",
            reference_node="S",
        )

        dut.del_db()
        dut._modelcard = flavor_i + "mos13G2"

        sign = 1
        if flavor_i == "p":
            sign = -1

        # define the sweeps
        sweep_ft = get_sweep_mos(
            name="300_idvg_vds_w_scale",
            vgs=sign * np.linspace(0.1, 1.5, 141),
            vds=[sign * 1],
            ac=True,
            freq=0.1e9,
            temp=300,
            mode="list",
        )
        sweep_out = get_sweep_mos(
            name="300_idvd_vgs_w_scale",
            vds=sign * np.linspace(0, 2, 201),
            vgs=[sign * 0.9],
            ac=True,
            freq=0.1e9,
            temp=300,
            mode="list",
        )
        sweeps = [sweep_ft, sweep_out]

        # run the simulations
        sim_con.append_simulation(dut=dut, sweep=sweep_ft)
        sim_con.append_simulation(dut=dut, sweep=sweep_out)
        sim_con.run_and_read(force=force, remove_simulations=False)

        for sweep in sweeps:
            df = dut.get_data(sweep=sweep)
            df.ensure_specifier_column(col_vgs, ports=["G", "D"])
            df.ensure_specifier_column(col_vgd, ports=["G", "D"])
            df.ensure_specifier_column(col_vds, ports=["G", "D"])
            df.ensure_specifier_column(col_ft, ports=["G", "D"])
            df.ensure_specifier_column(col_fmax, ports=["G", "D"])
            df.ensure_specifier_column(col_msg, ports=["G", "D"])
            df.ensure_specifier_column(col_rey21, ports=["G", "D"])

        dfs_out.append(dut.get_data(sweep=sweep_out))
        dfs_idvg.append(dut.get_data(sweep=sweep_ft))

    # define plots
    plts = []
    plt_idvgs_w_scale = Plot(
        r"$J_{\mathrm{D}} ( V_{\mathrm{GS}} ) $ at $V_{\mathrm{DS}}=\SI{1}{\volt}$ over device width.",
        y_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere \per\micro\meter} )$",
        y_scale=1e3 / 1e6,
        x_label=r"$V_{\mathrm{GS}} ( \si{\volt} )$",
        x_scale=1,
        y_log=True,
        legend_location="upper left",
    )
    plt_idvds_w_scale = Plot(
        r"$J_{\mathrm{D}} ( V_{\mathrm{DS}} ) $ at $V_{\mathrm{GS}}=\SI{0.9}{\volt}$ over device width.",
        y_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere \per\micro\meter} )$",
        y_scale=1e3 / 1e6,
        x_label=r"$V_{\mathrm{DS}} ( \si{\volt} )$",
        x_scale=1,
        legend_location="upper left",
    )
    plt_ft_w_scale = Plot(
        r"$f_{\mathrm{T}} ( J_{\mathrm{D}} )$ at $V_{\mathrm{DS}}=\SI{1}{\volt}$ over device width.",
        x_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere \per\micro\meter} )$",
        x_scale=1e3 / 1e6,
        y_label=r"$f_{\mathrm{T}} ( \si{\giga\hertz} )$",
        y_scale=1e-9,
        x_log=True,
        legend_location="upper left",
    )
    plts.append(plt_idvgs_w_scale)
    plts.append(plt_idvds_w_scale)
    plts.append(plt_ft_w_scale)
    for plt in plts:
        plt.num += flavor_i

    if sign == -1:
        for plt in plts:
            set_labels_p_type(plt)

    for width_i, df_out, df_idvg in zip(widths, dfs_out, dfs_idvg):
        label = r"$\SI{{{0:2.1f}}}{{\nano\meter}}$".format(width_i * 1e9)
        plt_idvgs_w_scale.add_data_set(
            np.abs(df_idvg["V_G"]), sign * df_idvg["I_D"] / width_i, label=label
        )
        plt_ft_w_scale.add_data_set(
            sign * df_idvg["I_D"] / width_i, df_idvg["F_T"], label=label
        )
        plt_idvds_w_scale.add_data_set(
            np.abs(df_out["V_D"] - df_out["V_S"]),
            sign * df_out["I_D"] / width_i,
            label=label,
        )

    # plt_idvds_w_scale.plot_py(show=False)
    # plt_ft_w_scale.plot_py(show=False)
    # plt_idvgs_w_scale.plot_py(show=True)

    # create the Tex document
    doc = SubFile()
    title = "IHP SG13G2 " + flavor_i.upper() + "MOS Width Scaling Sanity Checks"
    with doc.create(Section(NoEscape(title))):
        for plt in plts:
            plt_save_name = plt.save_tikz(
                fig_dir, width=None, mark_repeat=2, extension=r"tikz"
            )
            doc.append(NoEscape(r"\FloatBarrier "))
            with doc.create(Figure(position="h!")) as _plot:
                _plot.append(NoEscape(r"\centering"))
                _plot.append(
                    CommandInputTikz(
                        arguments=Arguments(
                            "width=0.6\\textwidth, height=0.4\\textheight",
                            "" + os.path.join("figures", plt_save_name) + "",
                        )
                    )
                )
                _plot.add_caption(NoEscape(plt.name))
                _plot.append(
                    CommandLabel(
                        arguments=Arguments(LatexNodes2Text().latex_to_text(plt.name))
                    )
                )

    doc.generate_tex(
        os.path.join(
            "documentation/model_check/MOSHV",
            r"MOS_model_check_flavor_" + flavor_i + "_wscaling",
        )
    )

# now we create a Tex section where the corner is varied
width = 0.35e-6
length = 0.45e-6
dfs_out, dfs_idvg = [], []
corners = ["mos_tt", "mos_ff", "mos_sf"]
for corner_i in corners:
    # define a circuit to be simulated
    circuit_CS = get_circuit_psp(
        circuit_type="common_source",
        modelcard=mc,
        type_mos="n",
        flavor="HV",
        corner=corner_i,
        width=width,
        length=length,
        dut_circuit=dut_circuit,
    )

    # define a device that combines all the previously defined objects
    dut = dut_circuit(
        None,
        DutType.n_mos,
        circuit_CS,
        nodes="D,G,S,B",
        command_openvaf="openvaf",
        reference_node="S",
    )

    dut.del_db()
    dut._modelcard = "nmos13G2"

    # define the sweeps
    sweep_ft = get_sweep_mos(
        name="300_idvg_vds_corner",
        vgs=np.linspace(0.1, 1.5, 141),
        vds=[1],
        ac=True,
        freq=0.1e9,
        temp=300,
        mode="list",
    )
    sweep_out = get_sweep_mos(
        name="300_idvd_vgs_corner",
        vds=np.linspace(0, 2, 201),
        vgs=[0.9],
        ac=True,
        freq=0.1e9,
        temp=300,
        mode="list",
    )
    sweeps = [sweep_ft, sweep_out]

    # run the simulations
    sim_con.append_simulation(dut=dut, sweep=sweep_ft)
    sim_con.append_simulation(dut=dut, sweep=sweep_out)
    sim_con.run_and_read(force=force, remove_simulations=False)

    for sweep in sweeps:
        df = dut.get_data(sweep=sweep)
        df.ensure_specifier_column(col_vgs, ports=["G", "D"])
        df.ensure_specifier_column(col_vgd, ports=["G", "D"])
        df.ensure_specifier_column(col_vds, ports=["G", "D"])
        df.ensure_specifier_column(col_ft, ports=["G", "D"])
        df.ensure_specifier_column(col_fmax, ports=["G", "D"])
        df.ensure_specifier_column(col_msg, ports=["G", "D"])
        df.ensure_specifier_column(col_rey21, ports=["G", "D"])

    dfs_out.append(dut.get_data(sweep=sweep_out))
    dfs_idvg.append(dut.get_data(sweep=sweep_ft))

# define plots
plts = []
plt_idvgs_corner = Plot(
    r"$J_{\mathrm{D}} ( V_{\mathrm{GS}} ) $ at $V_{\mathrm{DS}}=\SI{1}{\volt}$ over corners.",
    y_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere \per\micro\meter} )$",
    y_scale=1e3 / 1e6,
    x_label=r"$V_{\mathrm{GS}} ( \si{\volt} )$",
    x_scale=1,
    y_log=True,
    legend_location="upper left",
)
plt_idvds_corner = Plot(
    r"$J_{\mathrm{D}} ( V_{\mathrm{DS}} ) $ at $V_{\mathrm{GS}}=\SI{0.9}{\volt}$ over corners.",
    y_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere \per\micro\meter} )$",
    y_scale=1e3 / 1e6,
    x_label=r"$V_{\mathrm{DS}} ( \si{\volt} )$",
    x_scale=1,
    legend_location="upper left",
)
plt_ft_corner = Plot(
    r"$f_{\mathrm{T}} ( J_{\mathrm{D}} )$ at $V_{\mathrm{DS}}=\SI{1}{\volt}$ over corners.",
    x_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere \per\micro\meter} )$",
    x_scale=1e3 / 1e6,
    y_label=r"$f_{\mathrm{T}} ( \si{\giga\hertz} )$",
    y_scale=1e-9,
    x_log=True,
    legend_location="upper left",
)
plts.append(plt_idvgs_corner)
plts.append(plt_idvds_corner)
plts.append(plt_ft_corner)

for corner_i, df_out, df_idvg in zip(corners, dfs_out, dfs_idvg):
    label = corner_i.replace("_", "\_")
    plt_idvgs_corner.add_data_set(df_idvg["V_G"], df_idvg["I_D"] / width, label=label)
    plt_ft_corner.add_data_set(df_idvg["I_D"] / width, df_idvg["F_T"], label=label)
    plt_idvds_corner.add_data_set(
        df_out["V_D"] - df_out["V_S"], df_out["I_D"] / width, label=label
    )

# plt_idvds_w_scale.plot_py(show=False)
# plt_ft_w_scale.plot_py(show=False)
# plt_idvgs_w_scale.plot_py(show=True)

# create the Tex document
doc = SubFile()
title = "IHP SG13G2 NMOS Corner Sanity Checks"
with doc.create(Section(NoEscape(title))):
    for plt in plts:
        plt_save_name = plt.save_tikz(
            fig_dir, width=None, mark_repeat=2, extension=r"tikz"
        )
        doc.append(NoEscape(r"\FloatBarrier "))
        with doc.create(Figure(position="h!")) as _plot:
            _plot.append(NoEscape(r"\centering"))
            _plot.append(
                CommandInputTikz(
                    arguments=Arguments(
                        "width=0.6\\textwidth, height=0.4\\textheight",
                        "" + os.path.join("figures", plt_save_name) + "",
                    )
                )
            )
            _plot.add_caption(NoEscape(plt.name))
            _plot.append(
                CommandLabel(
                    arguments=Arguments(LatexNodes2Text().latex_to_text(plt.name))
                )
            )


doc.generate_tex(
    os.path.join(
        "documentation/model_check/MOSHV",
        r"MOS_model_check_corners",
    )
)
