# this file runs an initial ngspice simulation using the IHP HBT models to see if they work in ngspice
from pathlib import Path
from DMT.config import COMMANDS

COMMANDS["OPENVAF"] = "openvaf"
from DMT.core import (
    DutType,
    specifiers,
    sub_specifiers,
    SimCon,
    Plot,
    MCard,
    save_or_show,
    constants,
)
from DMT.extraction import get_sweep_mos
from DMT.ngspice import DutNgspice
from DMT.xyce import DutXyce
from get_circuit_psp import get_circuit_psp

repo_dir = Path(__file__).parent.parent

duts_circuit = [
    DutNgspice,
    DutXyce,
]
show = True  # If True: open plots and visualize them
force = (
    False  # If True: re-run simulations even if the simulation results already exist
)
flavor = "HV"  # HV or LV MOS flavors
type_channel = "n"

factor_v = 1 if type_channel == "n" else -1

# define a "sweep" object that defines the voltage and temperature sweeps in the simulator
sweep = get_sweep_mos(
    name="idvgh",
    # vgs=[0, factor_v * 2, 201],
    # vds=[factor_v * 1.5],
    vgs=[factor_v * 0.5, factor_v * 1, 51],
    vds=[factor_v * 1.5],
    vbs=[0],
    ac=True,
    temp=constants.P_CELSIUS0 + 27,
    freq=0.1e9,
    mode="lin",
)

# define a modelcard for the MOS
mc = MCard(
    ["G", "S", "D", "B"],
    "npn13G2",
    "common_source",
    1.0,
)

length = 0.13e-6
if flavor == "HV":
    length = 0.35e-6

# define a circuit to be simulated
duts = []
for dut_circuit in duts_circuit:
    circuit_CS = get_circuit_psp(
        circuit_type="common_source",
        modelcard=mc,
        corner="mos_tt",
        flavor=flavor,
        type_mos=type_channel,
        length=0.35e-6,
        width=1e-6,
        dut_circuit=dut_circuit,
    )

    # define a device that combines all the previously defined objects
    dut = dut_circuit(
        None,
        DutType.n_mos,
        circuit_CS,
        nodes="D,G,S,B",
        reference_node="S",
        simulator_options={},
    )

    dut.del_db()
    dut._modelcard = "nmos13G2"
    duts.append(dut)


# this object is used in DMT to control running simulations:
sim_con = SimCon(n_core=1, t_max=30)
sim_con.append_simulation(dut=duts, sweep=sweep)
sim_con.run_and_read(force=force, remove_simulations=False)

# make sure that the simulation data contains derived quantities like F_T and C_BE
col_id = specifiers.CURRENT + "D"
col_vd_f = specifiers.VOLTAGE + "D" + sub_specifiers.FORCED
col_vg_f = specifiers.VOLTAGE + "G" + sub_specifiers.FORCED
col_ft = specifiers.TRANSIT_FREQUENCY
col_fmax = specifiers.TRANSIT_FREQUENCY
col_cgs = specifiers.CAPACITANCE + ["G", "S"]
col_cgd = specifiers.CAPACITANCE + ["G", "D"]
col_y11 = specifiers.SS_PARA_Y + ["G", "G"] + sub_specifiers.REAL
col_y21 = specifiers.SS_PARA_Y + ["D", "G"] + sub_specifiers.REAL
col_y12 = specifiers.SS_PARA_Y + ["G", "D"] + sub_specifiers.REAL
col_y22 = specifiers.SS_PARA_Y + ["D", "D"] + sub_specifiers.REAL
cols_y = [col_y11, col_y21, col_y12, col_y22]

dfs = []
for dut in duts:
    for sweep_i in [sweep]:
        df = dut.get_data(sweep=sweep_i)
        for df_i in [df]:
            df_i.ensure_specifier_column(col_ft, ports=["G", "D"])
            df_i.ensure_specifier_column(col_fmax, ports=["G", "D"])
            df_i.ensure_specifier_column(col_cgs, ports=["G", "D"])
            df_i.ensure_specifier_column(col_cgd, ports=["G", "D"])
            df_i.ensure_specifier_column(col_y11, ports=["G", "D"])
            df_i.ensure_specifier_column(col_y21, ports=["G", "D"])
            df_i.ensure_specifier_column(col_y12, ports=["G", "D"])
            df_i.ensure_specifier_column(col_y22, ports=["G", "D"])

    df = dut.get_data(sweep=sweep)
    dfs.append(df)

# define plots
plts = []
plt_gummel = Plot(
    r"idvg",
    x_label=r"$V_{\mathrm{GS}} (\si{\volt})$",
    # y_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere\per\micro\meter} )$",
    y_label=r"$I_{\mathrm{D}} ( \si{\micro\ampere} )$",
    y_log=True,
    style="mix",
    legend_location="upper left",
    # y_scale=1e3 / 1e6,
    y_scale=1e6,
)
plts.append(plt_gummel)
plt_ft = Plot(
    r"ftid",
    x_label=r"$J_{\mathrm{D}} ( \si{\milli\ampere\per\square\micro\meter} )$",
    y_label=r"$f_{\mathrm{T}} ( \si{\giga\hertz} ) $",
    x_log=True,
    style="mix",
    legend_location="upper left",
    y_scale=1e-9,
    x_scale=1e3 / 1e6,
)
plt_ft.y_limits = (0, 80e9)
# plts.append(plt_ft)
plt_cgs = Plot(
    r"cgsvg",
    x_label=r"$V_{\mathrm{GS}} (\si{\volt})$",
    y_label=r"$C_{\mathrm{GS}} ( \si{\femto\farad\per\square\micro\meter} ) $",
    style="mix",
    y_scale=1e15 / 1e6,
)
# plts.append(plt_cgs)

labels = {
    DutNgspice: "ngspice/OSDI",
    DutXyce: "xyce",
}
# normalize quantities to the ACTUAL emitter window area
width = 1  # 0.35e-6
for df, dut_circuit in zip(dfs, duts_circuit):
    idd = df[col_id].to_numpy() / width
    vg = df[col_vg_f].to_numpy()
    vd = df[col_vd_f].to_numpy()
    ft = df[col_ft].to_numpy()
    cgs = df[col_cgs].to_numpy() / width
    cds = df[col_cgd].to_numpy() / width

    plt_gummel.add_data_set(vg, idd, label=labels[dut_circuit])
    plt_cgs.add_data_set(vg, cgs, label=labels[dut_circuit])
    plt_ft.add_data_set(idd, ft, label=labels[dut_circuit])

# open the plots or save them as Tikz (if show=False)
save_or_show(
    plts=plts,
    show=show,
    location=str(
        repo_dir
        / "pictures"
        / "ihp_model_simulation_results"
        / f"MOS_{flavor}_{type_channel}mos"
    ),
    fontsize="normalsize",
    line_width="thin",
    mark_repeat=1,
    width="3in",
    height="2in",
)
