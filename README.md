# SemiMod GmbH: IHP SG13G2 model verification

This project by SemiMod GmbH targets the model verification and conversion for the [open-source IHP SG13G2](https://github.com/IHP-GmbH/IHP-Open-PDK) compact models for HBT and MOS devices. 
The target circuit simulators are ngspice and Xyce. Statistical modeling is only given for the ngspice MOS models.

The project is not in a finished state.

This project is funded by [IHP GmbH](https://www.ihp-microelectronics.com/de).

<img src="pictures/output_hbt.png" width="425"/> <img src="pictures/ft_jc_hbt.png" width="425"/> 

# Project roadmap

- [x] Create Gitlab repo with decent documentation
- [x] Create an overview of the measurement data as a PDF document
- [x] HBT models
  - [x] Convert HBT VBIC model to ngspice format
  - [x] Convert HBT VBIC model to Xyce format
  - [x] Verify model results and document results
  - [x] Compare model results to available measurement data
- [x] MOS HV and LV models
  - [x] Convert MOS PSP model to ngspice format
  - [x] Check statistical modeling results with ngspice
  - [x] Convert MOS PSP model to Xyce format
  - [x] Verify model results and document results
  - [x] Compare model results to available measurement data

# Result files download

Here are several results, available as a download:

- [HBT modeled device characteristics overview and sanity check for ngspice](https://gitlab.com/dmt-development/ihp_sg13g2_compact_models/-/jobs/artifacts/main/raw/documentation/model_check/HBT/documentation.pdf?job=hbt_sanity_document_ngspice)
- [HBT modeled device characteristics overview and sanity check for Xyce](https://gitlab.com/dmt-development/ihp_sg13g2_compact_models/-/jobs/artifacts/main/raw/documentation/model_check/HBT/documentation.pdf?job=hbt_sanity_document_xyce)
- [MOSLV modeled device characteristics overview and sanity check for ngspice](https://gitlab.com/dmt-development/ihp_sg13g2_compact_models/-/jobs/artifacts/main/raw/documentation/model_check/MOSLV/documentation.pdf?job=mos_lv_sanity_document)
- [MOSHV modeled device characteristics overview and sanity check for ngspice](https://gitlab.com/dmt-development/ihp_sg13g2_compact_models/-/jobs/artifacts/main/raw/documentation/model_check/MOSHV/documentation.pdf?job=mos_hv_sanity_document)
- [Model vs. measured device characteristics comparison](http://files.semimod.de/IHP_openPDK_model_vs_measurement.pdf)

If you miss a plot in any of the documents, issues and even better merge requests are always welcome.

# Installation and configuration

We recommend Python v3.8, but newer versions should also work. 
To run the scripts in this repository, the following Python packages 
must be installed:

- [DMT-core](https://gitlab.com/dmt-development/dmt-core)
- [DMT-extraction](https://gitlab.com/dmt-development/dmt-extraction)

In addition, a recent version of [ngspice](https://ngspice.sourceforge.io/) must be installed and **included in your PATH**, so that the ngspice executable is found anywhere on your machine. 
Furthermore, you will need [OpenVAF](https://openvaf.semimod.de/) for compiling the PSP Verilog-A model. 
If you also intend to compile the model documentation, a [Tex Live](https://tug.org/texlive/) installation is required.

# Results and final files

This repository contains several Python scripts with different purposes. 
Below, the most important scripts and their results are documented.

## 1. Measurement data overview Document

To generate the overview document for the measurement data, follow the following steps.

**First**, run the script create_dutlib/create_dut_lib.py: 
```
cd create_dutlib
python create_dut_lib.py
```
The output is a DMT.DutLib object that stores the IHP-provided measurement data as a Pandas database for further processing with Python.

**Second**, run the script create_dutlib/create_meas_doc.py: 
```
cd create_dutlib
python create_meas_doc.py
```
The output is several Tex files and PDF plots in the directory documentation/meas_data_overview.

**Third**, go to the directory documentation/meas_data_overview and run:
```
cd documentation/meas_data_overview
lualatex documentation.tex
lualatex documentation.tex
```


## 2. HBT and MOS model sanity check

The script in HBT_verification/2_gen_pdf_model_check.py, MOS_verification/2_gen_pdf_model_check_lv.py 
and MOS_verification/2_gen_pdf_model_check_hv.py runs several simulations for the SG13G2 HBT, LV MOS and HV MOS devices. 

The Tex document can be compiled as follows: 

```
cd documentation/model_check/HBT_<simulator>
mppbcc create cache/
lualatex documentation.tex
makepp -f documentation.makefile -j12 --last-chance-rules --build-cache=cache/ 
lualatex documentation.tex
```

Here, the [makepp](https://makepp.sourceforge.net/) tool is used to run the complex makefile. 
The document for the MOS devices is compiled in the exact same way. 

# Using the models with ngspice

## 1. HBT model

### ngspice

The HBT model is relatively straightforward to use in a ngspice netlist.  Simply include the "lib" file from "model_files/ngspice" and then initiate a model instance as shown below:

```
.include path/to/model_files/ngspice/SG13G2_hbt_woStatistics.hsp.lib
X_X1 n_C n_B n_E n_S n_T npn13G2 Nx=1 selft=1
```

It is important to use the "X" here as the first letter for indicating to ngspice that a sub-circuit instance shall be created. 

### Xyce

To use a HBT in a Xyce netlist the usage is similar. In a netlist use these lines:
 
```
.include path/to/model_files/xyce/SG13G2_hbt_woStatistics.hsp.lib
XX1 n_C n_B n_E n_S n_T npn13G2 NX=1 selft=1  
```

It is important to use the "`X`" here as the first letter for indicating to Xyce that a sub-circuit instance shall be created. 

### Flavors and parameters

There are three HBT flavors available:

  - `npn13G2` : The standard IHP high-speed HBT device as a unit cell.
  - `npn13G2l`: The same device as `npn13G2`, but a length scalable contact configuration and model.
  - `npn13G2v`: The same as `npn13G2l`, but with a modified collector design for withstanding higher voltages at the cost of lower transit frequency

The HBTs have the following nodes:

  - `n_C`: Collector node
  - `n_B`: Base node
  - `n_E`: Emitter node
  - `n_S`: Substrate node
  - `n_T`: Thermal node (self-heating temperature increase)

Convergence issues can be caused if HBTs are operated at very high power densities due to enormous self-heating.

The `npn13G2` model has the following instance parameters

  - `Nx` (default=1): Number of HBT unit cells.
  - `selft` (default=1): Activate or deactivate self-heating.
  - `dtemp` (default=0): Temperature increase with respect to the ambient temperature.

The `npn13G2l` and the `npn13G2l` models have the same instance parameters as the `npn13G2` model and additionally

  - `le` (default=2.5e-6): Drawn emitter window length. 

## 2. MOS models

### ngspice

The MOS high-voltage (HV) and low-voltage (LV) transistors use the PSP model, for which no built-in implementation is available in ngspice. 
So compile the file "va_code_psp103p6/psp103_nqs.va" with [OpenVAF](https://openvaf.semimod.de/): 

```
cd va_code_psp103p6
openvaf psp103_nqs.va
```

Important side node: The Verilog-A Code in this repo is not the official released PSP103.6 code! In order to allow local increased temperatures, we changed the parameter definition for "DTA" from a model parameter to an instance parameter!

Put the result "psp103_nqs.osdi" in a folder of your choice.  The ".pre_osdi" statement in ngspice will load the compiled Verilog-A model. Then, in a netlist write for example:

```
.pre_osdi /path/to/psp103_nqs.osdi
.lib path/to/model_files/ngspice/MOSHV/cornerMOSlv_psp.lib mos_tt
.lib path/to/model_files/ngspice/MOSLV/cornerMOShv_psp.lib mos_tt
X1_nmos n_D n_G n_S n_B sg13_lv_nmos l=0.13u w=0.35u
X2_nmos n_D n_G n_S n_B sg13_hv_nmos 
```

The lines above would load the "`mos_tt`" corner models and instantiate one LV nmos and one HV nmos transistor.

As this PDK uses the Verilog-A code of the model, scaling using the `.option scale=X` is not possible. This will result in false simulation results.

### Xyce

Xyce has PSP 103.4 built-in. The PDK can be used with the following lines in a PDK:

```
.lib path/to/model_files/xyce/MOSLV/cornerMOSlv_psp.lib mos_tt
Xnmos n_D n_G n_S n_B sg13_lv_nmos L=0.13u W=0.35u  
```

Important side node: The built-in PSP 103.4 model in Xyce uses "DTA" as a model parameter. Hence, local increased temperatures are not possible using one single model, like it is defined in this PDK. In order to increase the temperature for one or multiple transistor instances, one has to define another set of transistor models. 

The lines above would load the "`mos_tt`" corner models and instantiate one LV nmos transistor.

### Flavors and parameters

The MOS devices come in four flavors:

  - `sg13_lv_nmos`: A n-type LV MOSFET
  - `sg13_lv_pmos`: A p-type LV MOSFET
  - `sg13_hv_nmos`: A n-type HV MOSFET
  - `sg13_hv_pmos`: A p-type HV MOSFET

The MOS devices have four nodes:

  - `n_D`: Drain contact
  - `n_G`: Gate contact
  - `n_S`: Source contact
  - `n_B`: Bulk contact

The following MOS corners are supported by both simulators:

  - `mos_tt`: typical
  - `mos_ss`: slow n, slow p
  - `mos_ff`: fast n, fast p
  - `mos_sf`: slow n, fast p
  - `mos_fs`: fast n, slow p

For ngspice also statistical corners are available:

  - `mos_tt_stat`: typical with statistical variations
  - `mos_ss_stat`: slow n, slow p with statistical variations
  - `mos_ff_stat`: fast n, fast p with statistical variations
  - `mos_sf_stat`: slow n, fast p with statistical variations
  - `mos_fs_stat`: fast n, slow p with statistical variations

Independent of flavor or type, the mos model has the following instance parameters:

  - `w` (default=0.35u): channel width. Valid range [0.15um, 10um] or [1um, 10um], depending on rfmode.
  - `l` (default=0.34u(nmos), 0.28u(pmos)): channel length. Valid range [0.13um, 10um] (LV) or [0.4um, 10um] (hv).
  - `ng` (default=1): Number of gates for given width. Possible values: 1 or 2
  - `m` (default=1): Device multiplier
  - `as` (default=0): Bottom area of source junction (for MOS-Subckt with D/S-overlap, calculated automatically if `as`<=0)
  - `ad` (default=0): Bottom area of drain junction (for MOS-Subckt with D/S-overlap, calculated automatically if `as`<=1)
  - `pd` (default=0): Perimeter of drain junction (for MOS-Subckt with D/S-overlap, calculated automatically if `as`<=1)
  - `ps` (default=0): Perimeter of source junction (for MOS-Subckt with D/S-overlap, calculated automatically if `as`<=1)
  - `trise` (default=0): Temperature increase with respect to the ambient temperature (not possible in xyce)
  - `z1` (default=0.34e-6): D/S-overlap calculation helper
  - `z2` (default=0.38e-6): D/S-overlap calculation helper
  - `wmin` (default=0.15e-6): Minimal width 
  - `rfmode` (default=0): MOS transistor used in DC or rf mode, activates the nqs calculations in the model.
  - `pre_layout` (default=1): Switches some parameters related to the layout.

The length `l` and width `w` of have a defined valid range, depending on type, flavor and `rfmode`. While the upper limit is always 10 µm, the minimal values are as follows:

| transistor | minimal length | minimal width |
| ---------- | -------------- | ------------- |
| nmos lv DC | 0.13 µm        | 0.15 µm       |
| nmos lv rf | 0.13 µm        | 1.00 µm       |
| pmos lv DC | 0.13 µm        | 0.15 µm       |
| pmos lv rf | 0.13 µm        | 5.00 µm       |
| nmos hv DC | 0.45 µm        | 0.30 µm       |
| nmos hv rf | 0.45 µm        | 2.50 µm       |
| pmos hv DC | 0.40 µm        | 0.30 µm       |
| pmos hv rf | 0.40 µm        | 2.50 µm       |

These limitations are given in `model_files/xyce/MOSLV/sg13g2_moslv_psp_mod.lib` and `model_files/xyce/MOSHV/sg13g2_moshv_psp_mod.lib` and are copied from the commercial PDK. Since neither ngspice nor Xyce supports the `paramtest`` operation, it is commented in the file and noted here.

### Statistical process modeling

For ngspice, process variation simulation using monte-carlo (MC) is possible for the MOSFETs. In the model file model_files/ngspice/MOSHV/cornerMOShv_psp.lib and model_files/ngspice/MOSLV/cornerMOSlv_psp.lib, more than the 5 usual corners are implemented. Each corner has also a statistical corner, as mentioned in the section before, and here list again. 
The detailed usage is shown in the example circuits:

- LV MOS
  - device level
    - nmos_transfer_single_vds_stat.cir
    - nmos_transfer_single_vds_stat_loop.cir
    - nmos_output_single_vgs_stat.cir
    - nmos_output_single_vgs_stat_loop.cir
  - circuit level
    - mos_nand_stat.cir 
    - mos_nand_stat_loop.cir
- HV MOS
  - device level
    - hv_nmos_transfer_single_vds_stat.cir
    - hv_nmos_transfer_single_vds_stat_loop.cir 
  - circuit level
    - hv_mos_nand_stat_loop.cir

Alternatively, one could also implement the statistical modeling using a switching variable `mc_ok` and use it in the parameter library file for all statistical parameters as follows:

```
.param sg13g2_lv_nmos_toxo     ='gauss(sg13g2_lv_nmos_toxo_norm,     0.0133, mc_ok)'
```

This would replace the "statistical corners" and would allow switching on and off statistical simulations for the current loaded corner.

Currently, ngspice does not support miss-match simulations. If this changes in the future, feel invited to open an issue about this topic and maybe already prepare a merge request.

# Contributing

We are open to contributions from others. 
The main responsible persons for this project are:

- Markus Müller markus.mueller@semimod.de
- Mario Krattenmacher mario.krattenmacher@semimod.de
- Xiaodi Jin xiaodi.jin@semimod.de

# PSP and VBIC compact models acknowledgment

We acknowledge the PSP compact model and its authors. The model is used by the IHP SG13G2 PDK for MOSFET compact modeling. 
The PSP model was authored by NXP Semiconductors, Delft University of Technology, and CEA Leti. 

We acknowledge the VBIC compact model and its authors. The model is used by the IHP SG13G2 PDK for HBT compact modeling. 
According to [this website](https://designers-guide.org/vbic/references.html), these are the contributors to the VBIC model:
Jerry Seitchik, Derek Bowers, Didier Celi, Mark Dunn, Mark Foisy, Ian Getreu, Terry Magee, Marc McSwain, Shahriar Moinian, Kevin Negus, James Parker, David Roulston, Michael Schröter, Shaun Simpkins, Paul van Wijnen, and Larry Wagner.

# Acknowledgment

We acknowledge valuable support from the following persons:

  - Prof. Michael Schröter (TU Dresden)
  - Holger Vogt (ngspice)
  - René Scholz and Sergei Andreev (IHP GmbH)
  - Christoph Weimer (SemiMod GmbH)

# License

This project is licensed under the GPLv3.
